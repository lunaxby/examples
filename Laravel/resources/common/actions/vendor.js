export const REQUEST_VENDORS = 'REQUEST_VENDORS';
export const RECEIVE_VENDORS = 'RECEIVE_VENDORS';
export const RECEIVE_VENDOR = 'RECEIVE_VENDOR';

export const actionRequestVendors = () => (dispatch) => {
    dispatch({
        type: REQUEST_VENDORS,
        loading: true,
    });
};

export const fetchVendors = (params = {}) => (dispatch) => {
    dispatch(actionRequestVendors());
    return new Promise((resolve, reject) => {
        axios
            .get(window.API_URL + 'vendors', {
                params: params
            })
            .then((response) => {
                if (response.data) {
                    dispatch({
                        type: RECEIVE_VENDORS,
                        data: response.data,
                        loading: false,
                    });
                    dispatch(saveVendorResponse({}));
                    resolve(response.data);
                }
            })
            .catch((error) => {
                reject(error.response.data);
            });
    });
};

export const fetchVendor = (params = {}) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios
            .get(window.API_URL + `vendors/${params.id}`, {
                params: {}
            })
            .then((response) => {
                if (response.data) {
                    dispatch({
                        type: RECEIVE_VENDOR,
                        data: response.data,
                    });
                    resolve(response.data);
                }
            })
            .catch((error) => {
                reject(error.response.data);
            });
    });
};

export const SAVE_VENDOR_REQUEST = 'SAVE_VENDOR_REQUEST';
export const SAVE_VENDOR_RESPONSE = 'SAVE_VENDOR_RESPONSE';

const saveVendorResponse = (data) => ({
    type: SAVE_VENDOR_RESPONSE,
    data,
});
export const saveVendor = (params = {}) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios({
            method: params.id ? 'patch' : 'post',
            url: window.API_URL + (params.id ? `vendors/${params.id}` : `vendors`),
            data: params,
        })
            .then((response) => {
                if (response.data) {
                    resolve(response.data);
                }
            })
            .catch((error) => {
                dispatch(saveVendorResponse(error.response.data));
                reject(error.response.data);
            });
    });
};

export const CREATE_VENDOR = 'CREATE_VENDOR';
export const createVendor = () => ({
    type: CREATE_VENDOR,
});

export const RESET_ERROR = 'RESET_ERROR';
export const resetError = (data) => ({
    type: RESET_ERROR,
    data
});

export const deleteVendor = (id) => (dispatch) => {
    return new Promise((resolve, reject) => {
            axios({
                method: 'DELETE',
                url: window.API_URL + `vendors/${id}`,
            })
                .then((response) => {
                    resolve(response);
                })
                .catch((error) => {
                    reject(error.response.data);
                });
        }
    )
};