export const RECEIVE_LOG = 'RECEIVE_LOG';
export const actionFetchLog = (params = {}) => (dispatch) => {    
    return new Promise((resolve, reject) => {
        axios
            .get(window.API_URL + `logs?key=${params.key}`, {
                params: {}
            })
            .then((response) => {
                if (response.data) {
                    dispatch({
                        type: RECEIVE_LOG,
                        data: response.data,
                    });
                    resolve(response.data);
                }
            })
            .catch((error) => {
                reject(error.response.data);
            });
    });
};