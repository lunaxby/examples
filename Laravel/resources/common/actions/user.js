
import { USER_ROLE_POWER_USER } from "../../common/user-role"

export const REQUEST_USERS = 'REQUEST_USERS';
export const RECEIVE_USERS = 'RECEIVE_USERS';
export const RECEIVE_USER = 'RECEIVE_USER';

export const RECEIVE_POWER_USERS = 'RECEIVE_POWER_USERS';

export const actionRequestUsers = () => (dispatch) => {
    dispatch({
        type: REQUEST_USERS,
        loading: true,
    });
};

const fetchUsersByRole = (params = {}, role) => (dispatch) => {
    dispatch(actionRequestUsers());

    const roleActionsMap = {
        [USER_ROLE_POWER_USER]: RECEIVE_POWER_USERS,
    }

    let filter = [{ 'field': 'role', 'value': role }];
    if (params.filter) {
        filter = filter.concat(params.filter);
    }

    return new Promise((resolve, reject) => {
        axios
            .get(window.API_URL + 'users', {
                params: {
                    ...params,
                    filter: filter
                }
            })
            .then((response) => {
                if (response.data) {
                    dispatch({
                        type: roleActionsMap[role],
                        data: response.data,
                        loading: false,
                    });
                    resolve(response.data);
                }
            })
            .catch((error) => {
                reject(error.response.data);
            });
    });
};

export const fetchPowerUsers = (params = {}) => (dispatch) => {
    return dispatch(fetchUsersByRole(params, USER_ROLE_POWER_USER));
};

export const fetchUsers = (params = {}) => (dispatch) => {
    dispatch(actionRequestUsers());
    return new Promise((resolve, reject) => {
        axios
            .get(window.API_URL + 'users', {
                params: params
            })
            .then((response) => {
                if (response.data) {
                    dispatch({
                        type: RECEIVE_USERS,
                        data: response.data,
                        loading: false,
                    });
                    dispatch(saveUserResponse({}));
                    resolve(response.data);
                }
            })
            .catch((error) => {
                reject(error.response.data);
            });
    });
};

export const fetchUser = (params = {}) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios
            .get(window.API_URL + `users/${params.userId}`, {
                params: {}
            })
            .then((response) => {
                if (response.data) {
                    dispatch({
                        type: RECEIVE_USER,
                        data: response.data,
                    });
                    resolve(response.data);
                }
            })
            .catch((error) => {
                reject(error.response.data);
            });
    });
};

export const SAVE_USER_REQUEST = 'SAVE_USER_REQUEST';
export const SAVE_USER_RESPONSE = 'SAVE_USER_RESPONSE';

const saveUserResponse = (data) => ({
    type: SAVE_USER_RESPONSE,
    data,
});
export const saveUser = (params = {}) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios({
            method: params.id ? 'patch' : 'post',
            url: window.API_URL + (params.id ? `users/${params.id}` : `users`),
            data: params,
        })
            .then((response) => {
                if (response.data) {
                    resolve(response.data);
                }
            })
            .catch((error) => {
                dispatch(saveUserResponse(error.response.data));
                reject(error.response.data);
            });
    });
};

export const CREATE_USER = 'CREATE_USER';
export const createUser = () => ({
    type: CREATE_USER,
});

export const RESET_ERROR = 'RESET_ERROR';
export const resetError = (data) => ({
    type: RESET_ERROR,
    data
});

export const deleteUser = (id) => (dispatch) => {
    return new Promise((resolve, reject) => {
            axios({
                method: 'DELETE',
                url: window.API_URL + `users/${id}`,
            })
                .then((response) => {
                    resolve(response);
                })
                .catch((error) => {
                    reject(error.response.data);
                });
        }
    )
};