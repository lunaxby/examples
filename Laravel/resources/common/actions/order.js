export const REQUEST_ORDERS = 'REQUEST_ORDERS';
export const RECEIVE_ORDERS = 'RECEIVE_ORDERS';

export const RECEIVE_BUY_ORDERS = 'RECEIVE_BUY_ORDERS';

export const actionRequestOrders = () => (dispatch) => {
    dispatch({
        type: REQUEST_ORDERS,
        loading: true,
    });
};

export const fetchOrders = (params = {}) => (dispatch) => {
    dispatch(actionRequestOrders());
    return new Promise((resolve, reject) => {
        axios
            .get(window.API_URL + 'orders', {
                params: params
            })
            .then((response) => {
                if (response.data) {
                    dispatch({
                        type: RECEIVE_ORDERS,
                        data: response.data,
                        loading: false,
                    });
                    dispatch(saveOrderResponse({}));
                    resolve(response.data);
                }
            })
            .catch((error) => {
                reject(error.response.data);
            });
    });
};

export const fetchBuyOrders = (params = {}) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios
            .get(window.API_URL + 'orders/buy', {
                params: params
            })
            .then((response) => {
                if (response.data) {
                    dispatch({
                        type: RECEIVE_BUY_ORDERS,
                        data: response.data,
                    });
                    resolve(response.data);
                }
            })
            .catch((error) => {
                reject(error.response.data);
            });
    });
};

export const SAVE_ORDER_RESPONSE = 'SAVE_ORDER_RESPONSE';
export const SAVE_ORDER_REQUEST = 'SAVE_ORDER_REQUEST';
const saveOrderResponse = (data) => ({
    type: SAVE_ORDER_RESPONSE,
    data,
});

export const RECEIVE_ORDER = 'RECEIVE_ORDER';
export const fetchOrder = (params = {}) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios
            .get(window.API_URL + `orders/${params.id}`, {
                params: {}
            })
            .then((response) => {
                if (response.data) {
                    dispatch({
                        type: RECEIVE_ORDER,
                        data: response.data,
                    });
                    resolve(response.data);
                }
            })
            .catch((error) => {
                reject(error.response.data);
            });
    });
};

export const INITIAL_ORDER_LIST = 'INITIAL_ORDER_LIST';
export const initialOrderList = (data) => ({
    type: INITIAL_ORDER_LIST,
    data,
});