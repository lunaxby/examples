import { connect } from 'react-redux';
import LogComponent from '../components/LogComponent';

const mapStateToProps = (state) => {
    const { messages } = state.logReducer.store;
    return {
        state,
        messages,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    };
};

const LogContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(LogComponent);

export default LogContainer;