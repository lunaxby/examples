import {combineReducers} from 'redux';
import { RECEIVE_LOG } from '../actions/log';

const initState = {
    messages: []
};

const store = (state = initState, action) => {
    switch (action.type) {
        case RECEIVE_LOG:
            return {
                ...state,
                messages: action.data.data,
            };
        default:
            return state;
    }
};

const logReducer = combineReducers({
    store,
});

export default logReducer;