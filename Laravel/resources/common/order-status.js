import React from 'react';

export const ORDER_STATUS_OPEN = 'open';
export const ORDER_STATUS_SUBMITTED = 'submitted';
export const ORDER_STATUS_PARTIALLY_RECEIVED = 'partially_received';
export const ORDER_STATUS_RECEIVED = 'received';
export const ORDER_STATUS_CLOSED = 'closed';
export const ORDER_STATUS_CANCELED = 'canceled';

export const colorizeStatus = (status) => {
    return 'order-status-' + status;    
}

export const statusList = {
    [ORDER_STATUS_OPEN]: 'Open',
    [ORDER_STATUS_SUBMITTED]: 'Submitted',
    [ORDER_STATUS_RECEIVED]: 'Received',
    [ORDER_STATUS_PARTIALLY_RECEIVED]: 'Partially Received',
    [ORDER_STATUS_CLOSED]: 'Closed',
    [ORDER_STATUS_CANCELED]: 'Canceled'
}

export const colorizedVendorList = (order) => {
    const { sub_orders } = order;
    if (!Array.isArray(sub_orders)) {
        return null;
    }

    let notEmptySuborders = sub_orders.filter(sub_order => { return sub_order.order_prices && sub_order.order_prices.length > 0 });
    const sub_orders_count = notEmptySuborders.length;

    let content = notEmptySuborders.map((sub_order, i) => {
        const { vendor, order_prices } = sub_order;
        if (order_prices.length > 0) {
            return (
                <span key={i} className={colorizeStatus(sub_order.status)}>{vendor.name}{i + 1 != sub_orders_count ? ', ' : ''}</span>
            )
        }

    });

    return content;
}
