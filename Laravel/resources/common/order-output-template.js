export const ORDER_OUTPUT_TEMPLATE_PRODUCT_PURCHASE_HISTORY = 'product_purchase_history';
export const ORDER_OUTPUT_TEMPLATE_ORDER_STATUS_REPORT = 'order_status';
export const ORDER_OUTPUT_TEMPLATE_TOTAL_PURCHASES_RECEIVED = 'total_purchases_received';
export const ORDER_OUTPUT_TEMPLATE_PRINT_LABELS = 'print_labels';
export const ORDER_OUTPUT_TEMPLATE_PRINT_TYPE_EASY_ORDER_FORM = 'easy_order_form';
