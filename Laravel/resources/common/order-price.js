export const getPrice = (orderPriceItem) => {
    const { price_entity: { price_breakings }, quantity } = orderPriceItem;

    // const originalPrice = orderPriceItem.price_entity.price;
    const originalPrice = orderPriceItem.price;

    if (price_breakings.length == 0) {
        return Number(originalPrice)
    }

    const filteredPriceBreaks = price_breakings.filter((price_breaking) => {
        return quantity >= price_breaking.quantity
    })
    filteredPriceBreaks.sort((a, b) => (a.quantity < b.quantity) ? 1 : ((b.quantity < a.quantity) ? -1 : 0));
    
    const priceBreak = filteredPriceBreaks.length > 0 ? filteredPriceBreaks[0] : null;
    const priceValue = priceBreak ? priceBreak.price : originalPrice;

    return Number(priceValue);
}