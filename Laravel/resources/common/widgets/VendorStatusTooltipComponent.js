import React from 'react';

import Tooltip from "react-simple-tooltip"
import {
    ORDER_STATUS_OPEN,
    ORDER_STATUS_SUBMITTED,
    ORDER_STATUS_CLOSED,
    ORDER_STATUS_CANCELED,
    ORDER_STATUS_RECEIVED,

    colorizeStatus,
} from '../../common/order-status'

export default class VendorStatusTooltipComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const tooltipContent = (
            <div className="text-center vendor-status-tooltip">
                <div className="row">
                    <div className="col-sm-12 text-left">
                        Vendors, which are displayed in this column, are colored to reflect status of an order to each vendor.
                    </div>
                </div>

                <div className={`row mt-2 ${colorizeStatus(ORDER_STATUS_OPEN)}`}>
                    <div className="col-sm-3 text-left">Red</div>
                    <div className="col-sm-9 text-left">Order to this vendor is Open</div>
                </div>
                <div className={`row ${colorizeStatus(ORDER_STATUS_SUBMITTED)}`}>
                    <div className="col-sm-3 text-left">Blue</div>
                    <div className="col-sm-9 text-left">Order to this vendor is Submitted or Partially Received</div>
                </div>
                <div className={`row ${colorizeStatus(ORDER_STATUS_RECEIVED)}`}>
                    <div className="col-sm-3 text-left">Turquoise</div>
                    <div className="col-sm-9 text-left">Order to this vendor is Received</div>
                </div>
                <div className={`row ${colorizeStatus(ORDER_STATUS_CLOSED)}`}>
                    <div className="col-sm-3 text-left">Green</div>
                    <div className="col-sm-9 text-left">Order to this vendor is Closed</div>
                </div>
                <div className={`row ${colorizeStatus(ORDER_STATUS_CANCELED)}`}>
                    <div className="col-sm-3 text-left">Grey</div>
                    <div className="col-sm-9 text-left">Order to this vendor is Canceled</div>
                </div>

            </div>
        )

        return (
            <span className="vendor-status-tooltip-icon">
                <Tooltip
                    placement={"top"}
                    content={tooltipContent}
                    background={'#fff'}
                    color={'#000'}
                    zIndex={999999}
                >
                    <div>
                        <i className="fas fa-question-circle"></i>
                    </div>
                </Tooltip>
            </span>
        )   
    }
}