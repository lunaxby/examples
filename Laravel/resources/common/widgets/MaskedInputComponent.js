import React from 'react';
import Cleave from 'cleave.js/react';

import {INPUT_MASK_PRICE, INPUT_MASK_QTY} from '../../common/input'
import { convertToPrice } from '../../common/price'

export default class MaskedInputComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            readOnly: false,
            type: 'text',
            value: '',
            name: '',
            options: {},
            placeholder: '',
            className: '',
            max: null,
            min: null,
        }

        this.state = {
            ...props
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps != this.props) {
            this.setState({
                ...nextProps
            })
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.value != prevProps.value) {
            if (this.state.cleave) {
                this.state.cleave.setRawValue(this.props.value);
            }
        }
    }

    onKeyPress(e) {
        if (this.state.options != INPUT_MASK_QTY) {
            return;
        }
        if (e.which != 8 && e.which != 0 && e.which < 48 || e.which > 57)
        {
            e.preventDefault();
        }
    }

    async handleChange(event) {
        const target = event.target;
        let value = null;

        switch (target.type) {
            case 'checkbox':
                value = target.checked;
                break;
            case 'file':
                value = target.files[0];
                break;
            default:
                value = target.value;
        }

        const { max } = this.state;
        if (max && Number(max) < Number(value)) {
            value = Number(max);
            this.state.cleave.setRawValue(value);
        }

        await this.setState({
            value: value,
        });

        if (this.props.onChange) {
            this.props.onChange(event);
        }
    }

    async hadlePriceChanged(rawValue) {
        if (this.state.options === INPUT_MASK_PRICE) {
            const { cleave } = this.state;
            const convertedToPrice = convertToPrice(rawValue, '');
            if (!Number.isNaN(convertedToPrice) && (convertedToPrice != rawValue || rawValue == 0) && cleave) {
                cleave.setRawValue(convertedToPrice);
            }
        }
    }

    async handleOnBlur(e) {
        e.preventDefault();
        await this.hadlePriceChanged(e.target.rawValue);
    }

    async onCleaveInit(cleave) {
        await this.setState({ cleave: cleave });
        const { value } = this.state;
        cleave.setRawValue(value);

        await this.hadlePriceChanged(value);
    }

    render() {
        const { className, name, options, value, placeholder, type, readOnly, min } = this.state;

        return (
                <Cleave
                    onInit={(cleave) => {
                        this.onCleaveInit(cleave);
                    }}
                    options={options}
                    className={className}
                    type={type}
                    readOnly={readOnly}
                    placeholder={placeholder}
                    name={name}
                    min={min}
                    onKeyPress={(e) => {this.onKeyPress(e)}}
                    onChange={(e) => {
                            this.handleChange({ target: { name: name, type: type, value: e.target.rawValue } })
                        }
                    }
                    onBlur={(e) => {
                        this.handleOnBlur(e)
                    }}
                    // value={value}
                />
        )
    }
}
