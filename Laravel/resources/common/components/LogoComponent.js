import React from 'react';

export default class LogoComponent extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { description, title } = this.props;
        return (
            <div className="card-header">
                <img className="logo-img" src="/img/dhpi-logo.png" alt="logo" width="102" height="#{conf.logoHeight}"></img>
                <span className="splash-description">{description ? description : "Dental Health Products, Inc"}</span>
                { title }
            </div>
        );
    }
}