import React from 'react';
import { actionFetchLog } from '../actions/log'
import toastr from "../toastr";
import { convertToDateTime } from '..//date'

export default class LogComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    errorMessage(message) {
        toastr.error(message, 'Error');
    }

    async componentDidMount() {
        const { key } = this.props.match.params;

        try {
            await this.props.dispatch(actionFetchLog({ key }));
        } catch (e) {
            this.errorMessage(e.message ? e.message : 'Log not found');
        }
    }

    render() {
        const { messages } = this.props;

        return (
            <div className="row justify-content-center">
                <div className="col-md-12 card pt-3">
                    <div className="rounded">
                        <div className="row justify-content-center mb-3 с-card-header">
                            <div className="col-md-12">
                                <h1>Logs</h1>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">

                                <table className="table w-100 g-mb-25">
                                    <thead className="g-hidden-sm-down g-color-gray-dark-v6">
                                        <tr>
                                            <th className="align-middle">
                                                Message
                                            </th>
                                            <th className="align-middle">
                                                Date
                                            </th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody className="g-font-size-default g-color-black" aria-multiselectable="true">
                                        {messages ? messages.map((message, key) => {
                                            return (
                                                <tr key={key}>
                                                    <td className="">
                                                        {message.message}
                                                    </td>
                                                    <td className="">
                                                        {message.created_at ? convertToDateTime(message.created_at) : '-'}
                                                    </td>
                                                </tr>
                                            )
                                        }) : null}
                                    </tbody>
                                </table>

                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )   
    }
}