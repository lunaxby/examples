export const USER_ROLE_ADMIN = 'admin'
export const USER_ROLE_USER = 'user'
export const USER_ROLE_POWER_USER = 'user-power'

export const USER_TITLE_ROLES = {
    [USER_ROLE_ADMIN]: 'DHPI Admin User',
    [USER_ROLE_POWER_USER]: 'Clinic Power User',
    [USER_ROLE_USER]: 'Clinic User'
}