export const FILTER_ORDER = 'orders.id';
export const FILTER_CLINIC = 'orders.clinic_id';
export const FILTER_SHIP_TO = 'orders.ship_to';
export const FILTER_BILL_TO = 'orders.bill_to';
export const FILTER_STATUS = 'sub_orders.status';
export const FILTER_VENDOR = 'sub_orders.vendor_id';
export const FILTER_ORDER_NUMBER = 'orders.number';
export const FILTER_PURCHASE_ORDER_NUMBER = 'sub_orders.purchase_number';
export const FILTER_MANUFACTURER_NUMBER = 'products.manufacturer_number';
export const FILTER_VENDOR_PRODUCT_NUMBER = 'prices.vendor_number';
export const FILTER_ORDER_DATE = 'orders.created_at';

export const FILTER_ORDER_S2K = 'orders.s2k_number';

export const filterNames = {
    [FILTER_ORDER]: 'Order',
    [FILTER_SHIP_TO]: 'Ship To',
    [FILTER_BILL_TO]: 'Bill To',
    [FILTER_STATUS]: 'Status',
    [FILTER_VENDOR]: 'Vendor',
    [FILTER_ORDER_NUMBER]: 'Order #',
    [FILTER_PURCHASE_ORDER_NUMBER]: 'Purchase Order #',
    [FILTER_MANUFACTURER_NUMBER]: 'Manufacturer #',
    [FILTER_VENDOR_PRODUCT_NUMBER]: 'Vendor Product #',
    [FILTER_ORDER_DATE]: 'Order Date'
}