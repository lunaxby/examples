import * as serviceUserActions from '../../common/actions/user'

const overwriteObject = {};
const userActions = { ...serviceUserActions, ...overwriteObject };
export default userActions;