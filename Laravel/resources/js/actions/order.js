import * as serviceOrderActions from '../../common/actions/order'

export const SAVE_ORDER_RESPONSE = 'SAVE_ORDER_RESPONSE';
const saveOrderResponse = (data) => ({
    type: SAVE_ORDER_RESPONSE,
    data,
});
export const saveOrder = (params = {}) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios({
            method: params.id ? 'patch' : 'post',
            url: window.API_URL + (params.id ? `orders/${params.id}` : `orders`),
            data: params,
        })
            .then((response) => {
                if (response.data) {
                    resolve(response.data);
                }
            })
            .catch((error) => {
                dispatch(saveOrderResponse(error.response.data));
                reject(error.response.data);
            });
    });
};

export const CREATE_ORDER = 'CREATE_ORDER';
export const createOrder = () => ({
    type: CREATE_ORDER,
});

export const RESET_ERROR = 'RESET_ERROR';
export const resetError = (data) => ({
    type: RESET_ERROR,
    data
});

export const deleteOrder = (id) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'DELETE',
            url: window.API_URL + `orders/${id}`,
        })
            .then((response) => {
                resolve(response);
            })
            .catch((error) => {
                reject(error.response.data);
            });
    }
    )
};

export const CANCEL_SUBORDERS_RESPONSE = 'CANCEL_SUBORDERS_RESPONSE';
const cancelSubOrdersResponse = (data) => ({
    type: CANCEL_SUBORDERS_RESPONSE,
    data,
});
export const actionCancelSubOrders = (params = {}) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'post',
            url: window.API_URL + `orders/${params.id}/cancel`,
            data: params,
        })
            .then((response) => {
                if (response.data) {
                    resolve(response.data);
                }
            })
            .catch((error) => {
                dispatch(cancelSubOrdersResponse(error.response.data));
                reject(error.response.data);
            });
    });
};

export const CLOSE_SUBORDERS_RESPONSE = 'CLOSE_SUBORDERS_RESPONSE';
const closeSubOrdersResponse = (data) => ({
    type: CLOSE_SUBORDERS_RESPONSE,
    data,
});
export const actionCloseSubOrders = (params = {}) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'post',
            url: window.API_URL + `orders/${params.id}/close`,
            data: params,
        })
            .then((response) => {
                if (response.data) {
                    resolve(response.data);
                }
            })
            .catch((error) => {
                dispatch(closeSubOrdersResponse(error.response.data));
                reject(error.response.data);
            });
    });
};

export const OPEN_SUBORDERS_RESPONSE = 'OPEN_SUBORDERS_RESPONSE';
const openSubOrdersResponse = (data) => ({
    type: OPEN_SUBORDERS_RESPONSE,
    data,
});
export const actionOpenSubOrders = (params = {}) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'post',
            url: window.API_URL + `orders/${params.id}/open`,
            data: params,
        })
            .then((response) => {
                if (response.data) {
                    resolve(response.data);
                }
            })
            .catch((error) => {
                dispatch(openSubOrdersResponse(error.response.data));
                reject(error.response.data);
            });
    });
};

export const EMAIL_SUBORDERS_RESPONSE = 'EMAIL_SUBORDERS_RESPONSE';
const emailSubOrdersResponse = (data) => ({
    type: EMAIL_SUBORDERS_RESPONSE,
    data,
});
export const actionEmailSubOrders = (params = {}) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'post',
            url: window.API_URL + `orders/${params.id}/send-email`,
            data: params,
        })
            .then((response) => {
                if (response.data) {
                    resolve(response.data);
                }
            })
            .catch((error) => {
                dispatch(emailSubOrdersResponse(error.response.data));
                reject(error.response.data);
            });
    });
};

export const SUBMISSION_PAGE_ERROR = 'SUBMISSION_PAGE_ERROR';
export const actionSubmissionPageError = (data) => ({
    type: SUBMISSION_PAGE_ERROR,
    data,
});

export const SUBMISSION_ORDER_RESPONSE = 'SUBMISSION_ORDER_RESPONSE';
const submissionOrderResponse = (data) => ({
    type: SUBMISSION_ORDER_RESPONSE,
    data,
});
export const actionSubmissionOrder = (params = {}) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'post',
            url: window.API_URL + `orders/${params.id}/submit`,
            data: params,
        })
            .then((response) => {
                if (response.data) {
                    resolve(response.data);
                    dispatch(submissionOrderResponse(response.data));
                }
            })
            .catch((error) => {
                dispatch(submissionOrderResponse(Object.assign({}, error.response.data, { hasErrors: true })));
                reject(error.response.data);
            });
    });
};


export const IMPORT_PRODUCTS_RESPONSE = 'IMPORT_PRODUCTS_RESPONSE';
export const importProductsResponse = (data) => ({
    type: IMPORT_PRODUCTS_RESPONSE,
    data,
})
export const actionImportProductFromScanner = (params = {}) => (dispatch) => {
    let formData = new FormData();
    Object.keys(params).forEach((item, index) => {
        if (Array.isArray(params[item])) {
            params[item].forEach(val => {
                formData.append(`${item}[]`, val);
            });
        } else {
            formData.append(item, params[item]);
        }
    });

    if (params.id) {
        formData.append('_method', 'PATCH');
    }

    return new Promise((resolve, reject) => {
        axios({
            method: 'post',
            url: window.API_URL + `orders/import/products`,
            data: formData,
            config: { headers: { 'Content-Type': 'multipart/form-data' } }
        })
            .then((response) => {
                if (response.data) {
                    resolve(response.data);
                    dispatch(importProductsResponse(response.data));
                }
            })
            .catch((error) => {
                dispatch(importProductsResponse(error.response.data));
                reject(error.response.data);
            });
    });
}

export const RECEIVE_LOW_STOCK_INVENTORIES = 'RECEIVE_LOW_STOCK_INVENTORIES';
export const receiveLowStockInventories = (data) => ({
    type: RECEIVE_LOW_STOCK_INVENTORIES,
    data,
})
export const actionFetchLowStockInventories = (params = {}) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios
            .get(window.API_URL + 'inventories', {
                params: {
                    'low_stock': 1,
                    'except_products': params.exceptProducts ? params.exceptProducts : [],
                    ...params,
                }
            })
            .then((response) => {
                if (response.data) {
                    dispatch(receiveLowStockInventories(response.data));
                    resolve(response.data);
                }
            })
            .catch((error) => {
                reject(error.response.data);
            });
    });
};

export const EXPORT_ORDERS_RESPONSE = 'EXPORT_ORDERS_RESPONSE';
export const exportOrdersResponse = (data) => ({
    type: EXPORT_ORDERS_RESPONSE,
    data,
})
export const actionExportOrders = (params = {}) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios
            .get(window.API_URL + 'orders/export', {
                params: params
            })
            .then((response) => {
                if (response.data) {
                    dispatch(exportOrdersResponse(response.data));
                    resolve(response.data);
                }
            })
            .catch((error) => {
                reject(error.response.data);
            });
    });
};

export const PRINT_ORDERS_RESPONSE = 'PRINT_ORDERS_RESPONSE';
export const printOrdersResponse = (data) => ({
    type: PRINT_ORDERS_RESPONSE,
    data,
})
export const actionPrintOrders = (params = {}) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios
            .get(window.API_URL + 'orders/print', {
                params: params
            })
            .then((response) => {
                if (response.data) {
                    dispatch(printOrdersResponse(response.data));
                    resolve(response.data);
                }
            })
            .catch((error) => {
                reject(error.response.data);
            });
    });
};

const orderActions = Object.assign({}, serviceOrderActions, {
    SAVE_ORDER_RESPONSE, saveOrder,
    CREATE_ORDER, createOrder,
    deleteOrder,
    CANCEL_SUBORDERS_RESPONSE, actionCancelSubOrders,
    CLOSE_SUBORDERS_RESPONSE, actionCloseSubOrders,
    OPEN_SUBORDERS_RESPONSE, actionOpenSubOrders,
    EMAIL_SUBORDERS_RESPONSE, actionEmailSubOrders,

    SUBMISSION_PAGE_ERROR, actionSubmissionPageError,
    SUBMISSION_ORDER_RESPONSE, actionSubmissionOrder,

    IMPORT_PRODUCTS_RESPONSE, actionImportProductFromScanner, importProductsResponse,
    RECEIVE_LOW_STOCK_INVENTORIES, actionFetchLowStockInventories, receiveLowStockInventories,

    EXPORT_ORDERS_RESPONSE, exportOrdersResponse, actionExportOrders,
    PRINT_ORDERS_RESPONSE, printOrdersResponse, actionPrintOrders
});
export default orderActions;
