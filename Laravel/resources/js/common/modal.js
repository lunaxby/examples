import Modal from "react-modal";

Modal.customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '80%',
        maxHeight: '100vh',
        // overflow: 'visible'
        overflow: 'scroll'
    }
};

export default Modal;