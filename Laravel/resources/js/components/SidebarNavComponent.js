import React from 'react';
import { NavLink } from 'react-router-dom';
import { isPowerUser, fetchAuthUser, isInventoryMode } from '../actions/auth' 

export default class SidebarNavComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    powerUser() {
        return isPowerUser(this.props.user);
    }

    componentDidMount() {
        const { user } = this.props;

        if (user && user.id) {
            this.props.dispatch(fetchAuthUser({
                userId: user.id,
            }));
        }
    }
    
    render() {
        const { clinic, user } = this.props;

        const renderUserPanel = () => {
            return (
                <ul className="sidebar-elements">
                    {/* <li className="divider">Menu</li> */}
                    <li className="parent open"><a href="#"><i className="icon mdi mdi-home"></i><span>Customer Panel</span></a>
                        <ul className="sub-menu">
                            <li>
                                <NavLink className="nav-link"
                                    activeClassName="active"
                                    to={`/orders`}
                                    exact>Orders
                                </NavLink>
                            </li>

                            {isInventoryMode(clinic) ?
                                <li>
                                    <NavLink className="nav-link"
                                        activeClassName="active"
                                        to={`/inventories`}
                                        exact>Inventory
                                    </NavLink>
                                </li>
                                : null}

                            <li>
                                <NavLink className="nav-link"
                                    activeClassName="active"
                                    to={`/products`}
                                    exact>Products
                                </NavLink>
                            </li>

                            <li>
                                <NavLink className="nav-link"
                                    activeClassName="active"
                                    to={`/vendors`}
                                    exact>Vendors
                                </NavLink>
                            </li>

                            {this.powerUser() ?
                                <li>
                                    <NavLink className="nav-link"
                                        activeClassName="active"
                                        to={`/locations`}
                                        exact>Locations
                                    </NavLink>
                                </li> : ''
                            }

                            {this.powerUser() ?
                                <li>
                                    <NavLink className="nav-link"
                                        activeClassName="active"
                                        to={`/users`}
                                        exact>Manage Users
                                    </NavLink>
                                </li> : ''
                            }

                            <li>
                                <NavLink className="nav-link"
                                    activeClassName="active"
                                    to={`/support`}
                                    exact>Support
                                </NavLink>
                            </li>
                        </ul>
                    </li>
                </ul>
            )
        }

        const renderAnonimusPanel = () => {
            return (
                <ul className="sidebar-elements">
                    <li>
                        <NavLink activeClassName="active" to={`/support`} exact>Support</NavLink>
                    </li>
                </ul>
            )
        }

        return (
            <div className="be-left-sidebar">
                <div className="left-sidebar-wrapper"><a className="left-sidebar-toggle" href="#">DHPI</a>
                    <div className="left-sidebar-spacer">
                        <div className="left-sidebar-scroll">
                            <div className="left-sidebar-content">
                                { user.id ? renderUserPanel() : renderAnonimusPanel() }
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}