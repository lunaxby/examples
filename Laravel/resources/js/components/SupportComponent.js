import React from 'react';
import {sendMail, resetError} from "../actions/support";
import toastr from "../common/toastr";


export default class SupportComponent extends React.Component {
    initialState() {
        const name = this.props.name;
        const email = this.props.email;
        const account = this.props.account;

        return {
            name: name,
            email: email,
            account: account,
            subject: '',
            message: '',
            submiting: false
        };
    }

    constructor(props) {
        super(props);
        this.state = this.initialState();
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.resetErrorMessage(name);

        this.setState({
            [name]: value,
        });
    }

    successMessage(message) {
        toastr.success(message, 'Success');
    }

    errorMessage(message) {
        toastr.error(message, 'Error');
    }

    resetErrorMessage(name) {
        const err = this.props.errors;
        if (err[name]) {
            delete err[name];
        }
        this.props.dispatch(resetError(err));
    }

    async handleSubmit(event) {
        event.preventDefault();
        const { name, email, account, message, subject } = this.state;

        this.setState({ submiting: true });

        try {
            let sendData = {
                name: name,
                email: email,
                account: account,
                subject: subject,
                message: message }

            await this.props.dispatch(sendMail(sendData));
            this.successMessage(`Your message has been sent. We will back to you shortly.`);
            this.setState({ submiting: false, subject: '', message: '' });
        } catch (e) {
            this.errorMessage(e.message ? e.message : `Email was not send`);
        }

    }



    render() {
        const { name, email, account, subject, message } = this.state;
        const { errors } = this.props;
        return (
                <div className="row">
                    <div className="col-lg-12">

                        <div className="card card-border-color card-border-color-primary">

                            <div className="card-header card-header-divider">
                                Contact Us
                            </div>

                            <div className="card-body">
                                <form onSubmit={(e) => { this.handleSubmit(e) }}>

                                    <div className="form-group pt-2 required">
                                        <label htmlFor="inputName">Name</label>
                                        <input
                                            className={`form-control form-control-sm ${errors.name ? ' is-invalid' : ''}`}
                                            type="text"
                                            placeholder="Name"
                                            name="name"
                                            id="inputName"
                                            onChange={(e) => { this.handleInputChange(e) }}
                                            value={name}
                                            autoFocus />
                                        <div className="invalid-feedback">{errors.name && errors.name[0]}</div>
                                    </div>

                                    <div className="form-group pt-2 required">
                                        <label htmlFor="inputEmail">Email</label>
                                        <input
                                            className={`form-control form-control-sm rounded${errors.email ? ' is-invalid' : ''}`}
                                            type="text"
                                            placeholder="Email"
                                            name="email"
                                            id="inputEmail"
                                            onChange={(e) => { this.handleInputChange(e) }}
                                            value={email}
                                            autoFocus />
                                        <div className="invalid-feedback">{errors.email && errors.email[0]}</div>
                                    </div>

                                    <div className="form-group pt-2">
                                        <label htmlFor="inputAccount">Account</label>
                                        <input
                                            className={`form-control form-control-sm rounded`}
                                            type="text"
                                            placeholder="Account"
                                            name="account"
                                            id="inputAccount"
                                            onChange={(e) => { this.handleInputChange(e) }}
                                            value={account}
                                            autoFocus />
                                    </div>
                                    <div className="form-group pt-2 required">
                                        <label htmlFor="inputSubject">Subject</label>
                                        <input
                                            className={`form-control form-control-sm rounded${errors.subject ? ' is-invalid' : ''}`}
                                            type="text"
                                            placeholder="Subject"
                                            name="subject"
                                            id="inputSubject"
                                            onChange={(e) => { this.handleInputChange(e) }}
                                            value={subject}
                                            autoFocus />
                                        <div className="invalid-feedback">{errors.subject && errors.subject[0]}</div>
                                    </div>
                                    <div className="form-group pt-2 required">
                                        <label htmlFor="inputMessage">Message</label>
                                        <textarea
                                            className={`form-control form-control-sm rounded${errors.message ? ' is-invalid' : ''}`}
                                            placeholder="Message"
                                            name="message"
                                            id="inputMessage"
                                            onChange={(e) => { this.handleInputChange(e) }}
                                            value={message}
                                            />
                                        <div className="invalid-feedback">{errors.message && errors.message[0]}</div>
                                    </div>

                                    <div className="row pt-3">
                                        <div className="col-lg-6 pb-4 pb-lg-0"></div>
                                        <div className="col-sm-6">
                                            <p className="text-right">
                                                <button className="btn btn-space btn-primary" type="submit">Send Form</button>
                                            </p>
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}