import React from 'react';

import orderActions from '../../actions/order';
const { fetchOrder, saveOrder, createOrder, actionCancelSubOrders, actionCloseSubOrders, actionOpenSubOrders, actionEmailSubOrders, actionSubmissionOrder } = orderActions;

import { fetchPrice, actionFetchPrices, actionGetRealtimePrices } from '../../actions/price';
import { actionPrintOrders, resetError} from "../../actions/order";

import {fetchAuthUser, isInventoryMode} from '../../actions/auth';

import toastr from "../../common/toastr";
import Modal from "../../common/modal";
import PropTypes from "prop-types";

import Select from 'react-select'
import Skeleton from 'react-loading-skeleton';

import { INPUT_MASK_PRICE } from '../../../common/input'

import OrderAddProductContainer from '../../containers/order/OrderAddProductContainer'
import OrderBuyProductContainer from "../../containers/order/OrderBuyProductContainer";
import OrderPriceListContainer from "../../containers/order/OrderPriceListContainer";

import OrderSubmissionFormContainer from "../../containers/order/OrderSubmissionFormContainer";

import { convertToPrice } from '../../../common/price'
import { getPrice } from '../../../common/order-price'

import {
    ORDER_STATUS_OPEN,
    ORDER_STATUS_SUBMITTED,
    ORDER_STATUS_PARTIALLY_RECEIVED,
    ORDER_STATUS_CLOSED,
    ORDER_STATUS_CANCELED,

    colorizeStatus,
    statusList
} from '../../../common/order-status'

import {
    sortSuborders
} from '../../../common/suborder'

import { convertToDateTime, DATE_FORMAT_DB, convertToDate } from '../../../common/date'
import { fetchLocations, fetchBillToLocations } from '../../actions/location';

import MaskedInputComponent from '../../../common/widgets/MaskedInputComponent'

import DateRangePickerComponent from "../widgets/DateRangePickerComponent";

import printJS from 'print-js'
import SubOrderPrintComponent from './SubOrderPrintComponent';

import moment from 'moment'
import {ORDER_OUTPUT_TEMPLATE_PRINT_LABELS} from "../../../common/order-output-template";
import PrintLabelsComponent from "./report/PrintLabelsComponent";
import swal from "sweetalert";

export default class OrderFormComponent extends React.Component {
    initialState() {
        this.props.dispatch(createOrder());

        return {
            formTitle: '',
            submitBtnText: '',

            id: null,
            number: '',

            sub_orders: [],
            active_sub_order_id: null,
            activeSuborder: null,

            totalProducts: 0,
            subtotal: 0,
            discount: 0,
            shipping: 0,
            misc: 0,
            total: 0,

            shipToLocationObject: null,
            billToLocationObject: null,

            bill_to: null,
            ship_to: null,
            notes: null,

            loading: true,
            submiting: false,
            addPricesInProcess: false,

            modalBuyIsOpen: false,
            buyProductId: '',

            modalOrderSubmissionIsOpen: false,

            isOrderModified: false,

            orderCanBeSubmissioned: false,
            orderCanBeCanceled: false,

            subOrderCanBeSubmissioned: false,
            subOrderCanBeCanceled: false,
            subOrderCanBeClosed: false,
            subOrderCanBeReopened: false,
            subOrderCanBeEmailed: false,
            subOrderCanBePrinted: false,

            canAddItemsToOrder: false,
            canEditInternalNotes: false,
            canChangeLocations: false,
            canPrint: false,

            subOrdersToPrint: [],

            shipToChangesConfirmed: false
        };
    }

    constructor(props) {
        super(props);
        this.state = this.initialState();

        this.statusList = statusList;
    }

    componentWillMount() {
        Modal.setAppElement('body');
    }

    orderPriceList(filterBySuborderId = false) {
        const { sub_orders } = this.state;
        const allOrderPrices = [];

        const filteredSuborders = sub_orders.filter((sub_order) => { return filterBySuborderId ? (sub_order.id == filterBySuborderId) : true })

        filteredSuborders.map((sub_order) => {
            const { order_prices } = sub_order
            if (Array.isArray(order_prices)) {
                order_prices.map((order_price) => {
                    const orderPriceExtended = Object.assign({}, order_price, {
                        sub_order: sub_order,
                    })
                    allOrderPrices.push(orderPriceExtended);
                })
            }
        })

        return allOrderPrices;
    }

    async afterOpenModal() {
        await this.setState({loading: true});

        const { author } = this.props;

        if (author) {
            await this.props.dispatch(fetchAuthUser({
                userId: author.id,
            }));
        }
        if (!this.state.id) {
            this.state.id = this.props.orderId;
        }

        await this.props.dispatch(fetchLocations({ 'all': 1 }));
        await this.props.dispatch(fetchBillToLocations());

        if (this.state.id) {
            this.setState({
                formTitle: '',
                submitBtnText: 'Save Order',
            });
            try {
                await this.props.dispatch(fetchOrder({
                    id: this.state.id,
                }));
                const { order } = this.props;
                await this.setState(order);

                await this.setState({
                    sub_orders: await sortSuborders(this.state.sub_orders)
                })
                await this.calculate();

                if (order.number) {
                    await this.setState({
                        formTitle: `Order: #${order.number}`,
                    });
                }

                try {
                    const billTo = this.props.billToLocations.filter((e) => {
                        return e.id == (order.bill_to ? order.bill_to.id : null)
                    })[0]

                    const shipTo = this.props.locationList.filter((e) => {
                        return e.id == (order.ship_to ? order.ship_to.id : null)
                    })[0]

                    await this.setState({
                        bill_to: billTo ? { value: billTo.id, label: billTo.name } : null,
                        ship_to: shipTo ? { value: shipTo.id, label: shipTo.name } : null,

                        billToLocationObject: billTo,
                        shipToLocationObject: shipTo,
                    });

                    await this.setState({
                        isOrderModified: false,
                    })
                } catch (e) {
                    this.errorMessage(e.message ? e.message : 'Can\'t load Location List');
                }

                try {
                    await this.getRealTimePrices();
                } catch (e) {
                    this.errorMessage(e.message ? e.message : 'Can\'t update Prices');
                }
            } catch (e) {
                this.errorMessage(e.message ? e.message : `Can't load Order`);
            }

        } else {
            await this.setState({
                formTitle: 'Create New Order',
                submitBtnText: 'Save Order',
            });
        }

        await this.syncOrderAvailableActions();
        await this.setState({ loading: false });
    }

    async handleInputChange(event) {
        const target = event.target;
        let value = null;

        switch (target.type) {
            case 'checkbox':
                value = target.checked;
                break;
            case 'file':
                value = target.files[0];
                break;
            default:
                value = target.value;
        }

        const name = target.name;

        this.resetErrorMessage(name);

        if (name == 'ship_to') {
            const { ship_to, shipToChangesConfirmed } = this.state;
            if (!shipToChangesConfirmed && ship_to && ship_to.value != value.value) {
                const willChange = await swal({
                    title: "Are you sure?",
                    text: "Do you really want to change Ship To Location?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                if (!willChange) {
                    return;
                }
                await this.setState({
                    shipToChangesConfirmed: true
                });
            }

            await this.setState({
                [name]: value,
            });

            await this.setState({
                shipToLocationObject: this.props.locationList.filter((e) => {
                    return e.id == value.value
                })[0]
            })

            return;
        }

        await this.setState({
            [name]: value,
        });

        if (name == 'bill_to') {
            await this.setState({
                billToLocationObject: this.props.billToLocations.filter((e) => {
                    return e.id == value.value
                })[0]
            })
        }
    }

    async handleSuborderInputChange(event) {
        const subOrderId = event.subOrderId ? event.subOrderId : this.state.active_sub_order_id;

        const target = event.target;
        let value = null;

        switch (target.type) {
            case 'checkbox':
                value = target.checked;
                break;
            case 'file':
                value = target.files[0];
                break;
            default:
                value = target.value;
        }

        const name = target.name;

        const { sub_orders } = this.state;
        await this.setState({
            sub_orders: sub_orders.map(subOrder => (subOrder.id == subOrderId ? Object.assign({}, subOrder, { [name]: value }) : subOrder))
        });

        this.resetSubOrderErrorMessage(name, subOrderId);

        if (name == 'receive_all') {
            await this.receiveAllOrderPrices(subOrderId, value);
        }

        await this.calculate();
    }

    async receiveAllOrderPrices(subOrderId, value) {
        const { sub_orders } = this.state;

        await this.setState({
            sub_orders: sub_orders.map(subOrder => (subOrder.id == subOrderId ? Object.assign({}, subOrder, { order_prices: subOrder.order_prices.map(orderPrice => {
                return Object.assign({}, orderPrice, {
                    quantity_received: value ? orderPrice.quantity : 0,
                    quantity_to_receive: value ? 0 : orderPrice.quantity
                })
            }) }) : subOrder))
        });
    }

    async setReceiveDateRequiredFlag(subOrderId, receivedDateIsRequired) {
        const { sub_orders } = this.state;
        await this.setState({
            sub_orders: sub_orders.map(subOrder => (subOrder.id == subOrderId ? Object.assign({}, subOrder, { received_date_is_required: receivedDateIsRequired}) : subOrder))
        });
    }

    async handlePrintLabels() {
        const { id, activeSuborder } = this.state;
        try {
            await this.props.dispatch(actionPrintOrders({
                report_type: 'export_' + ORDER_OUTPUT_TEMPLATE_PRINT_LABELS,
                order_id: id,
                vendor_id: activeSuborder ? activeSuborder.vendor_id : null
            }));

            printJS({
                printable: ORDER_OUTPUT_TEMPLATE_PRINT_LABELS,
                type: 'html',
                scanStyles: false,
                header: '',
                css: ['/css/' + ORDER_OUTPUT_TEMPLATE_PRINT_LABELS +'.css'],
            });
        } catch (e) {
            this.errorMessage(e.message ? e.message : 'Order were not printed');
        }
    }

    async handlePrintSuborders(subOrderIds) {
        const { sub_orders, number, created_at, billToLocationObject, shipToLocationObject } = this.state;
        const { clinic } = this.props;
        const subOrdersToPrint = sub_orders.filter(subOrder => { return subOrderIds.find(subOrderId => {
            return subOrderId == subOrder.id
            // return subOrderId != 0
        }) }).map(subOrder => {
            return Object.assign({}, subOrder, {
                order: {
                    number,
                    created_at,
                    billTo: billToLocationObject,
                    shipTo: shipToLocationObject,
                },
                clinic
            })
        })

        await this.setState({
            subOrdersToPrint: subOrdersToPrint,
        })
        if (subOrdersToPrint.length > 0) {
            printJS({
                printable: 'sub-order-print',
                type: 'html',
                scanStyles: false,
                header: '',
                css: ['/css/print_sub_orders.css'],
            });
        }
    }

    async allPricesReadyToSubmission() {
        this.setState({ submiting: true });

        let result = true;

        const { sub_orders } = this.state;
        const priceIds = [];

        sub_orders.map(subOrder => {
            subOrder.order_prices.map(orderPrice => {
                priceIds.push(orderPrice.price_id);
            })
         })

        await this.props.dispatch(actionFetchPrices({
            filter: [{
                field: 'id', 'value': priceIds
            }],
            all: 1
        }))

        const { prices } = this.props;
        const discontinuedPrices = [];

        prices.map(price => {
            if (price.discontinued) {
                result = false;
                discontinuedPrices.push(price);
            }
        })

        if (!result) {
            let errorMessage = 'The following products are discontinued: <br/><br/>';
            discontinuedPrices.map(price => {
                errorMessage += price.product ? '<u>' + price.product.name + '</u><br/>' : ''
            })
            errorMessage += '<br/>Please remove those items from an order to submit the order.'

            this.errorMessage(errorMessage);
        }
        this.setState({ submiting: false });

        return result;
    }

    async handleOpenSubmissionModal(e) {
        e.preventDefault();


        if (await this.allPricesReadyToSubmission()) {
            this.setState({ modalOrderSubmissionIsOpen: true })
        }
    }

    async handleOrderSubmission(submissionData) {
        const { bill_to, ship_to, notes, sub_orders } = submissionData;

        await this.setState({
            bill_to: bill_to,
            ship_to: ship_to,
            notes: notes,
            sub_orders: sub_orders
        })

        if (await this.handleSubmission()) {
            await this.handlePrintSuborders(sub_orders.filter((subOrder) => { return subOrder.print && subOrder.print == 1 }).map(subOrder => { return subOrder.id }));
            await this.handleSetActiveSuborder(null);
        }
    }

    async handleSubmission() {
        const {
            id,
            sub_orders,
            bill_to,
            ship_to,
            notes
        } = this.state;

        this.setState({ submiting: true });
        let success = true;

        try {
            let sendData = {
                id: id,
                sub_orders,
                bill_to: bill_to ? bill_to.value : null,
                ship_to: ship_to ? ship_to.value : null,
                notes
            }

            await this.props.dispatch(actionSubmissionOrder(sendData));

            const { submissionsResultMessages, submissionsResultErrors } = this.props;
            submissionsResultMessages.map((message) => {
                this.successMessage(message);
            })

            submissionsResultErrors.map((error) => {
                this.errorMessage(error);
            })

            await this.reloadOrder();
            this.props.afterSuccess();
        } catch (e) {
            this.errorMessage(e.message ? e.message : `Order was not submitted`);
            success = false;
        }
        this.setState({ submiting: false });

        return success;
    }

    async handleSubmit(event) {
        if (event) {
            event.preventDefault();
        }

        const {
            id,
            sub_orders,
            bill_to,
            ship_to,
            notes
        } = this.state;

        this.setState({ submiting: true });
        let success = true;

        try {
            let sendData = {
                id: id,
                sub_orders,
                bill_to: bill_to ? bill_to.value : null,
                ship_to: ship_to ? ship_to.value : null,
                notes
            }

            const savedOrder = await this.props.dispatch(saveOrder(sendData));
            const savedOrderId = savedOrder.data.id;
            this.successMessage(`Order was ${this.state.id ? 'saved' : 'created'}`);
            await this.setState({ isOrderModified: false })
            if (!id) {
                await this.setState({ id: savedOrderId});
                await this.afterOpenModal();
            } else {
                await this.reloadOrder();
            }
            this.props.afterSuccess();
        } catch (e) {
            const { errors } = this.props;
            if (errors.sub_orders) {
                const subOrderId = Object.keys(errors.sub_orders)[0];
                await this.handleSetActiveSuborder(subOrderId);
            }
            this.errorMessage(e.message ? e.message : `Order was not ${this.state.id ? 'saved' : 'created'}`);
            success = false;
        }
        this.setState({ submiting: false });

        return success;
    }

    async reloadOrder() {
        const { activeSuborder } = this.state;
        await this.afterOpenModal();
        if (activeSuborder) {
            await this.handleSetActiveSuborderByVendorId(activeSuborder.vendor_id);
        }
    }

    successMessage(message) {
        toastr.success(message, 'Success');
    }

    errorMessage(message) {
        toastr.error(message, 'Error');
    }

    resetErrorMessage(name) {
        const err = this.props.errors;
        if (err[name]) {
            delete err[name];
            this.props.dispatch(resetError({ errors: err }));
        }
    }

    resetQtyPriceInputErrorMessage(name,subOrderId, orderPriceId) {
        const err = this.props.errors;
        if (err['sub_orders'] && err['sub_orders'][subOrderId] && err['sub_orders'][subOrderId]['order_prices'] && err['sub_orders'][subOrderId]['order_prices'][orderPriceId] && err['sub_orders'][subOrderId]['order_prices'][orderPriceId][name] ) {
            delete err['sub_orders'][subOrderId]['order_prices'][orderPriceId][name];
            this.props.dispatch(resetError({ errors: err }));
        }
    }

    resetSubOrderErrorMessage(name, subOrderId) {
        const err = this.props.errors;
        if (err['sub_orders'] && err['sub_orders'][subOrderId] && err['sub_orders'][subOrderId][name]) {
            delete err['sub_orders'][subOrderId][name];
            this.props.dispatch(resetError({ errors: err }));
        }
    }

    close() {
        this.setState(this.initialState());
        this.props.closeModal();
    }

    handleSelectedProduct(product) {
        this.setState({
            modalBuyIsOpen: true,
            buyProductId: product.id
        })
    }

    async createSubOrder(vendor) {
        const { id, sub_orders } = this.state;

        const subOrder = {
            id: (new Date).getTime(),
            new: 1,
            actions: {
                canEditQtyOrdered: true,
                canRemoveItems: true,
                canEditQtyToReceive: false,
                canReopen: false,
                canEditVendorNotes: true,
                canEditShippingAndMisc: false,
                canEditItemNotes: true,
                canAddItemsToOrder: true,
                canChangeLocations: true,
                canEditOrderNotes: true,
            },
            order_id: id,
            vendor_id: vendor.id,
            vendor: vendor,
            shipping: 0,
            misc: 0,
            purchase_number: '',
            notes: '',
            status: ORDER_STATUS_OPEN,
            total: 0,
            order_prices: []
        }

        sub_orders.push(subOrder);

        this.setState({
            sub_orders: sub_orders
        });

        return subOrder;
    }

    async addOrderPriceToSuborder(priceData, sub_order) {
        // console.log('addOrderPriceToSuborder priceData: ', priceData);
        // console.log('addOrderPriceToSuborder sub_order: ', sub_order);
        let order_prices = Array.isArray(sub_order.order_prices) ? sub_order.order_prices : [];
        const { priceEntity } = priceData;

        const alreadyExistedPrice = order_prices.filter((order_price) => {
            return priceEntity.id == order_price.price_entity.id;
        })[0];
        // console.log('addOrderPriceToSuborder alreadyExistedPrice: ', alreadyExistedPrice);

        let new_order_price = {};
        if (alreadyExistedPrice) {
            alreadyExistedPrice.quantity += priceData.quantity;
        } else {
            new_order_price = {
                id: (new Date).getTime(),
                new: 1,
                price_entity: priceEntity,
                order_id: sub_order.order_id,
                price_id: priceEntity.id,
                sub_order_id: sub_order.id,
                price: priceEntity.price,
                quantity: priceData.quantity,
                quantity_received: 0,
                note: '',
            }
            order_prices.push(new_order_price);
        }
        sub_order.order_prices = order_prices;

        await this.setState({
            sub_orders: this.state.sub_orders
        })

        // console.log('addOrderPriceToSuborder state: ', this.state.sub_orders);

        return alreadyExistedPrice ? alreadyExistedPrice : new_order_price;
    }

    async handleSelectedMultiplePrices(prices, additionalInfo) {
        const { addPricesInProcess } = this.state;
        if (addPricesInProcess) {
            return
        }
        let added = 0;

        this.setState({ addPricesInProcess: true })
        const result = await Promise.all(prices.map(async (price) => {
            const result = await this.handleSelectedPriceData(price, false);
            if (result) {
                added++
            }
        }));
        if (added > 0) {
            this.successMessage('You have successfully added '+ added +' product(s) to the order')
            if (additionalInfo) {
                // came from low stock product
                const { location } = additionalInfo;
                if (location) {
                    await this.handleInputChange({ target: { name: 'ship_to', type: 'select', value: { value: location.id, label: location.name } } })
                }
            }
        }
        await this.setState({
            sub_orders: await sortSuborders(this.state.sub_orders)
        })
        this.setState({ addPricesInProcess: false })
    }

    async handleSelectedPriceData(priceData, needToSortSuborders = true) {
        const { price_id, quantity } = priceData;

        try {
            await this.props.dispatch(fetchPrice({
                id: price_id,
            }));
            const { selectedPrice } = this.props;
            const { sub_orders } = this.state;
            const { vendor } = selectedPrice;

            let subOrderForPrice = sub_orders.find((sub_order) => {
                return sub_order.vendor && sub_order.vendor.id == selectedPrice.vendor.id;
            })

            if (!subOrderForPrice) {
                subOrderForPrice = await this.createSubOrder(vendor);
            } else {
                const { actions: { canAddItemsToOrder } } = subOrderForPrice;
                if (!canAddItemsToOrder) {
                    this.errorMessage('You can\'t add selected Product to Vendor "' + vendor.name + '"');
                    return false;
                }
            }

            if (subOrderForPrice.actions.canReopen) {
                const willReopen = await swal({
                    title: "Are you sure?",
                    text: "Order to this vendor is canceled. Do you want to reopen this order?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                if (!willReopen) {
                    return false;
                }
                await this.reopenSuborders([subOrderForPrice.id]);
                await this.reloadOrder();
             // relink after reload
                subOrderForPrice = this.state.sub_orders.find((sub_order) => {
                    return sub_order.vendor && sub_order.vendor.id == selectedPrice.vendor.id;
                })
            }

            await this.addOrderPriceToSuborder({
                priceEntity: selectedPrice,
                quantity,
            }, subOrderForPrice);

            if (needToSortSuborders) {
                await this.setState({
                    sub_orders: await sortSuborders(this.state.sub_orders)
                })
            }
            await this.calculate();
            await this.syncOrderAvailableActions();

            return true;
        } catch (e) {
            this.errorMessage(e.message ? e.message : `Can't load Price`);
            return false;
        }
    }

    async syncOrderAvailableActions(subOrderId) {
        if (subOrderId == undefined) {
            subOrderId = this.state.active_sub_order_id;
        }

        await this.setState({
            subOrderCanBeSubmissioned: await this.subOrderCanBeSubmissioned(subOrderId),
            subOrderCanBeCanceled: await this.subOrderCanBeCanceled(subOrderId),
            subOrderCanBeClosed: await this.subOrderCanBeClosed(subOrderId),
            subOrderCanBeReopened: await this.subOrderCanBeReopened(subOrderId),
            subOrderCanBeEmailed: await this.subOrderCanBeEmailed(subOrderId),
            subOrderCanBePrinted: await this.subOrderCanBePrinted(subOrderId),
        })

        await this.setState({
            orderCanBeCanceled: await this.orderCanBeCanceled(),
            orderCanBeSubmissioned: await this.orderCanBeSubmissioned(),

            canAddItemsToOrder: await this.ruleCanAddItemsToOrder(),
            canEditInternalNotes: await this.ruleCanEditInternalNotes(),
            canChangeLocations: await this.ruleCanChangeLocations(),
            canPrint: await this.ruleCanPrint(),
        })
    }

    async handleSetActiveSuborderByVendorId(vendorId) {
        const { sub_orders } = this.state;
        const activeSubOrder = sub_orders.find((sub_order) => { return sub_order.vendor_id == vendorId });
        if (activeSubOrder) {
            await this.handleSetActiveSuborder(activeSubOrder.id);
        }
    }

    async handleSetActiveSuborder(sub_order_id) {
        const { sub_orders } = this.state;

        await this.setState({
            active_sub_order_id: sub_order_id,
            activeSuborder: sub_orders.find((sub_order) => { return sub_order.id == sub_order_id })
        })

        await this.syncOrderAvailableActions();
    }

    async getRealTimePrices() {
        let { sub_orders } = this.state;
        let index = null;

        sub_orders.map((sub_order, i) => {
            if (sub_order.vendor.dhpi === 1 && sub_order.status === "open") {
                index = i;
            }
        });

        if (index === null || (sub_orders[index].order_prices && sub_orders[index].order_prices.length === 0)) {
            return null;
        }

        toastr.warning('Getting prices from S2K', 'Please wait', {
            timeOut: 0
        });

        try {
            const response = await this.props.dispatch(actionGetRealtimePrices(sub_orders[index].order_prices));
            if (!response.prices) {
                throw new Error(response.error ? response.error : 'Some errors');
            }

            sub_orders[index].order_prices = response.prices;

            await this.setState({
                sub_orders: sub_orders
            });

            toastr.clear();
            if (response.message.length) {
                this.successMessage(response.message);
            }
        } catch (e) {
            toastr.clear();
            this.errorMessage(e.message ? e.message : 'Don\'t get prices');
        }
    }

    async calculate() {
        const { sub_orders } = this.state;

        let totalProducts = 0;
        let subtotal = 0;

        // TODO after DHPI sync
        let discount = 0;

        let shipping = 0;
        let misc = 0;
        let total = 0;

        sub_orders.map((sub_order, i) => {
            const { order_prices } = sub_order;
            let sub_order_total = 0;

            if (Array.isArray(order_prices)) {
                order_prices.map((order_price, i) => {
                    const order_price_value = getPrice(order_price);
                    order_prices[i].price = order_price_value;
                    let order_price_total = Number(order_price.quantity) * Number(order_price_value);

                    sub_order_total += order_price_total;
                    totalProducts += Number(order_price.quantity);
                })
            }
            sub_orders[i].order_prices = order_prices;
            sub_orders[i].total = sub_order_total;

            subtotal += sub_order_total;
            if (order_prices.length > 0) {
                shipping += Number(sub_order.shipping);
                misc += Number(sub_order.misc);
            }
        })

        total = subtotal - discount + misc + shipping;

        await this.setState({
            sub_orders: sub_orders,

            totalProducts,
            subtotal,
            discount,
            shipping,
            misc,
            total,
        })
    }

    async handleChangeOrderPrice(orderPriceId, field, value, recalculate = true) {
        const { sub_orders } = this.state;
        // const sub_order = sub_orders.find(async (sub_order) => {
        //     const { order_prices } = sub_order;
        //     if (Array.isArray(order_prices)) {
        //         const orderPriceIndex = order_prices.findIndex((order_price) => {
        //             return order_price.id == Number(orderPriceId)
        //         })
        //         if (orderPriceIndex != -1) {
        //             return;
        //         }
        //     }
        // })
        // if (sub_order && sub_order.id) {
        //     await this.resetQtyPriceInputErrorMessage(field, sub_order.id, orderPriceId);
        // }
        sub_orders.map(async (sub_order, i) => {
            const { order_prices } = sub_order;

            if (Array.isArray(order_prices)) {
                const orderPriceIndex = order_prices.findIndex((order_price) => {
                    return order_price.id == Number(orderPriceId)
                })
                if (orderPriceIndex != -1) {
                    order_prices[orderPriceIndex][field] = field == 'quantity' && !value ? 1 : value;
                    if (field == 'quantity_received') {
                        const qty = order_prices[orderPriceIndex].quantity;
                        const qty_received = value > qty ? qty : value;

                        order_prices[orderPriceIndex].quantity_received = qty_received;
                        order_prices[orderPriceIndex].quantity_to_receive = qty - qty_received;
                    }
                }
            }
        });

        await this.setState({
            sub_orders: sub_orders
        })

        if (recalculate) {
            await this.calculate();
        }
    }

    async handleDeleteOrderPrice(orderPriceId) {
        const { sub_orders } = this.state;
        sub_orders.map((sub_order, i) => {
            let { order_prices } = sub_order;

            if (Array.isArray(order_prices)) {
                const orderPriceFiltered = order_prices.filter((order_price) => {
                    return order_price.id != orderPriceId
                })

                order_prices = orderPriceFiltered;
            }
            sub_orders[i].order_prices = order_prices;
            if (order_prices.length == 0) {
                this.setState({
                    active_sub_order_id: null
                })
            }
        })

        await this.setState({
            sub_orders: sub_orders
        })
        await this.calculate();
    }

    async handleChangeNote(orderPriceId, note) {
        const { sub_orders } = this.state;
        sub_orders.map((sub_order, i) => {
            const { order_prices } = sub_order;

            if (Array.isArray(order_prices)) {
                const orderPriceIndex = order_prices.findIndex((order_price) => {
                    return order_price.id == Number(orderPriceId)
                })
                if (orderPriceIndex != -1) {
                    order_prices[orderPriceIndex].note = note;
                }
            }
        })

        await this.setState({
            sub_orders: sub_orders
        })
    }

    checkIsModifiedOrder(prevState) {
        let isOrderModified = prevState.isOrderModified;
        if (isOrderModified) {
            return;
        }

        const _checker = (prevObject, currentObject, field, type) => {
            let compare1 = prevObject[field];
            let compare2 = currentObject[field];
            switch (type) {
                case 'String':
                    compare1 = String(compare1 === null ? '' : compare1);
                    compare2 = String(compare2 === null ? '' : compare2);
                    break;
                case 'Number':
                    compare1 = Number(compare1).toFixed(2);
                    compare2 = Number(compare2).toFixed(2);
                    break;
            }

            if (compare1 != compare2) {
                this.setState({isOrderModified: true});
            }
        }

        const changeListToListen = {
            'sub_orders': 'Array',
            'totalProducts': 'Number',
            'subtotal': 'Number',
            'discount': 'Number',
            'shipping': 'Number',
            'misc': 'Number',
            'total': 'Number',
            'bill_to': 'Object',
            'ship_to': 'Object',
            'notes': 'String'
        };
        Object.keys(changeListToListen).map((field) => {
            if (field != 'sub_orders') {
                _checker(prevState, this.state, field, changeListToListen[field]);
            } else {
                const subOrderChangeListToListen = {
                    'shipping': 'Number',
                    'misc': 'Number',
                    'received_at': 'String',
                    'notes': 'String',
                    'purchase_number': 'String'
                };
                const { sub_orders } = this.state;
                sub_orders.map(subOrder => {
                    Object.keys(subOrderChangeListToListen).map(subOrderField => {
                        const prevSubOrder = prevState.sub_orders.find(el => { return el.id == subOrder.id });
                        if (!prevSubOrder) {
                            this.setState({isOrderModified: true});
                        } else {
                            _checker(prevSubOrder, subOrder, subOrderField, subOrderChangeListToListen[subOrderField]);
                        }
                    })
                })
            }
        })
    }

    componentDidUpdate(prevProps, prevState) {
        this.checkIsModifiedOrder(prevState);
    }

    async findSuborder(subOrderId) {
        const  { sub_orders } = this.state;
        return sub_orders.find((sub_order) => { return sub_order.id == subOrderId})
    }

    async setSubordersStatus(subOrdersId, status) {
        const { sub_orders } = this.state;

        subOrdersId.map((subOrderId) => {
            const subOrderIndex = sub_orders.findIndex((sub_order) => { return sub_order.id == subOrderId });
            if (subOrderIndex != -1) {
                sub_orders[subOrderIndex].status = status;
            }
        })

        await this.setState({
            sub_orders: sub_orders
        });

        await this.syncOrderAvailableActions();
    }

    async handleEmailSuborder() {
        const { activeSuborder, id } = this.state;
        await this.emailSuborders([activeSuborder.id]);
    }

    async emailSuborders(subOrdersIds) {
        this.setState({ submiting: true });

        let suborderVendorName = ''
        if (subOrdersIds.length == 1) {
            const subOrderToCancel = await this.findSuborder(subOrdersIds[0]);
            if (subOrderToCancel) {
                const { vendor } = subOrderToCancel;
                if (vendor) {
                    suborderVendorName = vendor.name;
                }
            }
        }
        try {
            const { id } = this.state;
            let sendData = {
                id,
                sub_orders: subOrdersIds
            }

            const results = await this.props.dispatch(actionEmailSubOrders(sendData));
            const { errors, messages } = results;
            if (errors) {
                errors.map((error) => {
                    this.errorMessage(error);
                })
            }
            if (messages) {
                messages.map((message) => {
                    this.successMessage(message);
                })
            }
            // this.successMessage(!suborderVendorName ? `Emails were sent` : `Email was sent to sub-order vendor ${suborderVendorName}`);
            // this.props.afterSuccess();
        } catch (e) {
            const errorMessage = suborderVendorName ? `Email was not sent to sub-order vendor ${suborderVendorName}` : `Emails were not sent`
            this.errorMessage(e.message ? e.message : errorMessage);
        }
        this.setState({ submiting: false });
    }

    async cancelSuborders(subOrdersIds) {
        this.setState({ submiting: true });

        let suborderVendorName = ''
        if (subOrdersIds.length == 1) {
            const subOrderToCancel = await this.findSuborder(subOrdersIds[0]);
            if (subOrderToCancel) {
                const { vendor } = subOrderToCancel;
                if (vendor) {
                    suborderVendorName = vendor.name;
                }
            }
        }
        try {
            const { id } = this.state;
            let sendData = {
                id,
                sub_orders: subOrdersIds
            }

            const results = await this.props.dispatch(actionCancelSubOrders(sendData));
            const { errors, message } = results;
            if (errors) {
                errors.map((error) => {
                    this.errorMessage(error);
                })
            }
            if (message) {
                message.map((text) => {
                    this.successMessage(text);
                })
                this.props.afterSuccess();
            }
        } catch (e) {
            const errorMessage = suborderVendorName ? `Sub-order to ${suborderVendorName} was not canceled` : `Order was not canceled`
            this.errorMessage(e.message ? e.message : errorMessage);
        }
        this.setState({ submiting: false });
    }

    async closeSuborders(subOrdersIds) {
        this.setState({ submiting: true });

        let suborderVendorName = ''
        if (subOrdersIds.length == 1) {
            const subOrderToCancel = await this.findSuborder(subOrdersIds[0]);
            if (subOrderToCancel) {
                const { vendor } = subOrderToCancel;
                if (vendor) {
                    suborderVendorName = vendor.name;
                }
            }
        }
        try {
            const { id } = this.state;
            let sendData = {
                id,
                sub_orders: subOrdersIds
            }

            const results = await this.props.dispatch(actionCloseSubOrders(sendData));
            const { errors, message } = results;
            if (errors) {
                errors.map((error) => {
                    this.errorMessage(error);
                })
            }
            if (message) {
                message.map((text) => {
                    this.successMessage(text);
                })
                this.props.afterSuccess();
            }
        } catch (e) {
            const errorMessage = suborderVendorName ? `Sub-order to ${suborderVendorName} was not closed` : `Order was not closed`
            this.errorMessage(e.message ? e.message : errorMessage);
        }
        this.setState({ submiting: false });
    }

    async reopenSuborders(subOrdersIds) {
        this.setState({ submiting: true });

        let suborderVendorName = ''
        if (subOrdersIds.length == 1) {
            const subOrderToReopen = await this.findSuborder(subOrdersIds[0]);
            if (subOrderToReopen) {
                const { vendor } = subOrderToReopen;
                if (vendor) {
                    suborderVendorName = vendor.name;
                }
            }
        }
        try {
            const { id } = this.state;
            let sendData = {
                id,
                sub_orders: subOrdersIds
            }

            const results = await this.props.dispatch(actionOpenSubOrders(sendData));
            const { errors, message } = results;
            if (errors) {
                errors.map((error) => {
                    this.errorMessage(error);
                })
            }
            if (message) {
                message.map((text) => {
                    this.successMessage(text);
                })
                this.props.afterSuccess();
            }
        } catch (e) {
            const errorMessage = suborderVendorName ? `Sub-order to ${suborderVendorName} was not re-opened` : `Order was not re-opened`
            this.errorMessage(e.message ? e.message : errorMessage);
        }
        this.setState({ submiting: false });
    }

    async handleCancelSuborder() {
        const willCancel = await swal({
            title: "Are you sure?",
            text: "Do you really want to cancel this sub-order?",
            icon: "warning",
            buttons: ['No', 'Yes'],
            dangerMode: true,
        })
        if (!willCancel) {
            return;
        }

        const { activeSuborder, id } = this.state;
        await this.cancelSuborders([activeSuborder.id]);
        await this.reloadOrder();
    }

    async handleCloseSuborder() {
        const willClose = await swal({
            title: "Are you sure?",
            text: "Do you really want to close this sub-order?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        if (!willClose) {
            return;
        }

        const { activeSuborder, id } = this.state;
        await this.closeSuborders([activeSuborder.id]);
        await this.reloadOrder();
    }

    async handleReopenSuborder() {
        const willClose = await swal({
            title: "Are you sure?",
            text: "Do you really want to re-open this sub-order?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        if (!willClose) {
            return;
        }

        const { activeSuborder } = this.state;
        await this.reopenSuborders([activeSuborder.id]);
        await this.reloadOrder();
    }

    async orderCanBeCanceled() {
        const { sub_orders, id } = this.state;
        if (!id) {
            return false;
        }

        const orderCanBeCanceled = await Promise.all(sub_orders.map(async (subOrder) => {
            const subOrderCanBeCanceled = await this.subOrderCanBeCanceled(subOrder.id);
            if (subOrder.order_prices.length > 0 && subOrderCanBeCanceled) {
                return true;
            }

            return false;
        }))

        return orderCanBeCanceled.length > 0 && Math.max(...orderCanBeCanceled);
    }

    async orderCanBeSubmissioned() {
        const { sub_orders, id } = this.state;
        if (!id) {
            return false;
        }

        const orderCanBeSubmissioned = await Promise.all(sub_orders.map(async (subOrder) => {
            const subOrderCanBeSubmissioned = await this.subOrderCanBeSubmissioned(subOrder.id);
            if (subOrder.order_prices.length > 0 && subOrderCanBeSubmissioned) {
                return true;
            }

            return false;
        }))

        return orderCanBeSubmissioned.length > 0 && Math.max(...orderCanBeSubmissioned);
    }

    async subOrderCanBeCanceled(subOrderId) {
        const { id } = this.state;
        const subOrder = await this.findSuborder(subOrderId);

        return id && subOrder && subOrder.actions.canCancel;
    }

    async subOrderCanBeSubmissioned(subOrderId) {
        const { id } = this.state;
        const subOrder = await this.findSuborder(subOrderId);

        return id && subOrder && subOrder.actions.canSubmit;
    }

    async subOrderCanBeReopened(subOrderId) {
        const { id } = this.state;
        const subOrder = await this.findSuborder(subOrderId);
        // console.log('subOrderCanBeReopened subOrder: ', subOrder);
        return id && subOrder && subOrder.actions.canReopen;
    }

    async subOrderCanBeClosed() {
        const { activeSuborder, id } = this.state;
        return id && activeSuborder && activeSuborder.actions.canClose;
    }

    async subOrderCanBeEmailed() {
        const { activeSuborder, id } = this.state;
        return id && activeSuborder && activeSuborder.actions.canPrintOrEmail;
    }

    async subOrderCanBePrinted() {
        return await this.subOrderCanBeEmailed();
    }

    async isOneSubOrderAtLeastGetAttributeAsTrue(attributeName) {
        const { sub_orders } = this.state;
        return sub_orders.filter(subOrder => {
            return subOrder.order_prices.length > 0 && subOrder.actions[attributeName]
        }).length > 0;
    }

    async primarySubOrder() {
        const { sub_orders } = this.state;
        return sub_orders.find(subOrder => {
            return subOrder.order_prices.length > 0
        })
    }

    async ruleCanAddItemsToOrder() {
        const primarySubOrder = await this.primarySubOrder();
        return !primarySubOrder || this.isOneSubOrderAtLeastGetAttributeAsTrue('canAddItemsToOrder');
    }

    async ruleCanEditInternalNotes() {
        const primarySubOrder = await this.primarySubOrder();
        return !primarySubOrder || this.isOneSubOrderAtLeastGetAttributeAsTrue('canEditOrderNotes');
    }

    async ruleCanChangeLocations() {
        const primarySubOrder = await this.primarySubOrder();
        return !primarySubOrder || this.isOneSubOrderAtLeastGetAttributeAsTrue('canChangeLocations');
    }

    async ruleCanPrint() {
        return this.isOneSubOrderAtLeastGetAttributeAsTrue('canPrintOrEmail');
    }

    async handleCancelOrder(e) {
        e.preventDefault();
        const { orderCanBeCanceled } = this.state;

        if (!orderCanBeCanceled) {
            return;
        }

        const willCancel = await swal({
            title: "Are you sure?",
            text: "Do you really want to cancel Order?",
            icon: "warning",
            buttons: ['No', 'Yes'],
            dangerMode: true,
        })
        if (!willCancel) {
            return;
        }

        const { sub_orders } = this.state;
        await this.cancelSuborders(sub_orders.map((sub_order) => { return sub_order.id }));
        await this.reloadOrder();
    }

    render() {
        const {
            id,
            number,
            author,
            created_at,
            notes,
            bill_to,
            billToLocationObject,

            ship_to,
            shipToLocationObject,

            status,
            sub_orders,

            totalProducts,
            subtotal,
            discount,
            shipping,
            misc,
            total,

            active_sub_order_id,
            activeSuborder,
            orderCanBeCanceled,
            orderCanBeSubmissioned,

            canAddItemsToOrder,
            canEditInternalNotes,
            canChangeLocations,
            canPrint,

            isOrderModified,

            formTitle, submitBtnText, loading, submiting
        } = this.state;
        const { errors, clinic, locationList, billToLocations } = this.props;

        const orderPrices = this.orderPriceList(active_sub_order_id);
        const createMode = !id;

        // console.log('order state: ', this.state);

        const renderOrderTotal = (cssClass) => {
            return (
                <div className={`card${cssClass ? ' ' + cssClass : ''}`}>
                    <div className="card-header">
                        <span className="text-uppercase">Order</span>
                    </div>
                    <div className="card-body">
                        <div className="row mb-1">
                            <div className="col-sm-6">Total Products</div>
                            <div className="col-sm-6 text-right">{totalProducts}</div>
                        </div>
                        <div className="row mb-1">
                            <div className="col-sm-6">Subtotal</div>
                            <div className="col-sm-6 text-right">{convertToPrice(subtotal)}</div>
                        </div>
                        {discount ?
                            <div className="row mb-1">
                                <div className="col-sm-6">Discount</div>
                                <div className="col-sm-6 text-right">{convertToPrice(discount)}</div>
                            </div> :
                            <div></div>
                        }
                        {shipping ?
                            <div className="row mb-1">
                                <div className="col-sm-6">Shipping/Handling</div>
                                <div className="col-sm-6 text-right">{convertToPrice(shipping)}</div>
                            </div> :
                            <div></div>
                        }
                        {misc ?
                            <div className="row mb-1">
                                <div className="col-sm-6">Misc</div>
                                <div className="col-sm-6 text-right">{convertToPrice(misc)}</div>
                            </div> :
                            <div></div>
                        }
                        <div className="row">
                            <div className="col-sm-6"><h4>Order Total</h4></div>
                            <div className="col-sm-6 text-right"><h4>{convertToPrice(total)}</h4></div>
                        </div>
                    </div>
                </div>
            )
        }

        const renderOrderInformation = () => {
            const locationPlaceholder = '-- Select Location --';
            const locationOptions = locationList.map(location => ({ value: location.id, label: location.name }));
            const billTolocationOptions = billToLocations.map(location => ({ value: location.id, label: location.name }));

            const getLocationAddress = (location) => {
                if (!location) {
                    return null;
                }

                const addresses = [];
                for (let i = 1; i <= 3; i++) {
                    if (location["address_" + i]) {
                        addresses.push(location["address_" + i]);
                    }
                }

                return {
                    'address': addresses.join(', '),
                    'city': location.city,
                    'state': location.state,
                    'zip': location.zip,
                };
            }

            const shipToAddress = getLocationAddress(shipToLocationObject);
            const billToAddress = getLocationAddress(billToLocationObject);

            const renderLocationSelector = (locationVariableName) => {
                const address = locationVariableName == 'bill_to' ? billToAddress : shipToAddress;

                return (
                    <div className="form-group row pt-0 required">
                        <div className="col-sm-12">
                            <Select
                                className={`react-select${errors[locationVariableName] ? ' is-invalid' : ''}`}
                                options={locationVariableName == 'bill_to' ? billTolocationOptions : locationOptions}
                                onChange={(selectedItem) => { this.handleInputChange({ target: { name: locationVariableName, type: 'select', value: selectedItem } }) }}
                                placeholder={locationPlaceholder}
                                required
                                isDisabled={!canChangeLocations}
                                isSearchable={true}
                                value={this.state[locationVariableName]}
                            />
                            <div className="invalid-feedback">{errors[locationVariableName] && errors[locationVariableName][0]}</div>
                            {address ?
                                <div className="location-address">
                                    {address.address ? <span>{address.address} <br /></span> : null}
                                    {address.city || address.state ? <span>{address.city} {address.state}<br /></span> : null}
                                    {address.zip ? <span>{address.zip} <br /></span> : null}
                                </div>
                                : ''}
                        </div>
                    </div>
                )
            }

            return (
                <div className="card mb-0">
                    <div className="card-header">
                        <span className="text-uppercase">Information</span>
                    </div>
                    <div className="card-body">
                        <div className="row mb-1">
                            <div className="col-sm-3">Clinic</div>
                            <div className="col-sm-9">{ clinic ? clinic.name : '' }</div>
                        </div>
                        {!createMode ?
                            <div className="row mb-1">
                                <div className="col-sm-3">Created by</div>
                                <div className="col-sm-9">{author ? author.name : ''}</div>
                            </div> : null
                        }
                        {!createMode ?
                            <div className="row mb-1">
                                <div className="col-sm-3">Created On</div>
                                <div className="col-sm-9">{created_at ? convertToDateTime(created_at) : ''}</div>
                            </div> : null
                        }
                        <div className="row mb-1">
                            <div className="col-sm-3 form-group required">
                                <label>Bill to</label>
                            </div>
                            <div className="col-sm-9">

                                { renderLocationSelector('bill_to') }

                            </div>
                        </div>
                        <div className="row mb-1">
                            <div className="col-sm-3 form-group required">
                                <label>Ship to</label>
                            </div>
                            <div className="col-sm-9">

                                {renderLocationSelector('ship_to')}

                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-3">Internal Notes</div>
                            <div className="col-sm-9">

                                <div className="form-group row">
                                    <div className="col-sm-12">
                                        <textarea rows={8}
                                            readOnly={!canEditInternalNotes}
                                            id="textarea-order-note"
                                            className={`form-control form-control-sm rounded${errors.notes ? ' is-invalid' : ''}`}
                                            type="text"
                                            name="notes"
                                            placeholder={canEditInternalNotes ? "Type Note here" : ''}
                                            onChange={(e) => { this.handleInputChange(e) }}
                                            value={notes || ''}
                                        />
                                        <div className="invalid-feedback">{errors.notes && errors.notes[0]}</div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            );
        }

        const renderSuborderActions = () => {
            const { isOrderModified, active_sub_order_id, subOrderCanBeCanceled, subOrderCanBeClosed, subOrderCanBeReopened, subOrderCanBeEmailed, subOrderCanBePrinted } = this.state;
            const showActionBtns = active_sub_order_id && !createMode;

            return (
                showActionBtns ?
                    <div>
                        <div className= "btn-group" role="group" aria-label="Sub-order Actions">
                            {subOrderCanBeEmailed ? <button type="button" disabled={isOrderModified} onClick={() => { this.handleEmailSuborder() }} className="btn btn-secondary">Send by Email</button> : ''}
                            {subOrderCanBePrinted ? <button type="button" onClick={() => { this.handlePrintSuborders([active_sub_order_id]) }} disabled={isOrderModified} className="btn btn-secondary">Print</button> : ''}
                            {subOrderCanBeCanceled ? <button type="button" onClick={() => { this.handleCancelSuborder() }} disabled={isOrderModified} className="btn btn-secondary">Cancel</button> : ''}
                            {subOrderCanBeReopened ? <button type="button" onClick={() => { this.handleReopenSuborder() }} disabled={isOrderModified} className="btn btn-secondary">Re-Open</button> : ''}
                            {subOrderCanBeClosed ? <button type="button" onClick={() => { this.handleCloseSuborder() }} disabled={isOrderModified} className="btn btn-secondary">Close</button> : ''}
                        </div>
                        {isOrderModified ? <div className="prompt-msg">*Please save the order first</div> : ''}
                    </div>
                : ''
            )
        }

        const renderSuborderFields = () => {
            const { active_sub_order_id, sub_orders } = this.state;

            const activeSuborder = sub_orders.find((sub_order) => { return sub_order.id == active_sub_order_id })

            if (!activeSuborder) {
                return null;
            }

            const { errors } = this.props;
            const { misc, shipping, total, received_at, receive_all, notes, purchase_number, id, received_date_is_required } = activeSuborder;

            let subOrderErrors = {};
            if (errors.sub_orders && errors.sub_orders[activeSuborder.id]) {
                subOrderErrors = errors.sub_orders[activeSuborder.id];
            }

            const receiptTotal = convertToPrice(Number(total ? total : 0) + Number(misc ? misc : 0) + Number(shipping ? shipping : 0));

            return (
                <div className="row text-left mt-2">
                    <div className="col-lg-4 col-md-12">
                        <div className="row mb-3">
                            <div className="col-sm-12">
                                <h4>Receipt Total {receiptTotal }</h4>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">

                                <div className="form-group row mb-2">
                                    <label className="col-lg-5 col-md-4 col-form-label">Shipping/Handling</label>
                                    <div className="col-lg-7 col-md-8">
                                        <MaskedInputComponent
                                            id={id}
                                            readOnly={!activeSuborder.actions.canEditShippingAndMisc}
                                            className={`form-control form-control-sm rounded${subOrderErrors.shipping ? ' is-invalid' : ''}`}
                                            options={INPUT_MASK_PRICE}
                                            type="text"
                                            placeholder="Type Shipping/Handling here"
                                            name="shipping"
                                            onChange={(e) => {
                                                    // console.info('change shipping e: ', e);
                                                    this.handleSuborderInputChange(e)
                                                }
                                            }
                                            value={shipping || 0}
                                        />
                                        <div className="invalid-feedback">{subOrderErrors.shipping && subOrderErrors.shipping[0]}</div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">

                                <div className="form-group row mb-2">
                                    <label className="col-lg-5 col-md-4 col-form-label">Misc</label>
                                    <div className="col-lg-7 col-md-8">
                                        <MaskedInputComponent
                                            id={id}
                                            readOnly={!activeSuborder.actions.canEditShippingAndMisc}
                                            className={`form-control form-control-sm rounded${subOrderErrors.misc ? ' is-invalid' : ''}`}
                                            options={INPUT_MASK_PRICE}
                                            type="text"
                                            placeholder="Type Misc here"
                                            name="misc"
                                            onChange={(e) => {
                                                    this.handleSuborderInputChange(e)
                                                }
                                            }
                                            value={misc || 0}
                                        />
                                        <div className="invalid-feedback">{subOrderErrors.misc && subOrderErrors.misc[0]}</div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-12">
                        <div className="row">
                            <div className="col-sm-12">

                                <div className={`form-group row${received_date_is_required ? ' required' : ''}`}>
                                    <label className="col-sm-4 col-form-label">Receive Date</label>
                                    <div className="col-sm-8">
                                        {activeSuborder.actions.canEditReceiveDate ?

                                            <DateRangePickerComponent
                                                config={{
                                                    opens: 'left',
                                                    singleDatePicker: true,
                                                    showDropdowns: true,
                                                    ranges: {},
                                                    showCustomRangeLabel: false,
                                                    startDate: received_at ? moment(received_at) : moment(),
                                                    endDate: received_at ? moment(received_at) : moment(),
                                                }}
                                                onApplyCallback={(startDate, endDate) => {
                                                    this.handleSuborderInputChange({
                                                        target: {
                                                            type: 'text',
                                                            value: startDate ? startDate.format(DATE_FORMAT_DB) : '',
                                                            name: 'received_at'
                                                        }
                                                    })
                                                }}>
                                                <input
                                                    className={`form-control form-control-sm rounded${subOrderErrors.received_at ? ' is-invalid' : ''}`}
                                                    placeholder="Type Receive date here"
                                                    onChange={(e) => { }}
                                                    value={received_at ? convertToDate(received_at) : ''} />
                                                    <div className="invalid-feedback">{subOrderErrors.received_at && subOrderErrors.received_at[0] ? 'Receive Date field is required when Qty Received is changed.' : ''}</div>
                                            </DateRangePickerComponent>
                                        :
                                            <input
                                                className={`form-control form-control-sm rounded`}
                                            readOnly={true} value={received_at ? convertToDate(received_at) : ''} />
                                        }
                                    </div>
                                </div>
                                {activeSuborder.actions.canEditQtyToReceive ?
                                    <div className="form-group row mb-2">
                                        <div className= "col-sm-4">
                                            &nbsp;
                                        </div>
                                        <div className= "col-sm-8">
                                            <span>
                                                <input id="receive_all"
                                                    className="mr-2"
                                                    type="checkbox"
                                                    name="receive_all"
                                                    value="1"
                                                    checked={receive_all || false}
                                                    onChange={(e) => { this.handleSuborderInputChange(e) }}
                                                />
                                                <label className="mb-0 clickable" htmlFor="receive_all">Receive All</label>
                                            </span>
                                        </div>
                                    </div>
                                : null }


                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-12">
                        <div className="row">
                            <div className="col-sm-12">

                                <div className="form-group row mb-2">
                                    <label className="col-sm-4 col-form-label">Notes to Vendor</label>
                                    <div className="col-sm-8">

                                        <textarea rows={2}
                                            readOnly={true}
                                            className={`form-control form-control-sm rounded${subOrderErrors.notes ? ' is-invalid' : ''}`}
                                            type="text"
                                            name="notes"
                                            onChange={(e) => { this.handleSuborderInputChange(e) }}
                                            value={notes || ''}
                                        />

                                        <div className="invalid-feedback">{subOrderErrors.notes && subOrderErrors.notes[0]}</div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">

                                <div className="form-group row mb-2">
                                    <label className="col-sm-4 col-form-label">PO #</label>
                                    <div className="col-sm-8">

                                        <input
                                            readOnly={true}
                                            className={`form-control form-control-sm rounded${subOrderErrors.purchase_number ? ' is-invalid' : ''}`}
                                            type="text"
                                            name="purchase_number"
                                            onChange={(e) => { this.handleSuborderInputChange(e) }}
                                            value={purchase_number || ''}
                                        />

                                        <div className="invalid-feedback">{subOrderErrors.purchase_number && subOrderErrors.purchase_number[0]}</div>
                                    </div>
                                </div>


                            </div>
                        </div>

                    </div>
                </div>
            )
        }

        let exceptProducts = [];
        sub_orders.map(subOrder => {
            const { order_prices } = subOrder;
            order_prices.map(orderPrice => {
                const { price_entity } = orderPrice;
                exceptProducts.push(price_entity.product_id);
            })
        });

        // console.log('canAddItemsToOrder: ', canAddItemsToOrder);

        return (
            <div>
                <Modal
                    isOpen={this.props.modalIsOpen}
                    onAfterOpen={(e) => { this.afterOpenModal() }}
                    onRequestClose={() => { this.close() }}
                    style={Modal.customStyles}
                >
                    <div className="order-form">
                        <div onSubmit={(e) => { this.handleSubmit(e) }}>
                            <div className="card-header card-header-divider">
                                {loading && !formTitle && <Skeleton height={25} width={250} />}
                                {formTitle}
                                <div className="tools">
                                    <button type="button" className="close" id="cross-close-btn" data-dismiss="modal" aria-label="Close"
                                        onClick={() => { this.close() }}>
                                        <span className="mdi mdi-close" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                            <div className="modal-body">
                                {/* begin content */}

                                <div className="row">
                                    <div className="col-lg-6 col-md-12">
                                        {loading ? <Skeleton height={150} width={"100%"} /> :
                                            !canAddItemsToOrder ? null :
                                            <OrderAddProductContainer
                                                placeholder={"Search Products (Product Name, Mfr # or Vendor Product #)"}
                                                onSelectCallback={( product ) => { this.handleSelectedProduct(product) }}
                                                    onSelectMultiplePrices={(prices, additionalInfo) => { this.handleSelectedMultiplePrices(prices, additionalInfo) }}
                                                exceptProducts={exceptProducts}
                                                preselectedLocation={shipToLocationObject}
                                            />
                                        }
                                        {loading ? <Skeleton height={250} width={"100%"} /> : renderOrderTotal(canAddItemsToOrder ? 'mt-2' : '')}

                                    </div>
                                    <div className="col-lg-6 col-md-12">
                                        {loading ? <Skeleton height={250} width={"100%"} /> : renderOrderInformation()}
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-lg-6 col-md-12">
                                        {loading ? <Skeleton height={33} width={"100%"} /> : <div className="card-header card-header-divider ml-0 mr-0">Order Items</div>}
                                    </div>
                                </div>

                                {loading ? <Skeleton height={150} width={"100%"} /> :
                                    <div className="row sub-order-panel">
                                        <div className="col-sm-12">
                                            {/* begin price list */}

                                            <ul className="nav nav-tabs nav-tabs-classic">
                                                <li className="nav-item text-center">
                                                    <a className={`nav-link${!active_sub_order_id ? ' active' : ''}`} onClick={() => { this.handleSetActiveSuborder(null) }} href="#">
                                                        <div className="all-vendors-tab">
                                                            <span>All Vendors</span>
                                                        </div>
                                                    </a>
                                                </li>
                                                {
                                                    sub_orders.map((sub_order, i) => {
                                                        const { status, order_prices } = sub_order;
                                                        const statusLabel = this.statusList[status] ? this.statusList[status] : status;

                                                        return (
                                                            Array.isArray(order_prices) && order_prices.length > 0 ?
                                                            <li className="nav-item text-center" key={i}>
                                                                <a className={`nav-link${sub_order.id == active_sub_order_id ? ' active' : ''}`} onClick={() => { this.handleSetActiveSuborder(sub_order.id) }} href="#">
                                                                    <div className="vendor-tab">
                                                                        <span>{sub_order.vendor.name.length < 30 ? sub_order.vendor.name : sub_order.vendor.name.slice(0,30)+"..."}</span>
                                                                        <br/>
                                                                        <span className={`order-status ${colorizeStatus(status)}`}>{statusLabel}</span>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            : null
                                                        )
                                                    })
                                                }
                                            </ul>

                                            <div className="card tab-content mb-0">
                                                <div className="card-body">
                                                    <div className="row mb-2">
                                                        <div className="col-sm-12 text-right pr-0">
                                                            { renderSuborderActions() }

                                                            { activeSuborder && activeSuborder.status != ORDER_STATUS_OPEN ? renderSuborderFields() : null }
                                                        </div>
                                                    </div>
                                                    <div className="row mb-1">
                                                        <div className="col-sm-12">
                                                            {/* begin order price table */}
                                                            <OrderPriceListContainer
                                                               showVendorColumn={ active_sub_order_id == null }
                                                               orderPrices={orderPrices}
                                                               onChangeQty={(orderPriceId, qty) => {
                                                                   this.handleChangeOrderPrice(orderPriceId, 'quantity', Number(qty))
                                                               }}
                                                               onChangeQtyReceived={(orderPriceId, qty) => {
                                                                   this.handleChangeOrderPrice(orderPriceId, 'quantity_received', Number(qty))
                                                               }}
                                                               onDelete={(orderPriceId) => { this.handleDeleteOrderPrice(orderPriceId) }}
                                                               onChangeNote={(orderPriceId, note) => { this.handleChangeNote(orderPriceId, note) }}
                                                               receivedDateIsRequired = {(receivedDateIsRequired) => { this.setReceiveDateRequiredFlag(active_sub_order_id, receivedDateIsRequired) }}
                                                           />
                                                               {/* end order price table */}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            {/* end price list */}
                                        </div>
                                    </div>
                                }

                                {/* end content */}
                            </div>
                            <div className="modal-footer">

                                {loading ? <Skeleton height={50} width={70} /> :
                                    ( orderCanBeCanceled ?
                                            <button
                                            disabled={submiting || isOrderModified}
                                            id="cancel-btn"
                                            onClick={(e) => { this.handleCancelOrder(e) }}
                                            style={submiting ? { cursor: 'progress' } : {}}
                                            className="btn btn-light">Cancel Order</button>
                                        : ''
                                     )
                                }
                                &nbsp;

                                {loading ? <Skeleton height={50} width={70} /> :
                                    <button
                                        disabled={submiting}
                                        id="submit-btn"
                                        onClick={(e) => { this.handleSubmit(e) }}
                                        style={submiting ? { cursor: 'progress' } : {}}
                                        className={`btn btn-${ orderCanBeSubmissioned ? 'light' : 'primary'}`}>{submitBtnText}</button>
                                }
                                &nbsp;

                                {loading ? <Skeleton height={50} width={70} /> :
                                    (canPrint ?
                                        <button
                                            disabled={submiting}
                                            id="submission-btn"
                                            onClick={(e) => {
                                                e.preventDefault();
                                                this.handlePrintLabels();
                                            }}
                                            style={submiting ? { cursor: 'progress' } : {}}
                                            className="btn btn-light">Print Labels</button>
                                        : ''
                                    )
                                }

                                {loading ? <Skeleton height={50} width={70} /> :
                                    (orderCanBeSubmissioned ?
                                        <button
                                            disabled={submiting || isOrderModified}
                                            id="submission-btn"
                                            onClick={(e) => { this.handleOpenSubmissionModal(e) }}
                                            style={submiting ? { cursor: 'progress' } : {}}
                                            className="btn btn-primary">Order Submission</button>
                                        : ''
                                    )
                                }
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </Modal>

                <OrderBuyProductContainer modalIsOpen={this.state.modalBuyIsOpen}
                    productId={this.state.buyProductId}
                    orderId={this.state.id}
                    hideOrderSelector={true}
                    onSubmit={(priceData) => { this.handleSelectedPriceData(priceData) } }
                    closeModal={() => {
                        this.setState({
                            modalBuyIsOpen: false,
                            buyProductId: null
                        });
                    }}
                    afterSuccess={() => {}}
                />

                <OrderSubmissionFormContainer modalIsOpen={this.state.modalOrderSubmissionIsOpen}
                    number={number}
                    bill_to={bill_to}
                    ship_to={ship_to}
                    notes={notes}
                    sub_orders={sub_orders.map(sub_order => ({ ...sub_order, email: Boolean(sub_order.vendor.email), print: !sub_order.vendor.email}))}

                    locationList={this.props.locationList}
                    billToLocationList={this.props.billToLocations}
                    onSubmit={ async (data) => { await this.handleOrderSubmission(data) }}
                    closeModal={() => {
                        this.setState({
                            modalOrderSubmissionIsOpen: false,
                        });
                    }}
                    afterSuccess={() => { }}
                />

                <SubOrderPrintComponent subOrders={this.state.subOrdersToPrint}/>
                <PrintLabelsComponent  data = {this.props.printData} mode = {isInventoryMode(clinic) ? 'inventory' : 'ordering'} />

            </div>
        );
    }
}

OrderFormComponent.propTypes = {
    afterSuccess: PropTypes.func,
    productId: PropTypes.number,
    closeModal: PropTypes.func,
    dispatch: PropTypes.func,
    errors: PropTypes.object,
    modalIsOpen: PropTypes.bool
}
