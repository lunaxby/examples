import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import {
    statusList,
    colorizedVendorList
} from '../../../common/order-status'

import {
    FILTER_ORDER,
    FILTER_SHIP_TO,
    FILTER_BILL_TO,
    FILTER_STATUS,
    FILTER_VENDOR,
    FILTER_ORDER_NUMBER,
    FILTER_PURCHASE_ORDER_NUMBER,
    FILTER_MANUFACTURER_NUMBER,
    FILTER_VENDOR_PRODUCT_NUMBER,
    FILTER_ORDER_DATE,

    filterNames
} from '../../../common/order-filter'

import {
    ORDER_OUTPUT_TEMPLATE_PRODUCT_PURCHASE_HISTORY,
    ORDER_OUTPUT_TEMPLATE_ORDER_STATUS_REPORT,
    ORDER_OUTPUT_TEMPLATE_TOTAL_PURCHASES_RECEIVED,
    ORDER_OUTPUT_TEMPLATE_PRINT_TYPE_EASY_ORDER_FORM,
} from '../../../common/order-output-template'

import {
    ORDER_OUTPUT_OPERATION_PRINT,
    ORDER_OUTPUT_OPERATION_EXPORT_TO_EXCEL
} from '../../../common/order-output-operation'

import { DATE_FORMAT, convertToDateTime } from '../../../common/date'

import { convertToPrice } from '../../../common/price'

import orderActions from '../../actions/order';
const { fetchOrders, actionExportOrders, exportOrdersResponse, actionPrintOrders, initialOrderList } = orderActions;

import vendorActions from '../../actions/vendor';
const { fetchVendors } = vendorActions;

import { fetchLocations, fetchBillToLocations } from '../../actions/location';

import { fetchAuthUser } from '../../actions/auth';

import {DEFAULT_PAGE_SIZE, DEFAULT_PAGE_SIZE_OPTIONS} from '../../../common/grid'
import TableShowComponent from "../widgets/TableShowComponent";
import TablePaginationComponent from "../widgets/TablePaginationComponent";

import DateRangePickerComponent from "../widgets/DateRangePickerComponent";

import toastr from "../../common/toastr";

import PropTypes from "prop-types";
import swal from 'sweetalert';
import Skeleton from 'react-loading-skeleton';
import Select from 'react-select'

import OrderFormContainer from "../../containers/order/OrderFormContainer";
import VendorStatusTooltipComponent from '../../../common/widgets/VendorStatusTooltipComponent'

import printJS from 'print-js'
import ProductPurchaseHistoryComponent from './report/ProductPurchaseHistoryComponent';
import OrderStatusReportComponent from "./report/OrderStatusReportComponent";
import TotalPurchaseReceivedComponent from "./report/TotalPurchaseReceivedComponent";
import PrintEasyOrderFormComponent from "../order/report/PrintEasyOrderFormComponent";

export default class OrderListComponent extends React.Component {
    initialState() {
        const outputOperationList = [
            { value: ORDER_OUTPUT_OPERATION_PRINT, label: 'Print' },
            { value: ORDER_OUTPUT_OPERATION_EXPORT_TO_EXCEL, label: 'Export to Excel file' },
        ];

        const outputTemplateList = [
            { value: ORDER_OUTPUT_TEMPLATE_PRODUCT_PURCHASE_HISTORY, label: 'Product Purchase History' },
            { value: ORDER_OUTPUT_TEMPLATE_ORDER_STATUS_REPORT, label: 'Order Status Report' },
            { value: ORDER_OUTPUT_TEMPLATE_TOTAL_PURCHASES_RECEIVED, label: 'Total Purchases Received' },
            { value: ORDER_OUTPUT_TEMPLATE_PRINT_TYPE_EASY_ORDER_FORM, label: 'Print Easy Order Form' }
        ];

        return {
            showFilterPanel: false,
            showPrintPanel: false,

            modalIsOpen: false,
            orderId: null,

            currentPage: 1,
            pageSize: DEFAULT_PAGE_SIZE,
            sortby: [],
            filter: {},
            search: '',

            outputTemplateList: outputTemplateList,
            outputTemplate: outputTemplateList[0],

            outputOperationList: outputOperationList,
            outputOperation: outputOperationList[0],

            selected: {},
            selectAll: 0,

            exportPrintInProcess: false,
            showPrintFilterFields: 'contents'
        }
    }

    constructor(props) {
        super(props);

        this.onSort = this.onSort.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.handleDelete = this.handleDelete.bind(this);

        this.state = this.initialState();

        this.statusList = statusList;

        this.defaultSorted = [
            {
                id: 'number',
                desc: true,
            },
        ];
    }

    async componentDidMount() {
        const { author } = this.props;
        if (author) {
            await this.props.dispatch(fetchAuthUser({
                userId: author.id,
            }));
        }

        await this.reloadList();
        const { mode } = this.props.match.params;
        if (mode == 'create') {
            this.handleOpenModalCreateOrder();
        }
        await this.props.dispatch(initialOrderList());
    }

    successMessage(message) {
        toastr.success(message, 'Success');
    }

    errorMessage(message) {
        toastr.error(message, 'Error');
    }

    fetchData(state, instance) {
        this.state.currentPage = state.page + 1;
        this.state.pageSize = state.pageSize;
        this.state.sortby = state.sorted;
        this.state.search = state.search;

        this.reloadList();
    }

    onSort(e, accessor, state, instance, sortMethod) {
        e.preventDefault();
        state.sorted = [{
            id: accessor,
            [sortMethod]: true,
        }];

        instance.state.sorted = state.sorted;
        this.fetchData(state, instance);
    }

    prepareFilter() {
        const { filter } = this.state;
        const formatedFilters = [];

        Object.keys(filter).map((filterName) => {
            const currentFilter = filter[filterName];
            let filterValue = filter[filterName];

            if (Array.isArray(currentFilter)) {
                filterValue = currentFilter.map((filterValue) => {
                    return filterValue.value;
                });
            }

            const emptyFilter = !filterValue || (Array.isArray(filterValue) && filterValue.length == 0);

            if (!emptyFilter) {
                formatedFilters.push({
                    'field': filterName, 'value': filterValue
                })
            }
        })

        return formatedFilters;
    }

    async applyFilters() {
        await this.reloadList();
    }

    async reloadList() {
        let { search, currentPage, pageSize, sortby } = this.state;

        if (!sortby || !sortby.length) {
            sortby = this.defaultSorted;
        }

        try {
            await this.props.dispatch(fetchOrders({
                page: currentPage,
                per_page: pageSize,
                sortby: sortby,
                search: search,
                filter: this.prepareFilter()
            }));
        } catch (e) {
            this.errorMessage(e.message ? e.message : e.error);
        }
    }

    handleOpenModalCreateOrder() {
        this.setState({ modalIsOpen: true, orderId: null })
    }

    async handleDelete(id) {
        const willDelete = await swal({
            title: "Are you sure?",
            text: "Are you sure that you want to delete this Order?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        if (!willDelete) {
            return;
        }

        try {
            await this.props.dispatch(deleteProduct(id));

            this.successMessage('The order has been successfully deleted!');
            this.reloadList();
        } catch (e) {
            this.errorMessage(e.message ? e.message : 'Order was not deleted');
        }
    }

    async handleExport(filter) {
        const { outputTemplate } = this.state;
        try {
            await this.props.dispatch(actionExportOrders({
                filter: filter,
                sortby: [{ "id": "number", "asc": 1 }],
                report_type: 'export_' + outputTemplate.value
            }));
        } catch (e) {
            this.errorMessage(e.message ? e.message : 'Order were not exported');
        }
    }

    async handlePrint(filter) {
        const { outputTemplate } = this.state;
        try {
            if (outputTemplate.value != ORDER_OUTPUT_TEMPLATE_PRINT_TYPE_EASY_ORDER_FORM) {
                await this.props.dispatch(actionPrintOrders({
                    filter: filter,
                    sortby: [{"id": "number", "asc": 1}],
                    report_type: 'export_' + outputTemplate.value
                }));
            }

            printJS({
                printable: outputTemplate.value,
                type: 'html',
                scanStyles: false,
                header: '',
                css: ['/css/print_' + outputTemplate.value +'.css'],
            });
        } catch (e) {
            this.errorMessage(e.message ? e.message : 'Order were not printed');
        }
    }

    async handleExportPrint() {
        const { outputOperation, exportPrintInProcess, selected } = this.state;
        if (exportPrintInProcess) {
            return;
        }

        this.setState({ exportPrintInProcess: true });
        const orderIds = [];
        Object.keys(selected).map(orderId => {
            if (selected[orderId] == true) {
                orderIds.push(orderId);
            }
        })
        let filter = this.prepareFilter();
        if (orderIds.length > 0) {
            filter.push({
                'field': FILTER_ORDER, 'value': orderIds
            })
        }

        switch (outputOperation.value) {
            case ORDER_OUTPUT_OPERATION_EXPORT_TO_EXCEL:
                await this.applyFilters();
                await this.handleExport(filter);
                break;
            case ORDER_OUTPUT_OPERATION_PRINT:
                await this.applyFilters();
                await this.handlePrint(filter);
                break;
        }
        this.setState({ exportPrintInProcess: false });
    }

    async afterTogglePanel(panelIsOpenedFlag) {
        const allPanelsAreClosed = !this.state.showFilterPanel && !this.state.showPrintPanel;

        if (this.state[panelIsOpenedFlag]) {
            try {
                await this.props.dispatch(fetchVendors({
                    'all': 1,
                    'dhpi': 1,
                    'sortby': [
                        {'id': 'vendors.dhpi', 'desc': 1},
                        {'id': 'vendors.name'}
                    ]
                }));
            } catch (e) {
                this.errorMessage(e.message ? e.message : 'Can\'t load Vendor List');
            }

            try {
                await this.props.dispatch(fetchLocations({ 'all': 1 }));
                await this.props.dispatch(fetchBillToLocations());
            } catch (e) {
                this.errorMessage(e.message ? e.message : 'Can\'t load Location List');
            }

        } else if (allPanelsAreClosed) {
            await this.setState({
                filter: this.initialState().filter
            });
            await this.reloadList();
        }
    }

    async handleShowFilterPanel() {
        const { showFilterPanel } = this.state;
        await this.setState({ showFilterPanel: !showFilterPanel });

        await this.afterTogglePanel('showFilterPanel');
    }

    async handleShowPrintPanel() {
        const { showPrintPanel } = this.state;
        await this.setState({ showPrintPanel: !showPrintPanel });

        if (!this.state.showPrintPanel) {
            await this.props.dispatch(exportOrdersResponse({}));
        }

        await this.afterTogglePanel('showPrintPanel');
    }

    async handleSelect(selectedItem) {
        const { value, name } = selectedItem;

        await this.setState({
            [name]: value,
        });
        if (value.value == 'export_to_excel') {
            let outputTemplateList = [
                { value: ORDER_OUTPUT_TEMPLATE_PRODUCT_PURCHASE_HISTORY, label: 'Product Purchase History' },
                { value: ORDER_OUTPUT_TEMPLATE_ORDER_STATUS_REPORT, label: 'Order Status Report' },
                { value: ORDER_OUTPUT_TEMPLATE_TOTAL_PURCHASES_RECEIVED, label: 'Total Purchases Received' },
            ];
            if (this.state.outputTemplate.value == ORDER_OUTPUT_TEMPLATE_PRINT_TYPE_EASY_ORDER_FORM) {
                await this.setState({outputTemplateList: outputTemplateList, outputTemplate: outputTemplateList[0]});
            } else {
                await this.setState({outputTemplateList: outputTemplateList});
            }

        } else if (value.value == 'print'){
            let outputTemplateList = [
                { value: ORDER_OUTPUT_TEMPLATE_PRODUCT_PURCHASE_HISTORY, label: 'Product Purchase History' },
                { value: ORDER_OUTPUT_TEMPLATE_ORDER_STATUS_REPORT, label: 'Order Status Report' },
                { value: ORDER_OUTPUT_TEMPLATE_TOTAL_PURCHASES_RECEIVED, label: 'Total Purchases Received' },
                { value: ORDER_OUTPUT_TEMPLATE_PRINT_TYPE_EASY_ORDER_FORM, label: 'Print Easy Order Form' }
            ];
            await this.setState({outputTemplateList: outputTemplateList});
        }

        if (value.value === ORDER_OUTPUT_TEMPLATE_PRINT_TYPE_EASY_ORDER_FORM) {
            this.setState({
                showPrintFilterFields: 'none'
            });
        } else {
            this.setState({
                showPrintFilterFields: 'contents'
            });
        }
    }

    async handleSelectFilter(selectedItem, name, reload = false) {
        await this.setState({
            filter: {
                ...this.state.filter,
                [name]: selectedItem,
            },
            currentPage: 1,
        });

        if (reload) {
            await this.reloadList();
        }
    }

    handleInputChange(event) {
        const target = event.target;
        let value = null;

        switch (target.type) {
            case 'checkbox':
                value = target.checked;
                break;
            case 'file':
                value = target.files[0];
                break;
            default:
                value = target.value;
        }

        const name = target.name;

        this.setState({
            [name]: value,
        });
    }

    toggleRow(id) {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[id] = !this.state.selected[id];
        this.setState({
            selected: newSelected,
            selectAll: 2
        });
    }

    toggleSelectAll() {
        const { orders } = this.props;
        let newSelected = {};

        if (this.state.selectAll === 0) {
            orders.map((product) => {
                newSelected[product.id] = true;
            });
        }

        this.setState({
            selected: newSelected,
            selectAll: this.state.selectAll === 0 ? 1 : 0
        });
    }

    render() {
        const { pages, loading, orders, total, vendorList, locationList, billToLocations } = this.props;
        const { showFilterPanel, showPrintPanel, filter, pageSize, currentPage, sortby, showPrintFilterFields } = this.state;

        let printColumns = [];
        let columns = [];

        if (showPrintPanel) {
            printColumns = [
                {
                    id: "checkbox",
                    accessor: "",
                    Header: (x) => {
                        return (
                            <div className="be-checkbox custom-control custom-checkbox mb-0">
                                <span>
                                    <input
                                        id = "checked-orders"
                                        type="checkbox"
                                        className="custom-control-input"
                                        checked={this.state.selectAll === 1}
                                        ref={input => {
                                            if (input) {
                                                input.indeterminate = this.state.selectAll === 2;
                                            }
                                        }}
                                        onChange={() => this.toggleSelectAll()}
                                    />
                                     <label className="custom-control-label" htmlFor="checked-orders"></label>
                                </span>
                            </div>
                        );
                    },
                    sortable: false,
                    width: 45,
                    style: { verticalAlign: 'middle' }
                }
            ]
        }

        columns = columns.concat(printColumns, [
            {
                Header: '#',
                id: 'id',
                accessor: 'id',
                sortable: true,
                width: 80,
            },
            {
                Header: 'Order #',
                id: 'number',
                accessor: 'number',
                sortable: true,
                width: 150,
            },
            {
                Header: 'Date',
                id: 'created_at',
                accessor: (order) => {
                    return order.created_at ? convertToDateTime(order.created_at) : ''
                },
                sortable: true,
                width: 200,
            },
            {
                Header: 'Bill To',
                id: 'bill_to',
                accessor: (order) => {
                    return order.bill_to ? order.bill_to.name : ''
                },
                sortable: true,
            },
            {
                Header: 'Ship To',
                id: 'ship_to',
                accessor: (order) => {
                    return order.ship_to ? order.ship_to.name : ''
                },
                sortable: true,
            },
            {
                Header: 'Vendors',
                id: 'vendors',
                Header: (x) => {
                    return (
                        <div>
                            <span>Vendors and Statuses <VendorStatusTooltipComponent/></span>
                        </div>
                    );
                },
                width: '20%',
                sortable: false,
            },
            {
                Header: 'Total',
                id: 'total',
                accessor: 'total',
                sortable: true,
            },
            {
                Header: '',
                accessor: '',
                sortable: false,
                width: 110,
            },
        ])

        const skeletonRows = [];
        for (let i = 0; i < 5; i++) {
            skeletonRows.push(
                <tr key={i}>
                    {
                        columns.map((column, index) => {
                            return (
                                <td key={index}><Skeleton height={33} /></td>
                            )
                        })
                    }
                </tr>
            )
        }

        const setSortedClass = (column) => {
            if (Array.isArray(sortby)) {
                let currentSort = sortby.find((sortItem) => {
                    return sortItem.id === column.id;
                });

                if (currentSort) {
                    return currentSort.desc ? 'sorting_desc' : 'sorting_asc';
                }
            }
            return 'sorting'
        }

        const renderFilters = () => {
            const anyPlaceholder = '-- ANY --';

            const getDateFilterValue = () => {
                let returnDate = [];
                const dateFilter = filter[FILTER_ORDER_DATE];
                if (dateFilter == undefined) {
                    return '';
                }

                if (dateFilter.startDate && dateFilter.endDate) {
                    returnDate.push(dateFilter.startDate ? dateFilter.startDate : '...');
                    if (dateFilter.startDate != dateFilter.endDate) {
                        returnDate.push(dateFilter.endDate ? dateFilter.endDate : '...');
                    }
                }

                return returnDate.join(' - ');
            };

            return (
                <div className="row col-sm-12" style={{ display: showPrintFilterFields }}>
                    <div className="col-lg-6 col-md-12">
                        <div className="form-group row">
                            <label className="col-lg-2 col-md-4 col-form-label">{ filterNames[FILTER_SHIP_TO] }</label>
                            <div className="col-lg-10 col-md-8">
                                <Select
                                    className={`react-select`}
                                    options={locationList.map(item => ({ value: item.id, label: item.name }))}
                                    onChange={(selectedItem) => { this.handleSelectFilter(selectedItem, FILTER_SHIP_TO) }}
                                    placeholder={anyPlaceholder}
                                    isMulti={true}
                                    isSearchable={true}
                                    value={filter[FILTER_SHIP_TO]} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-lg-2 col-md-4 col-form-label">{filterNames[FILTER_BILL_TO]}</label>
                            <div className="col-lg-10 col-md-8">
                                <Select
                                    className={`react-select`}
                                    options={billToLocations.map(item => ({ value: item.id, label: item.name }))}
                                    onChange={(selectedItem) => { this.handleSelectFilter(selectedItem, FILTER_BILL_TO) }}
                                    placeholder={anyPlaceholder}
                                    isMulti={true}
                                    isSearchable={true}
                                    value={filter[FILTER_BILL_TO]} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-lg-2 col-md-4 col-form-label">{filterNames[FILTER_STATUS]}</label>
                            <div className="col-lg-10 col-md-8">
                                <Select
                                    className={`react-select`}
                                    options={Object.keys(this.statusList).map((field) => ({ value: field, label: this.statusList[field] }))}
                                    onChange={(selectedItem) => { this.handleSelectFilter(selectedItem, FILTER_STATUS) }}
                                    placeholder={anyPlaceholder}
                                    isMulti={true}
                                    isSearchable={true}
                                    value={filter[FILTER_STATUS]} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-lg-2 col-md-4 col-form-label">{filterNames[FILTER_VENDOR]}</label>
                            <div className="col-lg-10 col-md-8">
                                <Select
                                    className={`react-select`}
                                    options={vendorList.map(vendor => ({ value: vendor.id, label: vendor.name }))}
                                    onChange={(selectedItem) => { this.handleSelectFilter(selectedItem, FILTER_VENDOR) }}
                                    placeholder={anyPlaceholder}
                                    isMulti={true}
                                    isSearchable={true}
                                    value={filter[FILTER_VENDOR]} />
                            </div>
                        </div>

                    </div>
                    <div className="col-lg-6 col-md-12 edge-pr-0">

                        <div className="form-group row">
                            <label className="col-lg-3 col-md-4 col-form-label">{filterNames[FILTER_ORDER_NUMBER]}</label>
                            <div className="col-lg-9 col-md-8 edge-pr-0">
                                <input
                                    className={`form-control form-control-sm`}
                                    onChange={(e) => { this.handleSelectFilter(e.target.value, FILTER_ORDER_NUMBER) }}
                                    placeholder={anyPlaceholder}
                                    value={filter[FILTER_ORDER_NUMBER] || ''} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-lg-3 col-md-4 col-form-label">{filterNames[FILTER_PURCHASE_ORDER_NUMBER]}</label>
                            <div className="col-lg-9 col-md-8 edge-pr-0">
                                <input
                                    className={`form-control form-control-sm`}
                                    onChange={(e) => { this.handleSelectFilter(e.target.value, FILTER_PURCHASE_ORDER_NUMBER) }}
                                    placeholder={anyPlaceholder}
                                    value={filter[FILTER_PURCHASE_ORDER_NUMBER] || ''} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-lg-3 col-md-4 col-form-label">{filterNames[FILTER_MANUFACTURER_NUMBER]}</label>
                            <div className="col-lg-9 col-md-8 edge-pr-0">
                                <input
                                    className={`form-control form-control-sm`}
                                    onChange={(e) => { this.handleSelectFilter(e.target.value, FILTER_MANUFACTURER_NUMBER) }}
                                    placeholder={anyPlaceholder}
                                    value={filter[FILTER_MANUFACTURER_NUMBER] || ''} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-lg-3 col-md-4 col-form-label">{filterNames[FILTER_VENDOR_PRODUCT_NUMBER]}</label>
                            <div className="col-lg-9 col-md-8 edge-pr-0">
                                <input
                                    className={`form-control form-control-sm`}
                                    onChange={(e) => { this.handleSelectFilter(e.target.value, FILTER_VENDOR_PRODUCT_NUMBER) }}
                                    placeholder={anyPlaceholder}
                                    value={filter[FILTER_VENDOR_PRODUCT_NUMBER] || ''} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-lg-3 col-md-4 col-form-label">{filterNames[FILTER_ORDER_DATE]}</label>
                            <div className="col-lg-9 col-md-8 edge-pr-0">
                                <DateRangePickerComponent
                                    config={ { opens: 'left' } }
                                    onApplyCallback={(startDate, endDate) => {
                                        this.handleSelectFilter({ 'startDate': startDate ? startDate.format(DATE_FORMAT) : '', 'endDate': endDate ? endDate.format(DATE_FORMAT) : ''}, FILTER_ORDER_DATE)
                                    }}>
                                <input
                                    className={`form-control form-control-sm`}
                                    placeholder={anyPlaceholder}
                                    onChange={(e) => {  }}
                                    value={getDateFilterValue()} />
                                </DateRangePickerComponent>
                            </div>
                        </div>

                    </div>
                </div>
            )
        }

        const renderFilterPanel = () => {
            return (
                <div className="row mb-3">
                    <div className="col-md-12">
                        <div className="card card-filter">
                            <div className="card-header">
                                Filters
                            </div>
                            <div className="row be-datatable-header">
                               { renderFilters() }

                                   <div className="col-sm-12">
                                        <div className="form-group row mb-2">
                                            <label className="col-sm-2 col-form-label"></label>
                                            <div className="col-sm-10 text-right">
                                                <button className="btn btn-primary" onClick={(e) => { this.applyFilters(); }}>Apply Filter</button>
                                            </div>
                                        </div>
                                   </div>

                            </div>
                        </div>
                    </div>
                </div>
            )
        }

        const renderPrintPanel = () => {
            const { outputTemplateList, outputTemplate, outputOperationList, outputOperation, exportPrintInProcess } = this.state;

            return (
                <div className="row mb-3">
                    <div style={{ display: 'none' }}>
                        {this.props.downloadedFile ? <iframe src={this.props.downloadedFile} /> : null }
                    </div>
                    <div className="col-md-12">

                        <div className="card">
                            <div className="card-header">
                                Export / Print
                            </div>
                            <div className="card-body">
                                <div className="row be-datatable-header">
                                    <div className="col-lg-3 col-md-12">

                                        <div className="form-group row">
                                            <div className="col-sm-12">
                                                <Select
                                                    className={`react-select`}
                                                    options={outputOperationList}
                                                    onChange={(selectedItem) => { this.handleSelect({ value: selectedItem, name: 'outputOperation'}) }}
                                                    placeholder="Output Operation"
                                                    required
                                                    name="outputOperation"
                                                    value={outputOperation} />
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <div className="col-sm-12">
                                                <Select
                                                    className={`react-select`}
                                                    options={outputTemplateList}
                                                    onChange={(selectedItem) => { this.handleSelect({ value: selectedItem, name: 'outputTemplate' }) }}
                                                    placeholder="Print Template"
                                                    required
                                                    name="outputTemplate"
                                                    value={outputTemplate} />
                                            </div>
                                        </div>

                                    </div>
                                    <div className="col-lg-9 col-md-12">
                                        <div className="row">
                                            { renderFilters() }
                                        </div>
                                    </div>
                                    <div className="col-md-12 text-right">
                                        <button className={`btn btn-primary`}
                                                style={exportPrintInProcess ? { 'cursor': 'progress' } : {}}
                                                disabled={exportPrintInProcess ? 'disabled' : null}
                                                href="#" onClick={(e) => {
                                            e.preventDefault();
                                            this.handleExportPrint();
                                        }}>
                                            <i className="fas fa-print"></i> Submit
                                        </button>
                                        <button className="btn btn-primary ml-1" onClick={(e) => { this.applyFilters(); }}>Apply Filter</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

        return (
                <div className="card card-table">
                    <div className="card-header">
                        Orders
                        <div className="tools">
                            <button onClick={() => { this.handleOpenModalCreateOrder() }}
                                    className="btn btn-primary" id="add-item-btn">New Order</button>
                        </div>
                    </div>
                    <div className="card-body">
                        <div className="dataTables_wrapper dt-bootstrap4 no-footer">
                            <ReactTable
                                manual
                                defaultPageSize={pageSize}
                                data={orders}
                                columns={columns}
                                pages={pages}
                                onFetchData={this.fetchData}
                                loading={loading}
                                total={total}
                                currentPage={currentPage}
                                pageSizeOptions={DEFAULT_PAGE_SIZE_OPTIONS}
                                defaultSorted={this.defaultSorted}
                            >
                                {(state, makeTable, instance) => {
                                    return (
                                        <div>
                                            <div className="row be-datatable-header">
                                                <div className="col-sm-6">
                                                    <div className="dataTables_length">
                                                        <TableShowComponent tableState={state} tableInstance={instance} />
                                                    </div>
                                                </div>

                                                <div className="col-sm-6">
                                                    <div className="tools">
                                                        <div className="dataTables_filter">
                                                            <a className={`btn btn-secondary mr-2${ showPrintPanel ? ' active' : '' }`}
                                                                href="#" onClick={(e) => {
                                                                    e.preventDefault();
                                                                    this.handleShowPrintPanel();
                                                                }}>
                                                                <i className="fas fa-print"></i> Export / Print
                                                            </a>
                                                            <a className={`btn btn-secondary${showFilterPanel ? ' active' : ''}`}
                                                                href="#" onClick={(e) => {
                                                                    e.preventDefault();
                                                                    this.handleShowFilterPanel();
                                                                }}>
                                                                <i className="fas fa-filter"></i> Filters
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            { showPrintPanel ? renderPrintPanel(): '' }

                                            { showFilterPanel ? renderFilterPanel(): '' }
                                            <div className="row be-datatable-body">
                                                <div className="col-sm-12">
                                                <div className="table-responsive">
                                                        <table className="table table-striped table-hover table-fw-widget dataTable no-footer dtr-inline">
                                                            <thead>
                                                                <tr role="row">
                                                                    {state.columns.map((item, index) => {
                                                                        return (
                                                                            <th
                                                                                key={index}
                                                                                width={item.width}
                                                                                className={`${item.sortable ? setSortedClass(item) : ''}${item.id == 'total' ? ' right-align-column-header' : ''}`}
                                                                                onClick={(e) => { item.sortable ? this.onSort(e, item.id, state, instance, setSortedClass(item) == 'sorting_asc' ? 'desc' : 'asc') : null }}
                                                                            >
                                                                                {typeof item.Header === 'function' ? item.Header() : item.Header}
                                                                            </th>
                                                                        )
                                                                    })}
                                                                </tr>
                                                            </thead>
                                                            {loading ?
                                                                <tbody className="g-font-size-default g-color-black">
                                                                {skeletonRows}
                                                                </tbody>

                                                                :
                                                                <tbody>
                                                                {state.pageRows.map((item, index) => {
                                                                    return (
                                                                        <tr key={index} className={index % 2 == 0 ? 'odd' : 'even'} role="row">{
                                                                            showPrintPanel ?
                                                                                <td className="">
                                                                                    <div className="be-checkbox custom-control custom-checkbox mb-0">
                                                                                        <span>
                                                                                            <input
                                                                                                id={`checked-order${index}`}
                                                                                                type="checkbox"
                                                                                                className="custom-control-input"
                                                                                                checked={this.state.selected[item._original.id] === true}
                                                                                                onChange={() => this.toggleRow(item._original.id)}
                                                                                            />
                                                                                            <label className="custom-control-label" htmlFor={`checked-order${index}`}></label>
                                                                                        </span>
                                                                                    </div>
                                                                                </td>
                                                                                : null
                                                                        }
                                                                            <td className="">
                                                                                {(state.currentPage - 1) * state.pageSize + index + 1} (#{item.id})
                                                                            </td>
                                                                            <td className="order-number-column">
                                                                                {item.number}
                                                                            </td>
                                                                            <td className="">
                                                                                {item.created_at}
                                                                            </td>
                                                                            <td className="table-column-location-name">
                                                                                {item.bill_to}
                                                                            </td>
                                                                            <td className="table-column-location-name">
                                                                                {item.ship_to}
                                                                            </td>
                                                                            <td className="table-column-vendor-name">
                                                                                {colorizedVendorList(item._original).length ? colorizedVendorList(item._original) : '-'}
                                                                            </td>
                                                                            {/* <td className="">
                                                                                {
                                                                                    item.statuses.map((status, i) => {
                                                                                        return (
                                                                                            <span key={i} className={`order-status ${colorizeStatus(status.status)}`}>{status.label} {status.count > 1 ? '(x' + status.count +')' : ''}</span>
                                                                                        )
                                                                                    })
                                                                                }
                                                                            </td> */}
                                                                            <td className="digit">
                                                                                {convertToPrice(item.total)}
                                                                            </td>

                                                                            <td className="">
                                                                                <div className="">
                                                                                    {/* <a className="btn btn-primary btn-sm mr-2"
                                                                                        href="#" onClick={(e) => {
                                                                                            e.preventDefault();
                                                                                            this.setState({ modalBuyIsOpen: true, orderId: item._original.id })
                                                                                        }}>
                                                                                        Buy
                                                                                    </a> */}
                                                                                    <a className="btn btn-primary btn-sm"
                                                                                       href="#" onClick={(e) => {
                                                                                        e.preventDefault();
                                                                                        this.setState({
                                                                                            modalIsOpen: true,
                                                                                            orderId: item._original.id
                                                                                        })
                                                                                    }}>
                                                                                        View Order
                                                                                    </a>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    );
                                                                })}
                                                                </tbody>
                                                            }
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row be-datatable-footer">
                                                <TablePaginationComponent tableState={state} tableInstance={instance} />
                                            </div>
                                        </div>
                                    )
                                }}
                            </ReactTable>
                        </div>
                    </div>


                    <OrderFormContainer modalIsOpen={this.state.modalIsOpen}
                        orderId={this.state.orderId}
                        closeModal={() => {
                            this.setState({
                                modalIsOpen: false,
                                orderId: null
                            });
                        }}
                        afterSuccess={() => {
                            this.reloadList();
                        }}
                    />

                    <ProductPurchaseHistoryComponent data={this.props.printData} filters={this.state.filter} />
                    <OrderStatusReportComponent data={this.props.printData} filters={this.state.filter} />
                    <TotalPurchaseReceivedComponent data={this.props.printData} filters={this.state.filter} />
                    <PrintEasyOrderFormComponent clinic = {this.props.clinic.name}/>

                </div>
        );
    }
}

OrderListComponent.propTypes = {
    vendors: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    currentPage: PropTypes.number,
    dispatch: PropTypes.func,
    history: PropTypes.object,
    loading: PropTypes.bool,
    location: PropTypes.object,
    match: PropTypes.object,
    pageSize: PropTypes.number,
    pages: PropTypes.number,
    sortby: PropTypes.array
}
