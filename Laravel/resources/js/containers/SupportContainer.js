import { connect } from 'react-redux';
import SupportComponent from '../components/SupportComponent';

const mapStateToProps = (state) => {
    const user = state.authReducer.auth.user;
    const clinic = user && user.clinic ? user.clinic : {};
    const response = state.supportReducer.store.response ? state.supportReducer.store.response : {};
    return {
        state,
        user: user ? user : {},
        name: user && user.name ? user.name : '',
        email: user && user.email ? user.email : '',
        account: clinic && clinic.name ? clinic.name : '',
        errors: response.errors ? response.errors : {},
        // message: state.authReducer.auth.message,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    };
};

const SupportContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SupportComponent);

export default SupportContainer;