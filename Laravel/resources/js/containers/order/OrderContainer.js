import { connect } from 'react-redux';
import OrderComponent from '../../components/order/OrderComponent';

const mapStateToProps = (state) => {
    return {
        state,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    };
};

const OrderContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(OrderComponent);

export default OrderContainer;