import {connect} from 'react-redux';
import OrderFormComponent from "../../components/order/OrderFormComponent";

const mapStateToProps = (state) => {
    const { order, response, submissionsResultErrors, submissionsResultMessages } = state.orderReducer.store;
    const author = state.authReducer.auth.user;
    const clinic = author ? author.clinic: {};

    const { price, prices } = state.priceReducer.store;

    return {
        state,
        order: order ? order : {},
        errors: response && response.errors ? response.errors : {},

        // products: state.productReducer.store.vendors || [],
        selectedPrice: price ? price : {},
        prices: prices,
        
        locationList: state.locationReducer.store.locations || [],
        billToLocations: state.locationReducer.store.billToLocations || [],
        
        author: author,
        clinic: clinic,

        submissionsResultErrors: submissionsResultErrors ? submissionsResultErrors : [],        
        submissionsResultMessages: submissionsResultMessages ? submissionsResultMessages : [],

        printData: state.orderReducer.store.printData || [],
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    };
};

const OrderFormContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(OrderFormComponent);

export default OrderFormContainer;