import {connect} from 'react-redux';
import OrderListComponent from '../../components/order/OrderListComponent';

const mapStateToProps = (state) => {
    const author = state.authReducer.auth.user;
    const clinic = author ? author.clinic : {};
    
    return {
        state,
        orders: state.orderReducer.store.orders || [],
        vendorList: state.vendorReducer.store.vendors || [],
        locationList: state.locationReducer.store.locations || [],
        billToLocations: state.locationReducer.store.billToLocations || [],

        total: state.orderReducer.store.total || 0,
        pages: state.orderReducer.store.pages || 1,

        loading: state.orderReducer.store.loading || false,
        downloadedFile: state.orderReducer.store.downloadedFile || null,
        printData: state.orderReducer.store.printData || [],

        author: author,
        clinic: clinic
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    };
};

const OrderListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(OrderListComponent);

export default OrderListContainer;