import { connect } from 'react-redux';
import LoginSidebarNavComponent from '../components/LoginSidebarNavComponent';

const mapStateToProps = (state) => {
    return {
        state,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    };
};

const LoginSidebarNavContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginSidebarNavComponent);

export default LoginSidebarNavContainer;