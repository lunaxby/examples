import {connect} from 'react-redux';
import UserFormComponent from "../../components/user/UserFormComponent";

const mapStateToProps = (state) => {
    const { user, response } = state.userReducer.store;
    const author = state.authReducer.auth.user;

    return {
        state,
        user: user ? user : {},
        errors: response && response.errors ? response.errors : {},

        author: state.authReducer.auth.user,
        clinic: author ? author.clinic : {},
        clinicLocations: author && author.clinic ? author.clinic.locations.map(location => ({ value: location.id, label: location.name })) : []
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    };
};

const UserFormContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(UserFormComponent);

export default UserFormContainer;