import {combineReducers} from 'redux';
import orderActions, {RESET_ERROR} from '../actions/order';

const {
    INITIAL_ORDER_LIST,
    REQUEST_ORDERS,
    RECEIVE_ORDERS,
    RECEIVE_BUY_ORDERS,
    RECEIVE_ORDER,
    SAVE_ORDER_RESPONSE,
    CREATE_ORDER,
    CANCEL_SUBORDERS_RESPONSE,
    CLOSE_SUBORDERS_RESPONSE,
    OPEN_SUBORDERS_RESPONSE,
    EMAIL_SUBORDERS_RESPONSE,
    SUBMISSION_PAGE_ERROR,
    SUBMISSION_ORDER_RESPONSE,

    IMPORT_PRODUCTS_RESPONSE,
    RECEIVE_LOW_STOCK_INVENTORIES,

    EXPORT_ORDERS_RESPONSE,
    PRINT_ORDERS_RESPONSE
} = orderActions;

const initState = {
    ordersToBuy: [],
    orders: [],
    order: {},
    prices: [],
    importFromScannerInfo: {},
    lowStockInventories: [],
    downloadedFile: null,
    printData: [],

    submissionsResultErrors: [],
    submissionsResultMessages: [],
};

const store = (state = initState, action) => {
    switch (action.type) {
        case INITIAL_ORDER_LIST:
            return {
                ...state,
                downloadedFile: null,
            };
        case CREATE_ORDER:
            return {
                ...state,
                order: action.data,
            };
        case REQUEST_ORDERS:
            return {
                ...state,
                loading: action.loading,
            };
        case RECEIVE_ORDERS:
            return {
                ...state,
                loading: action.loading,
                orders: action.data.data,

                total: action.data.meta.total,
                pages: action.data.meta.last_page
            };
        case RECEIVE_BUY_ORDERS:
            return {
                ...state,
                ordersToBuy: action.data.data,
            };
        case RECEIVE_ORDER:
            return {
                ...state,
                order: action.data,
                response: {},
            };
        case SAVE_ORDER_RESPONSE:
            return {
                ...state,
                response: action.data,
            };
        case CANCEL_SUBORDERS_RESPONSE:
            return {
                ...state,
                response: action.data,
            };
        case CLOSE_SUBORDERS_RESPONSE:
            return {
                ...state,
                response: action.data,
            };
        case OPEN_SUBORDERS_RESPONSE:
            return {
                ...state,
                response: action.data,
            };
        case EMAIL_SUBORDERS_RESPONSE:
            return {
                ...state,
                response: action.data,
            };
        case SUBMISSION_PAGE_ERROR:
            return {
                ...state,
                submissionPageError: action.data,
            };
        case SUBMISSION_ORDER_RESPONSE:
            return {
                ...state,
                response: action.data,
                submissionSuccess: !action.data.hasErrors,

                submissionsResultErrors: action.data.errors ? action.data.errors.rows : [],
                submissionsResultMessages: action.data.messages ? action.data.messages : [],
            };
        case IMPORT_PRODUCTS_RESPONSE:
            return {
                ...state,
                response: action.data,
                importFromScannerInfo: action.data.info ? action.data.info : {},
                prices: action.data.data ? action.data.data : [],
            };
        case RECEIVE_LOW_STOCK_INVENTORIES:
            return {
                ...state,
                lowStockInventories: action.data.data ? action.data.data : [],
                lowStockTotal: action.data.meta ? action.data.meta.total : 0,
                lowStockPages: action.data.meta ? action.data.meta.last_page : 1,
            };
        case EXPORT_ORDERS_RESPONSE:
            return {
                ...state,
                response: action.data,
                downloadedFile: action.data.path ? action.data.path : null
            };
        case PRINT_ORDERS_RESPONSE:
            return {
                ...state,
                printData: action.data.data ? action.data.data : [],
            };
        case RESET_ERROR:
            return {
                ...state,
                response: action.data,
            };
        default:
            return state;
    }
};

const orderReducer = combineReducers({
    store,
});

export default orderReducer;
