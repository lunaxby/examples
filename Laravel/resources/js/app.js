/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import React from 'react';
import {render} from 'react-dom';
import { Router, Route } from 'react-router-dom';
import { Switch } from 'react-router';
import {createStore, applyMiddleware, compose } from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import history from '../common/history';
import rootReducer from './reducers/root';
const middleware = [thunk];

const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        }) : compose;

const enhancer = composeEnhancers(
    applyMiddleware(...middleware),
    // other store enhancers if any
);
const store = createStore(
    rootReducer,
    enhancer
);

import AppContainer from './containers/AppContainer';
if (document.getElementById('app')) {
    render(
        <Provider store={store}>
            <Router history={history}>
                <AppContainer/>
            </Router>
        </Provider>,
        document.getElementById('app')
    );
}
