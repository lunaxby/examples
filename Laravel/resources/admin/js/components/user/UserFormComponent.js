import React from 'react';

import userActions from '../../actions/user';
const { fetchUser, saveUser, createUser, resetError } = userActions;

import { fetchClinics, fetchClinic } from '../../actions/clinic';
import { USER_ROLE_ADMIN, USER_ROLE_POWER_USER, USER_ROLE_USER } from '../../../../common/user-role'

import toastr from "../../common/toastr";
import Modal from "../../common/modal";
import PropTypes from "prop-types";

import Select from 'react-select'
import Skeleton from 'react-loading-skeleton';
import swal from "sweetalert";

export default class UserFormComponent extends React.Component {
    initialState() {
        this.props.dispatch(createUser());

        const roleList = [
            { value: USER_ROLE_ADMIN, label: 'DHPI Admin' },
            { value: USER_ROLE_POWER_USER, label: 'Clinic Power User' },
            { value: USER_ROLE_USER, label: 'Clinic User' }
        ];

        const statusList = [
            { value: 0, label: 'Disabled' },
            { value: 1, label: 'Enabled' }
        ];

        return {
            formTitle: 'New user',
            submitBtnText: 'Save',

            id: null,
            name: '',
            email: '',
            clinic_id: null,
            locations: [],

            role: roleList[0],
            status: statusList[1],
            password: '',
            autoGeneratePassword: 0,

            loading: false,
            submiting: false,

            roleList: roleList,
            statusList: statusList,
            passwordEnabled: true
        };
    }

    constructor(props) {
        super(props);
        this.state = this.initialState();
    }

    componentWillMount() {
        Modal.setAppElement('body');
    }

    async afterOpenModal() {
        this.setState({ loading: true });
        try {
            await this.props.dispatch(fetchClinics({ 'all': 1, 'sortby': [{ "id": "name", "asc": true }] }));
        } catch (e) {
            this.errorMessage(e.message ? e.message : e.error);
        }
        this.setState({ loading: false });

        if (this.props.userId) {
            this.setState({
                formTitle: '',
                loading: true,
            });
            await this.props.dispatch(fetchUser({
                userId: this.props.userId,
            }));

            const { user } = this.props;
            this.setState(user);

            if (user.clinic && user.clinic.id) {
                await this.props.dispatch(fetchClinic({
                    clinicId: user.clinic.id,
                }));
            }

            const userClinic = this.props.clinics.filter((e) => {
                return e.id === (user.clinic ? user.clinic.id : null)
            })[0]

            const locations = userClinic ? userClinic.locations.filter((location) => { return -1 !== user.locations.indexOf(location.id) }) : [];

            this.setState({
                role: this.state.roleList.filter((e) => {
                    return e.value === user.role
                })[0],
                status: this.state.statusList.filter((e) => {
                    return e.value === user.status
                })[0],
                clinic_id: userClinic ? { value: userClinic.id, label: userClinic.name } : null,
                locations: locations.map((location) => ({ value: location.id, label: location.name }))
            })

            if (user.name) {
                this.setState({
                    formTitle: `Edit User: ${user.name}`,
                    loading: false
                });
            }
        }
    }

    async handleSelectInputChange(selectedItem, name) {
        this.setState({
            [name]: selectedItem,
        });

        if (name === 'clinic_id') {
            this.setState({ locations: [] });
            if (selectedItem.value) {
                await this.props.dispatch(fetchClinic({
                    clinicId: selectedItem.value,
                }));
            }
        }
    }
    handleSelectInputRoleChange(selectedItem, name) {
        this.setState({
            [name]: selectedItem,
        });

        const role = selectedItem.value;
        if (role === USER_ROLE_ADMIN) {
            this.resetErrorMessage(name);
            this.setState({ clinic_id: null });
        }
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.resetErrorMessage(name);
        this.setState({
            [name]: value,
        });
    }

    handleCheckboxAutogeneratePasswordCheck(event) {
        const value = event.target.checked;

        this.setState({
            autoGeneratePassword: value,
        });

        if (value) {
            this.setState({password: ''})
        }
        this.resetErrorMessage('password');
        this.setState({
            passwordEnabled: !value
        })
    }

    roleAdmin() {
        const { role } = this.state;
        return role && role.value === USER_ROLE_ADMIN;
    }

    roleClinicUser() {
        const { role } = this.state;
        return role && role.value === USER_ROLE_USER;
    }

    async handleSubmit(event) {
        event.preventDefault();
        const { role, clinic_id, locations, status, autoGeneratePassword } = this.state;

        this.setState({ submiting: true});
        try {
            await this.props.dispatch(saveUser({
                id: this.state.id,
                name: this.state.name,
                email: this.state.email,

                role: role ? role.value : null,
                clinic_id: !this.roleAdmin() && clinic_id ? clinic_id.value : null,
                locations: this.roleClinicUser() && locations ? locations.map(location => location.value) : [],
                status: status ? status.value : null,

                password: this.state.password,
                autoGeneratePassword: Number(autoGeneratePassword)
            }));
            this.successMessage(`User was ${this.state.id ? 'saved' : 'created'}`);
            this.close();
            this.props.afterSuccess();

        } catch (e) {
            this.errorMessage(e.message ? e.message : `User was not ${this.state.id ? 'saved' : 'created'}`);
        }
        this.setState({ submiting: false});
    }

    successMessage(message) {
        toastr.success(message, 'Success');
    }

    errorMessage(message) {
        toastr.error(message, 'Error');
    }

    resetErrorMessage(name) {
        const err = this.props.errors;
        if (err[name]) {
            delete err[name];
        }
        this.props.dispatch(resetError(err));
    }

    close() {
        this.setState(this.initialState());
        this.props.closeModal();
    }

    async reload(e) {
        e.preventDefault();
        const willReset = await swal({
            title: "Are you sure?",
            text: "Are you sure you want to reset changes?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        if (!willReset) {
            return;
        }
        this.afterOpenModal();
    }

    render() {
        let { id, name, email, role, roleList, status, statusList, password, clinic_id, locations, autoGeneratePassword, passwordEnabled, formTitle, submitBtnText, loading, submiting } = this.state;
        const { errors, clinics, clinicLocations } = this.props;

        return (
            <div>
                <Modal
                    isOpen={this.props.modalIsOpen}
                    onAfterOpen={(e) => { this.afterOpenModal() }}
                    onRequestClose={() => { this.close() }}
                    style={{ content: { ...Modal.customStyles.content, overflow: 'visible' }}}
                >
                    <form onSubmit={(e) => { this.handleSubmit(e) }}>
                        <div className="card-header card-header-divider">
                            {loading && !formTitle && <Skeleton height={25} width={250} />}
                            <div className="short-title-wrapper">{formTitle}</div>
                            <div className="tools">
                                <button type="button" className="close" id="cross-close-btn" data-dismiss="modal" aria-label="Close"
                                    onClick={() => { this.close() }}>
                                    <span className="mdi mdi-close" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                        <div className="modal-body">
                            <div className="form-group row required">
                                <label className="col-3 col-lg-1 col-form-label text-right">User name:</label>
                                <div className="col-9 col-lg-11">
                                    {loading ? <Skeleton height={32} /> :
                                        <input
                                            className={`form-control form-control-sm ${errors.name ? ' is-invalid' : ''}`}
                                            type="text"
                                            placeholder="Name"
                                            name="name"
                                            onChange={(e) => { this.handleInputChange(e) }}
                                            value={name}
                                            required
                                            autoFocus />
                                    }
                                    <div className="invalid-feedback">{errors.name && errors.name[0]}</div>
                                </div>
                            </div>

                            <div className="form-group row required">
                                <label className="col-3 col-lg-1 col-form-label text-right">Email:</label>
                                <div className="col-9 col-lg-11">
                                    {loading ? <Skeleton height={32} /> :
                                        <input
                                            className={`form-control form-control-sm ${errors.email ? ' is-invalid' : ''}`}
                                            type="text"
                                            placeholder="Email"
                                            name="email"
                                            onChange={(e) => { this.handleInputChange(e) }}
                                            value={email}
                                            required
                                        />
                                    }
                                    <div className="invalid-feedback">{errors.email && errors.email[0]}</div>
                                </div>
                            </div>

                            <div className="form-group row required">
                                <label className="col-3 col-lg-1 col-form-label text-right">Type:</label>
                                <div className="col-9 col-lg-11">
                                    {loading ? <Skeleton height={32} /> :
                                        <Select
                                            className={`react-select${errors.role ? ' is-invalid' : ''}`}
                                            options={roleList}
                                            onChange={(selectedItem) => { this.handleSelectInputChange(selectedItem, 'role') }}
                                            placeholder="Type"
                                            required
                                            name="role"
                                            isSearchable={true}
                                            value={role} />
                                    }
                                    <div className="invalid-feedback">{errors.role && errors.role[0]}</div>
                                </div>
                            </div>

                            {!this.roleAdmin() ?
                                <div className="form-group row required">
                                    <label className="col-3 col-lg-1 col-form-label text-right">Clinic:</label>
                                    <div className="col-9 col-lg-11">
                                        {loading ? <Skeleton height={32} /> :
                                            <Select
                                                className={`react-select${errors.clinic_id ? ' is-invalid' : ''}`}
                                                options={clinics.map(clinic => ({ value: clinic.id, label: clinic.name }))}
                                                onChange={(selectedItem) => { this.handleSelectInputChange(selectedItem, 'clinic_id') }}
                                                placeholder="Clinic"
                                                required
                                                name="clinic_id"
                                                isSearchable={true}
                                                value={clinic_id} />
                                        }
                                        <div className="invalid-feedback">{errors.clinic_id && errors.clinic_id[0]}</div>
                                    </div>
                                </div>
                                : ''
                            }

                            {this.roleClinicUser() ?
                                <div className="form-group row">
                                    <label className="col-3 col-lg-1 col-form-label text-right">Locations:</label>
                                    <div className="col-9 col-lg-11">
                                        {loading ? <Skeleton height={32} /> :
                                            <Select
                                                className={`react-select${errors.locations ? ' is-invalid' : ''}`}
                                                options={clinicLocations}
                                                onChange={(selectedItem) => { this.handleSelectInputChange(selectedItem, 'locations') }}
                                                placeholder="Locations"
                                                isMulti={true}
                                                required
                                                name="locations"
                                                isSearchable={true}
                                                value={locations} />
                                        }
                                        <div className="invalid-feedback">{errors.clinic_id && errors.clinic_id[0]}</div>
                                        <small id="locationsHelp" className="form-text text-muted">A user can view inventory and manage items of the selected locations</small>
                                    </div>
                                </div>
                                : ''
                            }
                            { id ?
                                <div className="form-group row required">
                                    <label className="col-3 col-lg-1 col-form-label text-right">Status:</label>
                                    <div className="col-9 col-lg-11">
                                        {loading ? <Skeleton height={32} /> :
                                            <Select
                                                className={`react-select${errors.status ? ' is-invalid' : ''}`}
                                                options={statusList}
                                                onChange={(selectedItem) => { this.handleSelectInputChange(selectedItem, 'status') }}
                                                placeholder="Status"
                                                required
                                                name="status"
                                                isSearchable={true}
                                                value={status} />
                                        }
                                        <div className="invalid-feedback">{errors.status && errors.status[0]}</div>
                                    </div>
                                </div> : null
                            }

                            <div className={`form-group row ${!id && !autoGeneratePassword ? 'form-group required' : ''}`}>
                                <label className="col-3 col-lg-1 col-form-label text-right">Password:</label>
                                <div className="col-9 col-lg-11">
                                    {loading ? <Skeleton height={32} /> :
                                        <input
                                            className={`form-control form-control-sm ${errors.password ? ' is-invalid' : ''}`}
                                            type="password"
                                            placeholder="Type here to set new password"
                                            name="password"
                                            onChange={(e) => { this.handleInputChange(e) }}
                                            readOnly={!passwordEnabled}
                                            autoComplete="new-password"
                                        />
                                    }
                                    <div className="invalid-feedback">{errors.password && errors.password[0]}</div>

                                    <div className="be-checkbox custom-control custom-checkbox mt-1">
                                        {loading ? <Skeleton height={16} /> :
                                            <span>
                                                <input id="autoGeneratePassword"
                                                    className="custom-control-input"
                                                    type="checkbox"
                                                    name="autoGeneratePassword"
                                                    value="1"
                                                    checked={autoGeneratePassword}
                                                    onChange={(e) => { this.handleCheckboxAutogeneratePasswordCheck(e) }}
                                                />
                                                <label className="custom-control-label no-asterisk" htmlFor="autoGeneratePassword">Send New Auto-Generated Password When Saving the User</label>
                                            </span>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            {loading ? <Skeleton height={50} width={70} /> :
                                <button
                                    disabled={submiting}
                                    id="submit-btn"
                                    style={submiting ? { cursor: 'progress' } : {}}
                                    className="btn btn-primary" type="submit">{submitBtnText}</button>
                            }
                            &nbsp;
                            {this.state.id ? (loading ? <Skeleton height={50} width={70} /> :
                                <button onClick={(e) => { this.reload(e) }}
                                    id="reset-btn"
                                    className="btn btn-light">Reset</button>) : ''
                            }
                            &nbsp;
                        {loading ? <Skeleton height={50} width={70} /> :
                                <button onClick={() => { this.close() }}
                                    id="close-btn"
                                    className="btn btn-light" data-dismiss="modal">Close</button>
                            }
                        </div>
                    </form>
                </Modal>
            </div>
        );
    }
}

UserFormComponent.propTypes = {
    afterSuccess: PropTypes.func,
    userId: PropTypes.number,
    closeModal: PropTypes.func,
    dispatch: PropTypes.func,
    errors: PropTypes.object,
    modalIsOpen: PropTypes.bool
}
