import React from 'react';
import { Route } from 'react-router-dom';
import { Switch } from 'react-router';
import UserListContainer from "../../containers/user/UserListContainer";
import PropTypes from "prop-types";

export class UserComponent extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Switch>
                <Route exact path={`${this.props.match.url}`} component={UserListContainer} />
            </Switch>
        );
    }
}

export default UserComponent;

UserComponent.propTypes = {
    dispatch: PropTypes.func,
    match: PropTypes.object,
}