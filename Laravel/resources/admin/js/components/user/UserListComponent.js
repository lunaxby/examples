import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import userActions from '../../actions/user';
const { fetchUsers, deleteUser } = userActions;

import TableShowComponent from "../widgets/TableShowComponent";
import TableSearchComponent from "../widgets/TableSearchComponent";
import TablePaginationComponent from "../widgets/TablePaginationComponent";
import { USER_TITLE_ROLES } from '../../../../common/user-role'

import toastr from "../../common/toastr";
import UserFormContainer from "../../containers/user/UserFormContainer";
import PropTypes from "prop-types";

import swal from 'sweetalert';

import Skeleton from 'react-loading-skeleton';
import {NavLink} from "react-router-dom";
import {DEFAULT_PAGE_SIZE, DEFAULT_PAGE_SIZE_OPTIONS} from "../../../../common/grid";

export default class UserListComponent extends React.Component {
    constructor(props) {
        super(props);

        this.onSort = this.onSort.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.handleDeleteUser = this.handleDeleteUser.bind(this);

        // Modal
        this.state = {
            modalIsOpen: false,
            userId: null,

            currentPage: 1,
            pageSize: DEFAULT_PAGE_SIZE,
        };

        this.statusList = {
            0: 'Disabled',
            1: 'Enabled',
        }

        this.defaultSorted = [
            {
                id: 'clinic',
                asc: true,
            },
        ];
    }

    componentDidMount() {
        this.reloadList();
    }

    successMessage(message) {
        toastr.success(message, 'Success');
    }

    errorMessage(message) {
        toastr.error(message, 'Error');
    }

    fetchData(state, instance) {
        this.props.dispatch(fetchUsers({
            page: state.page + 1,
            per_page: state.pageSize,
            sortby: state.sorted,
            search: state.search,
        }));
    }

    onSort(e, accessor, state, instance, sortMethod) {
        e.preventDefault();

        state.sorted = [{
            id: accessor,
            [sortMethod]: true,
        }];

        instance.state.sorted = state.sorted;
        this.fetchData(state, instance);
    }

    async reloadList() {
        let { currentPage, pageSize, sortby } = this.props;
        let { search } = this.state;

        if (!sortby || !sortby.length) {
            sortby = this.defaultSorted;
        }

        try {
            await this.props.dispatch(fetchUsers({
                page: currentPage,
                per_page: pageSize,
                sortby: sortby,
                search: search,
            }));
        } catch (e) {
            this.errorMessage(e.message ? e.message : e.error);
        }
    }

    async handleDeleteUser(id) {
        const willDelete = await swal({
            title: "Are you sure?",
            text: "Are you sure you want to delete this User?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        if (!willDelete) {
            return;
        }

        try {
            await this.props.dispatch(deleteUser(id));

            this.successMessage('User successfully deleted!');
            this.reloadList();
        } catch (e) {
            this.errorMessage(e.message ? e.message : 'User was not deleted');
        }
    }

    render() {
        const { pages, loading, users, total, currentPage, sortby, authUser } = this.props;
        const { pageSize } = this.state;

        const columns = [
            {
                Header: '#',
                id: 'id',
                accessor: 'id',
                sortable: true,
                // width: 80,
            },
            {
                Header: 'Clinic',
                id: 'clinic',
                accessor: (user) => {
                    const { clinic } = user;
                    return clinic ? clinic.name : '';
                },
                sortable: true,
            },
            {
                Header: 'Name',
                id: 'name',
                accessor: 'name',
                sortable: true,
                width: '20%',
            },
            {
                Header: 'Email',
                id: 'email',
                accessor: 'email',
                sortable: true,
            },
            {
                Header: 'Locations',
                accessor: 'locations',
                id: 'locations',
                accessor: (user) => {
                    const { clinic } = user;
                    const locationIds = user.locations;

                    const locations = clinic ? clinic.locations.filter((location) => { return -1 !== locationIds.indexOf(location.id) }) : [];

                    let locationsList = '';
                    locations.map((location) => { locationsList += location.name + ', '; });
                    return locationsList.substring(0, locationsList.length - 2);
                },
                sortable: false,
            },
            {
                Header: 'Type',
                id: 'role',
                accessor: 'role',
                sortable: true,
            },
            {
                Header: 'Status',
                id: 'status',
                accessor: 'status',
                sortable: true,
            },
            {
                Header: '',
                accessor: '',
                sortable: false,
                width: 65,
            },
        ];

        const skeletonRows = [];
        for (let i = 0; i < 5; i++) {
            skeletonRows.push(
                <tr key={i}>
                    {
                        columns.map((column, index) => {
                            return (
                                <td key={index}><Skeleton height={33} /></td>
                            )
                        })
                    }
                </tr>
            )
        }

        const setSortedClass = (column) => {
            if (Array.isArray(sortby)) {
                let currentSort = sortby.find((sortItem) => {
                    const sortItemObject = JSON.parse(sortItem);
                    return sortItemObject.id === column.id;
                });

                if (currentSort) {
                    currentSort = JSON.parse(currentSort);
                    return currentSort.desc ? 'sorting_desc' : 'sorting_asc';
                }
            }
            return 'sorting'
        }

        return (
            <div className="card card-table">
                <div className="card-header">
                    Manage Users
                    <div className="tools">
                        <button onClick={() => { this.setState({ modalIsOpen: true, userId: null }) }} className="btn btn-primary float-right" id="add-item-btn">New User</button>

                        <NavLink className="btn btn-secondary float-right mr-1"
                            activeClassName="active"
                            to={`/admin/history`}
                            exact>Login History
                                    </NavLink>
                    </div>
                </div>
                <div className="card-body">
                    <div id="table_users_wrapper" className="dataTables_wrapper dt-bootstrap4 no-footer">

                        <ReactTable
                            manual
                            defaultPageSize={pageSize}
                            data={users}
                            columns={columns}
                            pages={pages}
                            onFetchData={this.fetchData}
                            loading={loading}
                            total={total}
                            currentPage={currentPage}
                            pageSizeOptions={DEFAULT_PAGE_SIZE_OPTIONS}
                            defaultSorted={this.defaultSorted}
                        >
                            {(state, makeTable, instance) => {
                                return (
                                    <div>

                                        <div className="row be-datatable-header">
                                            <div className="col-sm-6">
                                                <div className="dataTables_length">
                                                    {/* show items */}
                                                    <TableShowComponent tableState={state} tableInstance={instance} />

                                                </div>
                                            </div>
                                            <div className="col-sm-6">
                                                <div className="dataTables_filter">
                                                    {/* search */}
                                                    <TableSearchComponent
                                                        placeholder={"Search Users (User Name, Email or Locations)"}
                                                        tableState={state}
                                                        onChangeCB={(search) => { this.setState({ search: search }) }}
                                                        tableInstance={instance} />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row be-datatable-body">
                                            <div className="col-sm-12">
                                                <div className="table-responsive">
                                                    <table className="table table-striped table-hover table-fw-widget dataTable no-footer dtr-inline">
                                                        <thead>
                                                            <tr role="row">
                                                                {state.columns.map((item, index) => {
                                                                    return (
                                                                        <th key={index} width={item.width} className={`${item.sortable ? setSortedClass(item) : ''}`}
                                                                            onClick={(e) => { item.sortable ? this.onSort(e, item.id, state, instance, setSortedClass(item) == 'sorting_asc' ? 'desc' : 'asc') : null }}
                                                                        >
                                                                            {item.Header}
                                                                        </th>
                                                                    )
                                                                })}
                                                            </tr>
                                                        </thead>
                                                        {loading ?
                                                            <tbody className="g-font-size-default g-color-black">
                                                                {skeletonRows}
                                                            </tbody>
                                                            :
                                                            <tbody>
                                                                {state.pageRows.map((item, index) => {
                                                                    return (
                                                                        <tr key={index} className={index % 2 == 0 ? 'odd' : 'even'} role="row">
                                                                            <td className="">
                                                                                {(state.currentPage - 1) * state.pageSize + index + 1} (#{item.id})
                                                                            </td>
                                                                            <td style={{ maxWidth: '150px' }}>
                                                                                {item.clinic}
                                                                            </td>
                                                                            <td className="table-column-user-name">
                                                                                {item.name}
                                                                            </td>
                                                                            <td className="table-column-email">
                                                                                {item.email}
                                                                            </td>
                                                                            <td className="table-column-location-name">
                                                                                {item.locations}
                                                                            </td>
                                                                            <td className="">
                                                                                {USER_TITLE_ROLES[item.role] ? USER_TITLE_ROLES[item.role] : item.role}
                                                                            </td>
                                                                            <td className="">
                                                                                {this.statusList[item.status] ? this.statusList[item.status] : item.status}
                                                                            </td>
                                                                            <td>
                                                                                <a className="btn btn-primary btn-sm mr-2"
                                                                                    href="#" onClick={(e) => {
                                                                                        e.preventDefault();
                                                                                        this.setState({ modalIsOpen: true, userId: item._original.id })
                                                                                    }}>
                                                                                    Edit
                                                                                    </a>
                                                                                {authUser.id == item._original.id ? null :
                                                                                    <a
                                                                                        href="#" onClick={(e) => {
                                                                                            e.preventDefault();
                                                                                            this.handleDeleteUser(item._original.id)
                                                                                        }}>
                                                                                        <i className="far fa-trash-alt"></i>
                                                                                    </a>
                                                                                }
                                                                            </td>
                                                                        </tr>
                                                                    );
                                                                })}
                                                            </tbody>
                                                        }
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row be-datatable-footer">
                                            <TablePaginationComponent tableState={state} tableInstance={instance} />
                                        </div>

                                    </div>
                                )
                            }}
                        </ReactTable>
                    </div>
                </div>

                <UserFormContainer modalIsOpen={this.state.modalIsOpen}
                    userId={this.state.userId}
                    closeModal={() => {
                        this.setState({
                            modalIsOpen: false,
                            userId: null
                        });
                    }}
                    afterSuccess={() => {
                        this.reloadList();
                    }}
                />
            </div>
        );
    }
}

UserListComponent.propTypes = {
    users: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    currentPage: PropTypes.number,
    dispatch: PropTypes.func,
    history: PropTypes.object,
    loading: PropTypes.bool,
    location: PropTypes.object,
    match: PropTypes.object,
    pageSize: PropTypes.number,
    pages: PropTypes.number,
    sortby: PropTypes.array
}
