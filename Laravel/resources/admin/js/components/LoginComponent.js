import React from 'react';
import {Link} from "react-router-dom";
import {
    login,
    changeLogin
} from "../actions/auth";

import { removeUserFromLocalStorage } from "../../../common/local-storage";
import swal from 'sweetalert';

import toastr from "../common/toastr";
import LogoComponent from '../../../common/components/LogoComponent'

export default class LoginComponent extends React.Component {
    initialState() {
        return {
            loading: false
        };
    }
    
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = this.initialState();
    }

    onChange(e) {
        this.props.dispatch(changeLogin({
            [e.target.name]: e.target.value,
        }));
    }

    successMessage(message) {
        toastr.success(message, 'Success');
    }

    errorMessage(message) {
        toastr.error(message, 'Error');
    }

    async onSubmit(e) {
        e.preventDefault();
        let data = this.props.login;

        this.setState({ loading: true });
        try {
            await this.props.dispatch(login(data));
        } catch (e) {
            this.errorMessage(e.message ? e.message : 'Login Error');
            this.setState({ loading: false });
        }
    }

    componentDidMount() {
        const { user: { adminZoneAvailable }, authenticated } = this.props;
        if (authenticated && !adminZoneAvailable) {
            // maybe user or other role (NOT DHPI Admin)
            swal({
                title: "Active Session will be closed",
                text: "Your session will be Closed. Would you like to Logout?",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willLogout) => {
                if (willLogout) {
                    removeUserFromLocalStorage();
                    window.location = '/admin';
                } else {
                    window.history.back();
                }
            });
        }
    }

    render() {
        const { loading } = this.state;

        let login = this.props.login;
        let errors = this.props.errors;

        return (
            <div className="main-content container-fluid">
                <div className="splash-container">
                    <div className="card card-border-color card-border-color-primary">
                        <LogoComponent title={"Admin Login"} />
                        
                        <div className="card-body">
                            <form onSubmit={this.onSubmit}>
                                <div className="mb-2">
                                    <label>Email:</label>
                                    <input
                                        className={`form-control form-control-sm rounded${errors.email ? ' is-invalid' : ''}`}
                                        type="email"
                                        placeholder="Email"
                                        name="email"
                                        value={login.email || ''}
                                        onChange={this.onChange}
                                        required
                                        autoFocus />
                                    {/* <div className="invalid-feedback">{errors.email && errors.email[0]}</div> */}
                                </div>

                                <div className="mb-2">
                                    <label>Password:</label>
                                    <input
                                        className={`form-control form-control-sm rounded${errors.email ? ' is-invalid' : ''}`}
                                        type="password"
                                        placeholder="Password"
                                        name="password"
                                        value={login.password || ''}
                                        onChange={this.onChange}
                                        required />
                                    {/* <div className="invalid-feedback">{errors.email && errors.email[0]}</div> */}

                                    <div className="row justify-content-between">
                                        <div className="col align-self-center">

                                        </div>
                                        <div className="col align-self-center text-right">
                                            <Link className="g-font-size-12"
                                                to="/admin/password/reset">Forgot password?</Link>
                                        </div>
                                    </div>
                                </div>

                                <div className="form-group login-submit">
                                    <button className="btn btn-primary btn-xl"
                                        disabled={loading}
                                        style={loading ? { cursor: 'progress' } : {}}
                                        type="submit">Login
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}