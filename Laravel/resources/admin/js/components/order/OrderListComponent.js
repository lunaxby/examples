import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import {
    statusList,
    colorizedVendorList
} from '../../../../common/order-status'

import {
    FILTER_CLINIC,
    FILTER_SHIP_TO,
    FILTER_BILL_TO,
    FILTER_STATUS,
    FILTER_VENDOR,
    FILTER_ORDER_NUMBER,
    FILTER_PURCHASE_ORDER_NUMBER,
    FILTER_MANUFACTURER_NUMBER,
    FILTER_VENDOR_PRODUCT_NUMBER,
    FILTER_ORDER_DATE,
    FILTER_ORDER_S2K
} from '../../../../common/order-filter'

import {
    ORDER_OUTPUT_TEMPLATE_PRODUCT_PURCHASE_HISTORY,
    ORDER_OUTPUT_TEMPLATE_ORDER_STATUS_REPORT,
    ORDER_OUTPUT_TEMPLATE_TOTAL_PURCHASES_RECEIVED
} from '../../../../common/order-output-template'

import {
    ORDER_OUTPUT_OPERATION_PRINT,
    ORDER_OUTPUT_OPERATION_EXPORT_TO_EXCEL
} from '../../../../common/order-output-operation'

import { DATE_FORMAT, convertToDateTime } from '../../../../common/date'

import { convertToPrice } from '../../../../common/price'

import orderActions from '../../actions/order';
const { fetchOrders } = orderActions;

import vendorActions from '../../actions/vendor';
const { fetchVendors } = vendorActions;

import { fetchAuthUser } from '../../actions/auth';

import {DEFAULT_PAGE_SIZE, DEFAULT_PAGE_SIZE_OPTIONS} from '../../../../common/grid'
import TableShowComponent from "../widgets/TableShowComponent";
import TablePaginationComponent from "../widgets/TablePaginationComponent";

import DateRangePickerComponent from "../widgets/DateRangePickerComponent";

import toastr from "../../common/toastr";

import PropTypes from "prop-types";
import swal from 'sweetalert';
import Skeleton from 'react-loading-skeleton';
import Select from 'react-select'

import OrderFormContainer from "../../containers/order/OrderFormContainer";
import { fetchClinics, fetchClinic, createClinic } from '../../actions/clinic';

import VendorStatusTooltipComponent from '../../../../common/widgets/VendorStatusTooltipComponent'

export default class OrderListComponent extends React.Component {
    initialState() {
        const outputOperationList = [
            { value: ORDER_OUTPUT_OPERATION_PRINT, label: 'Print' },
            { value: ORDER_OUTPUT_OPERATION_EXPORT_TO_EXCEL, label: 'Export to Excel file' },
        ];

        const outputTemplateList = [
            { value: ORDER_OUTPUT_TEMPLATE_PRODUCT_PURCHASE_HISTORY, label: 'Product Purchase History' },
            { value: ORDER_OUTPUT_TEMPLATE_ORDER_STATUS_REPORT, label: 'Order Status Report' },
            { value: ORDER_OUTPUT_TEMPLATE_TOTAL_PURCHASES_RECEIVED, label: 'Total Purchases Received' },
        ];

        return {
            showFilterPanel: false,
            showPrintPanel: false,

            modalIsOpen: false,
            orderId: null,

            currentPage: 1,
            pageSize: DEFAULT_PAGE_SIZE,
            sortby: [],
            // filter: {
            //     [FILTER_SHIP_TO]: [],
            //     [FILTER_BILL_TO]: [],
            //     [FILTER_STATUS]: [],
            //     [FILTER_VENDOR]: [],
            //     [FILTER_ORDER_NUMBER]: '',
            //     [FILTER_PURCHASE_ORDER_NUMBER] : '',
            //     [FILTER_MANUFACTURER_NUMBER]: '',
            //     [FILTER_VENDOR_PRODUCT_NUMBER]: '',
            //     [FILTER_ORDER_DATE]: {
            //         'from': '',
            //         'to': ''
            //     }
            // },
            filter: {},
            search: '',

            outputTemplateList: outputTemplateList,
            outputTemplate: outputTemplateList[0],

            outputOperationList: outputOperationList,
            outputOperation: outputOperationList[0],

            selected: {},
            selectAll: 0,
        }
    }

    constructor(props) {
        super(props);

        this.onSort = this.onSort.bind(this);
        this.fetchData = this.fetchData.bind(this);

        this.state = this.initialState();

        this.statusList = statusList;

        this.defaultSorted = [
            {
                id: 'number',
                desc: true,
            },
        ];
    }

    async componentDidMount() {
        const { author } = this.props;
        if (author) {
            await this.props.dispatch(fetchAuthUser({
                userId: author.id,
            }));
        }

        await this.reloadList();
    }

    successMessage(message) {
        toastr.success(message, 'Success');
    }

    errorMessage(message) {
        toastr.error(message, 'Error');
    }

    fetchData(state, instance) {
        this.state.currentPage = state.page + 1;
        this.state.pageSize = state.pageSize;
        this.state.sortby = state.sorted;
        this.state.search = state.search;

        this.reloadList();
    }

    onSort(e, accessor, state, instance, sortMethod) {
        e.preventDefault();
        state.sorted = [{
            id: accessor,
            [sortMethod]: true,
        }];

        instance.state.sorted = state.sorted;
        this.fetchData(state, instance);
    }

    prepareFilter() {
        const { filter } = this.state;
        const formatedFilters = [];

        Object.keys(filter).map((filterName) => {
            const currentFilter = filter[filterName];
            let filterValue = filter[filterName];

            if (currentFilter) {
                if (Array.isArray(currentFilter)) {
                    filterValue = currentFilter.map((filterValue) => {
                        return filterValue.value;
                    });
                } else if (currentFilter.value) {
                    filterValue = currentFilter.value;
                }
            }

            const emptyFilter = !filterValue || (Array.isArray(filterValue) && filterValue.length == 0);

            if (!emptyFilter) {
                formatedFilters.push({
                    'field': filterName, 'value': filterValue
                })
            }
        })

        return formatedFilters;
    }

    async reloadList() {
        let { search, currentPage, pageSize, sortby } = this.state;

        if (!sortby || !sortby.length) {
            sortby = this.defaultSorted;
        }

        try {
            await this.props.dispatch(fetchOrders({
                page: currentPage,
                per_page: pageSize,
                sortby: sortby,
                search: search,
                filter: this.prepareFilter()
            }));
        } catch (e) {
            this.errorMessage(e.message ? e.message : e.error);
        }
    }

    async afterTogglePanel(panelIsOpenedFlag) {
        const allPanelsAreClosed = !this.state.showFilterPanel && !this.state.showPrintPanel;

        if (this.state[panelIsOpenedFlag]) {
            try {
                await this.props.dispatch(fetchVendors({ 'dhpi': 1 }));

                // const { vendorList } = this.props;

                // if (vendorList && vendorList.length > 0) {
                //     const firstVendor = vendorList[0];
                //     if (firstVendor.name) {
                //         await this.handleSelectFilter({ value: firstVendor.id, label: firstVendor.name }, FILTER_VENDOR);
                //     }
                // }
            } catch (e) {
                this.errorMessage(e.message ? e.message : 'Can\'t load Vendor List');
            }

            try {
                await this.props.dispatch(fetchClinics({'all': 1, 'sortby': [{ "id": "name", "asc": true }]}));
            } catch (e) {
                this.errorMessage(e.message ? e.message : 'Can\'t load Clinic List');
            }

        } else if (allPanelsAreClosed) {
            await this.setState({
                filter: this.initialState().filter
            });
            await this.reloadList();
        }
    }

    async handleShowFilterPanel() {
        const { showFilterPanel } = this.state;
        await this.setState({ showFilterPanel: !showFilterPanel });

        await this.afterTogglePanel('showFilterPanel');
    }

    async handleShowPrintPanel() {
        const { showPrintPanel } = this.state;
        await this.setState({ showPrintPanel: !showPrintPanel });

        await this.afterTogglePanel('showPrintPanel');
    }

    async handleSelect(selectedItem) {
        const { value, name } = selectedItem;

        await this.setState({
            [name]: value,
        });
    }

    async dhpiVendor() {
        const { vendorList } = this.props;
        return vendorList.find((vendor) => { return vendor.dhpi == 1})
    }

    async handleSelectFilter(selectedItem, name, reload = false) {
        await this.setState({
            filter: {
                ...this.state.filter,
                [name]: selectedItem,
            },
            currentPage: 1,
        });

        if (name === FILTER_CLINIC) {
            if (selectedItem && selectedItem.value) {
                await this.props.dispatch(fetchClinic({ clinicId: selectedItem.value }))
            } else {
                await this.props.dispatch(createClinic());

                await this.setState({
                    filter : {
                        ...this.state.filter,
                        [FILTER_CLINIC] : null,
                        [FILTER_SHIP_TO]: null,
                        [FILTER_BILL_TO]: null,
                    }
                })
            }

            // const dhpiVendor = await this.dhpiVendor();
            // await this.setState({
            //     filter: {
            //         ...this.state.filter,
            //         [FILTER_VENDOR]: dhpiVendor ? { value: dhpiVendor.id, label: dhpiVendor.name } : null,
            //     }
            // })
        }

        if (reload) {
            await this.reloadList();
        }
    }

    handleInputChange(event) {
        const target = event.target;
        let value = null;

        switch (target.type) {
            case 'checkbox':
                value = target.checked;
                break;
            case 'file':
                value = target.files[0];
                break;
            default:
                value = target.value;
        }

        const name = target.name;

        this.setState({
            [name]: value,
        });
    }

    toggleRow(id) {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[id] = !this.state.selected[id];
        this.setState({
            selected: newSelected,
            selectAll: 2
        });
    }

    toggleSelectAll() {
        const { orders } = this.props;
        let newSelected = {};

        if (this.state.selectAll === 0) {
            orders.map((product) => {
                newSelected[product.id] = true;
            });
        }

        this.setState({
            selected: newSelected,
            selectAll: this.state.selectAll === 0 ? 1 : 0
        });
    }

    render() {
        const { pages, loading, orders, total, vendorList, locationList, billToLocations, clinics, showBillTo, showShipTo, showVendor } = this.props;
        const { showFilterPanel, showPrintPanel, filter, pageSize, currentPage, sortby } = this.state;

        let printColumns = [];
        let columns = [];

        if (showPrintPanel) {
            printColumns = [
                {
                    id: "checkbox",
                    accessor: "",
                    Header: (x) => {
                        return (
                            <input
                                type="checkbox"
                                className="checkbox"
                                checked={this.state.selectAll === 1}
                                ref={input => {
                                    if (input) {
                                        input.indeterminate = this.state.selectAll === 2;
                                    }
                                }}
                                onChange={() => this.toggleSelectAll()}
                            />
                        );
                    },
                    sortable: false,
                    width: 45,
                    style: { verticalAlign: 'middle' }
                }
            ]
        }

        columns = columns.concat(printColumns, [
            {
                Header: '#',
                id: 'id',
                accessor: 'id',
                sortable: true,
                width: 80,
            },
            {
                Header: 'Clinic',
                id: 'clinic',
                accessor: (order) => {
                    return order.clinic ? order.clinic.name : ''
                },
                sortable: true,
            },
            {
                Header: 'Order #',
                id: 'number',
                accessor: 'number',
                sortable: true,
                width: 150,
            },
            // {
            //     Header: 'Bill To',
            //     id: 'bill_to',
            //     accessor: (order) => {
            //         return order.bill_to ? order.bill_to.name : ''
            //     },
            //     sortable: true,
            // },
            // {
            //     Header: 'Ship To',
            //     id: 'ship_to',
            //     accessor: (order) => {
            //         return order.ship_to ? order.ship_to.name : ''
            //     },
            //     sortable: true,
            // },
            {
                Header: 'Date',
                id: 'created_at',
                accessor: (order) => {
                    return order.created_at ? convertToDateTime(order.created_at) : ''
                },
                width: 200,
                sortable: true,
            },
            {
                Header: 'Vendors',
                id: 'vendors',
                Header: (x) => {
                    return (
                        <div>
                            <span>Vendors and Statuses <VendorStatusTooltipComponent/></span>
                        </div>
                    );
                },
                width: '35%',
                sortable: false,
            },
            // {
            //     Header: 'Statuses',
            //     id: 'statuses',
            //     accessor: (order) => {
            //         const statuses = [];
            //         const { sub_orders } = order;
            //         if (Array.isArray(sub_orders)) {
            //             sub_orders.map((suborder) => {
            //                 if (Array.isArray(suborder.order_prices) && suborder.order_prices.length) {
            //                     const statusLabel = this.statusList[suborder.status] ? this.statusList[suborder.status] : suborder.status;
            //                     const objectStatus = {
            //                         'status': suborder.status,
            //                         'label': statusLabel,
            //                         'count': 1,
            //                     };

            //                     const findIndex = statuses.findIndex(item => item.status === suborder.status);
            //                     if (findIndex != -1) {
            //                         statuses[findIndex].count += 1;
            //                     } else {
            //                         statuses.push(objectStatus);
            //                     }
            //                 }
            //             })
            //         }
            //         return statuses;
            //     },
            //     sortable: false,
            //     width: 220,
            // },
            {
                Header: 'Total',
                id: 'total',
                accessor: 'total',
                sortable: true,
            },
            {
                Header: '',
                accessor: '',
                sortable: false,
                width: 85,
            },
        ])

        const skeletonRows = [];
        for (let i = 0; i < 5; i++) {
            skeletonRows.push(
                <tr key={i}>
                    {
                        columns.map((column, index) => {
                            return (
                                <td key={index}><Skeleton height={33} /></td>
                            )
                        })
                    }
                </tr>
            )
        }

        const setSortedClass = (column) => {
            if (Array.isArray(sortby)) {
                let currentSort = sortby.find((sortItem) => {
                    return sortItem.id === column.id;
                });
                if (currentSort) {
                    return currentSort.desc ? 'sorting_desc' : 'sorting_asc';
                }
            }
            return 'sorting'
        }

        const renderFilters = () => {
            const anyPlaceholder = '-- ANY --';

            const getDateFilterValue = () => {
                let returnDate = [];
                const dateFilter = filter[FILTER_ORDER_DATE];
                if (dateFilter == undefined) {
                    return '';
                }

                if (dateFilter.startDate && dateFilter.endDate) {
                    returnDate.push(dateFilter.startDate ? dateFilter.startDate : '...');
                    if (dateFilter.startDate != dateFilter.endDate) {
                        returnDate.push(dateFilter.endDate ? dateFilter.endDate : '...');
                    }
                }

                return returnDate.join(' - ');
            };

            return (
                <div className="row col-sm-12" style={{ display: "contents" }}>
                    <div className="col-lg-6 col-md-12">
                        <div className="form-group row">
                            <label className="col-lg-2 col-md-3 col-form-label">Clinic</label>
                            <div className="col-lg-10 col-md-9">
                                <Select
                                    className={`react-select`}
                                    options={clinics.map(item => ({ value: item.id, label: item.name }))}
                                    onChange={(selectedItem) => { this.handleSelectFilter(selectedItem, FILTER_CLINIC) }}
                                    placeholder={"- Please select -"}
                                    isMulti={false}
                                    isSearchable={true}
                                    isClearable={true}
                                    value={filter[FILTER_CLINIC] || ''} />
                            </div>
                        </div>

                        {showBillTo ?

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Ship To</label>
                                <div className="col-sm-10">
                                    <Select
                                        className={`react-select`}
                                        options={locationList.map(item => ({ value: item.id, label: item.name }))}
                                        onChange={(selectedItem) => { this.handleSelectFilter(selectedItem, FILTER_SHIP_TO) }}
                                        placeholder={anyPlaceholder}
                                        isMulti={true}
                                        isSearchable={true}
                                        value={filter[FILTER_SHIP_TO] || ''} />
                                </div>
                            </div>

                        : null}

                        { showShipTo ?

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Bill To</label>
                                <div className="col-sm-10">
                                    <Select
                                        className={`react-select`}
                                        options={billToLocations.map(item => ({ value: item.id, label: item.name }))}
                                        onChange={(selectedItem) => { this.handleSelectFilter(selectedItem, FILTER_BILL_TO) }}
                                        placeholder={anyPlaceholder}
                                        isMulti={true}
                                        isSearchable={true}
                                        value={filter[FILTER_BILL_TO] || ''} />
                                </div>
                            </div>

                        : null }

                        <div className="form-group row">
                            <label className="col-lg-2 col-md-3 col-form-label">Status</label>
                            <div className="col-lg-10 col-md-9">
                                <Select
                                    className={`react-select`}
                                    options={Object.keys(this.statusList).map((field) => ({ value: field, label: this.statusList[field] }))}
                                    onChange={(selectedItem) => { this.handleSelectFilter(selectedItem, FILTER_STATUS) }}
                                    placeholder={anyPlaceholder}
                                    isMulti={true}
                                    isSearchable={true}
                                    value={filter[FILTER_STATUS] || ''} />
                            </div>
                        </div>

                        { showVendor ?

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Vendor</label>
                                <div className="col-sm-10">
                                    <Select
                                        className={`react-select`}
                                        options={vendorList.map(vendor => ({ value: vendor.id, label: vendor.name }))}
                                        onChange={(selectedItem) => { this.handleSelectFilter(selectedItem, FILTER_VENDOR) }}
                                        placeholder={anyPlaceholder}
                                        isMulti={true}
                                        isSearchable={true}
                                        value={filter[FILTER_VENDOR] || ''} />
                                </div>
                            </div>

                        : null }

                    </div>
                    <div className="col-lg-6 col-md-12  edge-pr-0">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label">S2K Reference #</label>
                            <div className="col-sm-9 edge-pr-0">
                                <input
                                    className={`form-control form-control-sm`}
                                    onChange={(e) => { this.handleSelectFilter(e.target.value, FILTER_ORDER_S2K) }}
                                    placeholder={anyPlaceholder}
                                    value={filter[FILTER_ORDER_S2K] || ''} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label">Order #</label>
                            <div className="col-sm-9 edge-pr-0">
                                <input
                                    className={`form-control form-control-sm`}
                                    onChange={(e) => { this.handleSelectFilter(e.target.value, FILTER_ORDER_NUMBER) }}
                                    placeholder={anyPlaceholder}
                                    value={filter[FILTER_ORDER_NUMBER] || ''} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label">Purchase Order #</label>
                            <div className="col-sm-9 edge-pr-0">
                                <input
                                    className={`form-control form-control-sm`}
                                    onChange={(e) => { this.handleSelectFilter(e.target.value, FILTER_PURCHASE_ORDER_NUMBER) }}
                                    placeholder={anyPlaceholder}
                                    value={filter[FILTER_PURCHASE_ORDER_NUMBER] || ''} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label">Manufacturer #</label>
                            <div className="col-sm-9 edge-pr-0">
                                <input
                                    className={`form-control form-control-sm`}
                                    onChange={(e) => { this.handleSelectFilter(e.target.value, FILTER_MANUFACTURER_NUMBER) }}
                                    placeholder={anyPlaceholder}
                                    value={filter[FILTER_MANUFACTURER_NUMBER] || ''} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label">Vendor Product #</label>
                            <div className="col-sm-9 edge-pr-0">
                                <input
                                    className={`form-control form-control-sm`}
                                    onChange={(e) => { this.handleSelectFilter(e.target.value, FILTER_VENDOR_PRODUCT_NUMBER) }}
                                    placeholder={anyPlaceholder}
                                    value={filter[FILTER_VENDOR_PRODUCT_NUMBER] || ''} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label">Order Date</label>
                            <div className="col-sm-9 edge-pr-0">
                                <DateRangePickerComponent
                                    config={ { opens: 'left' } }
                                    onApplyCallback={(startDate, endDate) => {
                                        this.handleSelectFilter({ 'startDate': startDate ? startDate.format(DATE_FORMAT) : '', 'endDate': endDate ? endDate.format(DATE_FORMAT) : ''}, FILTER_ORDER_DATE)
                                    }}>
                                <input
                                    className={`form-control form-control-sm`}
                                    placeholder={anyPlaceholder}
                                    onChange={(e) => {  }}
                                    value={getDateFilterValue()} />
                                </DateRangePickerComponent>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label"></label>
                            <div className="col-sm-9 edge-pr-0">
                                <button className="btn btn-primary float-right" onClick={(e) => { this.reloadList() }}>Apply Filter</button>
                            </div>
                        </div>

                    </div>
                </div>
            )
        }

        const renderFilterPanel = () => {
            return (
                <div className="row mb-3">
                    <div className="col-md-12">

                        <div className="card card-filter">
                            <div className="card-header">
                                Filters
                            </div>
                            <div className="card-body">
                                <div className="row be-datatable-header">
                                    { renderFilters() }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div className="card card-table">
                <div className="card-header">
                    Orders
                </div>
                <div className="card-body">
                    <div className="dataTables_wrapper dt-bootstrap4 no-footer">

                        <ReactTable
                            manual
                            defaultPageSize={pageSize}
                            data={orders}
                            columns={columns}
                            pages={pages}
                            onFetchData={this.fetchData}
                            loading={loading}
                            total={total}
                            currentPage={currentPage}
                            pageSizeOptions={DEFAULT_PAGE_SIZE_OPTIONS}
                            defaultSorted={this.defaultSorted}
                        >
                            {(state, makeTable, instance) => {
                                return (
                                    <div>
                                        <div className="row be-datatable-header">
                                            <div className="col-sm-6">
                                                <div className="dataTables_length">
                                                    {/* show items */}
                                                    <TableShowComponent tableState={state} tableInstance={instance} />

                                                </div>
                                            </div>
                                            <div className="col-sm-6">
                                                <div className="dataTables_filter">
                                                    {/* search */}
                                                    {/* <TableSearchComponent tableState={state} tableInstance={instance} /> */}
                                                    <a className={`btn btn-secondary${showFilterPanel ? ' active' : ''}`}
                                                        href="#" onClick={(e) => {
                                                            e.preventDefault();
                                                            this.handleShowFilterPanel();
                                                        }}>
                                                        <i className="fas fa-filter"></i> Filters
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        {showFilterPanel ? renderFilterPanel() : ''}

                                        <div className="row be-datatable-body">
                                            <div className="col-sm-12">
                                                <div className="table-responsive">
                                                    <table className="table table-striped table-hover table-fw-widget dataTable no-footer dtr-inline">
                                                        <thead>
                                                            <tr role="row">
                                                                {state.columns.map((item, index) => {
                                                                    return (
                                                                        <th
                                                                            key={index}
                                                                            width={item.width}
                                                                            className={`${item.sortable ? setSortedClass(item) : ''}${item.id == 'total' ? ' right-align-column-header' : ''}`}
                                                                            onClick={(e) => { item.sortable ? this.onSort(e, item.id, state, instance, setSortedClass(item) == 'sorting_asc' ? 'desc' : 'asc') : null }}
                                                                        >
                                                                            {typeof item.Header === 'function' ? item.Header() : item.Header}
                                                                        </th>
                                                                    )
                                                                })}
                                                            </tr>
                                                        </thead>
                                                        {loading ?
                                                        <tbody className="g-font-size-default g-color-black">
                                                            {skeletonRows}
                                                        </tbody>
                                                        :
                                                            <tbody>
                                                            {state.pageRows.map((item, index) => {
                                                                return (
                                                                    <tr key={index} className={index % 2 == 0 ? 'odd' : 'even'} role="row">
                                                                        {
                                                                            showPrintPanel ?
                                                                                <td className="">
                                                                                    <input
                                                                                        type="checkbox"
                                                                                        className="checkbox"
                                                                                        checked={this.state.selected[item._original.id] === true}
                                                                                        onChange={() => this.toggleRow(item._original.id)}
                                                                                    />
                                                                                </td>
                                                                                : null
                                                                        }
                                                                        <td className="">
                                                                            {(state.currentPage - 1) * state.pageSize + index + 1} (#{item.id})
                                                                        </td>
                                                                        <td className="table-column-clinic-name">
                                                                            {item.clinic}
                                                                        </td>
                                                                        <td className="">
                                                                            {item.number}
                                                                        </td>
                                                                        <td className="">
                                                                            {item.created_at}
                                                                        </td>
                                                                        <td className="table-column-vendor-name">
                                                                            {colorizedVendorList(item._original).length ? colorizedVendorList(item._original) : '-'}
                                                                        </td>
                                                                        <td className="digit">
                                                                            {convertToPrice(item.total)}
                                                                        </td>

                                                                        <td className="">
                                                                            <div className="">
                                                                                <a className="btn btn-primary btn-sm"
                                                                                    href="#" onClick={(e) => {
                                                                                        e.preventDefault();
                                                                                        this.setState({ modalIsOpen: true, orderId: item._original.id })
                                                                                    }}>
                                                                                    View Order
                                                                                </a>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                );
                                                            })}
                                                            </tbody>
                                                        }
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row be-datatable-footer">
                                            <TablePaginationComponent tableState={state} tableInstance={instance} />
                                        </div>

                                    </div>
                                )
                            }}
                        </ReactTable>
                    </div>
                </div>

                <OrderFormContainer modalIsOpen={this.state.modalIsOpen}
                    orderId={this.state.orderId}
                    closeModal={() => {
                        this.setState({
                            modalIsOpen: false,
                            orderId: null
                        });
                    }}
                    afterSuccess={() => {
                        this.reloadList();
                    }}
                />
            </div>
        );
    }
}

OrderListComponent.propTypes = {
    vendors: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    currentPage: PropTypes.number,
    dispatch: PropTypes.func,
    history: PropTypes.object,
    loading: PropTypes.bool,
    location: PropTypes.object,
    match: PropTypes.object,
    pageSize: PropTypes.number,
    pages: PropTypes.number,
    sortby: PropTypes.array
}
