import React from 'react';
import ReactDOM from 'react-dom'
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import {
    ORDER_STATUS_OPEN,
    ORDER_STATUS_SUBMITTED,
    ORDER_STATUS_PARTIALLY_RECEIVED,
    ORDER_STATUS_CLOSED,
    ORDER_STATUS_CANCELED,

    colorizeStatus
} from '../../../../common/order-status'

import { convertToPrice } from '../../../../common/price'

import { fetchAuthUser } from '../../actions/auth';

import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE_SIZE_OPTIONS } from '../../../../common/grid'
import TableShowComponent from "../widgets/TableShowComponent";
import TablePaginationComponent from "../widgets/TablePaginationComponent";

import toastr from "../../common/toastr";

import PropTypes from "prop-types";
import swal from 'sweetalert';
import Skeleton from 'react-loading-skeleton';

import Cleave from 'cleave.js/react';
import { INPUT_MASK_QTY } from '../../../../common/input'

import Tooltip from "react-simple-tooltip"

import { getPrice } from '../../../../common/order-price'
import { trimText } from '../../../../common/string'

export default class OrderPriceListComponent extends React.Component {
    initialState() {
        return {
            pages: 1,
            currentPage: 1,
            pageSize: DEFAULT_PAGE_SIZE,
            sortby: [],

            orderPrice: null,

            loading: false,
        }
    }

    constructor(props) {
        super(props);

        this.onSort = this.onSort.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.handleDelete = this.handleDelete.bind(this);

        this.state = this.initialState();
        this.defaultSorted = [
            {
                id: 'number',
                desc: true,
            },
        ];
    }

    async componentDidMount() {
        const { author } = this.props;
        if (author) {
            await this.props.dispatch(fetchAuthUser({
                userId: author.id,
            }));
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.orderPrices !== this.props.orderPrices) {
            this.setState({
                currentPage: 1
            })
        }
    }

    successMessage(message) {
        toastr.success(message, 'Success');
    }

    errorMessage(message) {
        toastr.error(message, 'Error');
    }

    fetchData(state, instance) {
        this.state.currentPage = state.page + 1;
        this.state.pageSize = state.pageSize;
        this.state.sortby = state.sorted;
        this.state.search = state.search;
    }

    onSort(e, accessor, state, instance, sortMethod) {
        e.preventDefault();
        state.sorted = [{
            id: accessor,
            [sortMethod]: true,
        }];

        instance.state.sorted = state.sorted;
        this.fetchData(state, instance);
    }

    prepareFilter() {
        const { filter } = this.state;
        const formatedFilters = [];

        Object.keys(filter).map((filterName) => {
            const currentFilter = filter[filterName];
            let filterValue = filter[filterName];

            if (Array.isArray(currentFilter)) {
                filterValue = currentFilter.map((filterValue) => {
                    return filterValue.value;
                });
            }

            const emptyFilter = !filterValue || (Array.isArray(filterValue) && filterValue.length == 0);

            if (!emptyFilter) {
                formatedFilters.push({
                    'field': filterName, 'value': filterValue
                })
            }
        })

        return formatedFilters;
    }

    async handleDelete(id) {
        const willDelete = await swal({
            title: "Are you sure?",
            text: "Do you really want to remove this product from the order?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        if (!willDelete) {
            return;
        }

        this.props.onDelete(id);
    }

    handleInputChange(event) {
        const target = event.target;
        let value = null;

        switch (target.type) {
            case 'checkbox':
                value = target.checked;
                break;
            case 'file':
                value = target.files[0];
                break;
            default:
                value = target.value;
        }

        const name = target.name;

        this.setState({
            [name]: value,
        });
    }

    async changeQty(e, operation, priceOrderItem) {
        e.preventDefault();

        let { quantity, _original: { id } } = priceOrderItem;

        switch (operation) {
            case '+':
                quantity += 1;
                break;
            case '-':
                quantity -= 1;
                break;
            default:
                break;
        }
        quantity = quantity <= 0 ? 1 : quantity;

        this.props.onChangeQty ? this.props.onChangeQty(id, quantity) : '';
    }

    getPriceTooltip(orderPriceItem) {
        let { price_entity: { price_breakings }} = orderPriceItem;

        price_breakings.sort((a, b) => (a.quantity < b.quantity) ? 1 : ((b.quantity < a.quantity) ? -1 : 0));

        const priceValue = getPrice(orderPriceItem);
        const originalPrice = Number(orderPriceItem.price_entity.price);

        const pricesIsDifferent = Number(priceValue).toFixed(2) !== originalPrice.toFixed(2);

        const tooltipContent = (
            <div className="price-tooltip text-center">
                <span><b>Price Breaks:</b></span>
                {
                    price_breakings.map((price_break, i) => {
                        return (
                            <div key={i}>{price_break.quantity} @ {convertToPrice(price_break.price)}</div>
                        )
                    })
                }
            </div>
        )

        return (
            <div>
                <div>
                    <span className={pricesIsDifferent ? 'actual-price' : ''}>{convertToPrice(priceValue)}</span> <span className="old-price">{pricesIsDifferent ? convertToPrice(originalPrice) : ''}</span>
                </div>

                {price_breakings.length > 0 ?

                    <Tooltip
                        placement={"bottom"}
                        content={tooltipContent}
                        background={'#fff'}
                        color={'#000'}
                    >
                        <div>
                            <span className={'price-breaking-label'}>Price Breaking</span>
                        </div>
                    </Tooltip>

                : null}
            </div>
        )
    }

    handleNoteSubmitted(orderPriceItem, note) {
        this.props.onChangeNote(orderPriceItem.id, note);
    }

    getNoteTooltip(orderPriceItem) {
        let { note } = orderPriceItem;
        if (note === null) {
            note = '';
        }

        let trimmedNote = trimText(note);

        const tooltipContent = (
            <div className="text-center note-tooltip">{note}</div>
        )

        return (
            <div>
                { note ?
                    <div>
                    <Tooltip
                        placement={"bottom"}
                        content={tooltipContent}
                        background={'#fff'}
                        color={'#000'}
                    >
                        <div>
                            <span className="clickable">{trimmedNote}{ trimmedNote != note ? '...' : '' }</span>
                        </div>
                    </Tooltip>
                    <br />
                    </div>
                : null }
            </div>
        )
    }

    render() {
        const { orderPrices, showVendorColumn, errors } = this.props;
        const { pages, pageSize, sortby, loading, currentPage } = this.state;

        const orderPriceErrors = (orderPriceId) => {
            let collectedErrors = [];
            if (errors) {
                const { sub_orders } = errors;

                if (sub_orders) {
                    Object.keys(sub_orders).map((key) => {
                        const sub_order = sub_orders[key];
                        const { order_prices } = sub_order;

                        if (order_prices && order_prices[orderPriceId]) {
                            collectedErrors.push(order_prices[orderPriceId]);
                        }
                    })
                }
            }

            return (collectedErrors.length ? collectedErrors[0] : {});
        }

        const columns = [
            {
                Header: 'Product Name',
                id: 'name',
                accessor: (orderPrice) => {
                    const { price_entity } = orderPrice;

                    return price_entity && price_entity.product ? price_entity.product.name : ''
                },
                sortable: false,
                // width: '20%',
            },
        ];

        if (showVendorColumn) {
            columns.push({
                Header: 'Vendor',
                id: 'vendor',
                accessor: (orderPrice) => {
                    const { price_entity } = orderPrice;

                    return price_entity && price_entity.vendor ? price_entity.vendor.name : ''
                },
                sortable: false,
                width: '20%',
            })
        }

        columns.push({
            Header: 'Qty',
            id: 'quantity',
            accessor: (orderPrice) => {
                const quantity = Number(orderPrice.quantity);
                return quantity ? quantity : 0;
            },
            width: 190,
        })

        columns.push({
            Header: 'Price',
            id: 'price',
            accessor: (orderPrice) => {
                return orderPrice.price;
            },
            width: 190,
        })

        columns.push({
            Header: 'Row Total',
            id: 'row_total',
            accessor: (orderPrice) => {
                const priceValue = getPrice(orderPrice);

                return convertToPrice(Number(priceValue) * Number(orderPrice.quantity))
            },
            width: 190,
        })

        columns.push({
            Header: 'Note',
            id: 'note',
            accessor: (orderPrice) => {
                return orderPrice.note;
            },
        })

        const skeletonRows = [];
        for (let i = 0; i < 5; i++) {
            skeletonRows.push(
                <tr key={i}>
                    {
                        columns.map((column, index) => {
                            return (
                                <td key={index}><Skeleton height={33} /></td>
                            )
                        })
                    }
                </tr>
            )
        }

        const setSortedClass = (column) => {
            if (Array.isArray(sortby)) {
                let currentSort = sortby.find((sortItem) => {
                    return sortItem.id === column.id;
                });
                if (currentSort) {
                    return currentSort.desc ? 'sorting_desc' : 'sorting_asc';
                }
            }
            return 'sorting'
        }

        return (
            <div className="card card-table">
                <div className="card-header"></div>
                <div className="card-body">

                    <div className="dataTables_wrapper dt-bootstrap4 no-footer">

                        <ReactTable
                            pageSize={pageSize}
                            data={orderPrices}
                            columns={columns}
                            // pages={pages}
                            loading={loading}
                            total={orderPrices.length}
                            currentPage={currentPage}
                            pageSizeOptions={DEFAULT_PAGE_SIZE_OPTIONS.concat(orderPrices.length)}
                            page={currentPage - 1}
                            defaultSorted={this.defaultSorted}
                        >
                            {(state, makeTable, instance) => {
                                return (
                                    <div>

                                        <div className="row be-datatable-header">
                                            <div className="col-sm-6">
                                                <div className="dataTables_length">
                                                    {/* show items */}
                                                    <TableShowComponent tableState={state} tableInstance={instance} onChange={(pageSize) => {
                                                        this.setState({
                                                            pageSize: pageSize,
                                                            currentPage: 1
                                                        });
                                                    }} />
                                                </div>
                                            </div>
                                            <div className="col-sm-6"></div>
                                        </div>

                                        <div className="row be-datatable-body">
                                            <div className="col-sm-12">
                                                <div className="table-responsive">
                                                    <table className="table table-striped table-hover table-fw-widget dataTable no-footer dtr-inline">
                                                        <thead>
                                                            <tr>
                                                                {state.columns.map((item, index) => {
                                                                    return (
                                                                        <th
                                                                            key={index}
                                                                            width={item.width}
                                                                            className={`${item.sortable ? setSortedClass(item) : ''}`}
                                                                            onClick={(e) => { item.sortable ? this.onSort(e, item.id, state, instance, setSortedClass(item) == 'sorting_asc' ? 'desc' : 'asc') : null }}
                                                                        >
                                                                            {typeof item.Header === 'function' ? item.Header() : item.Header}
                                                                        </th>
                                                                    )
                                                                })}
                                                            </tr>
                                                        </thead>
                                                        {loading ?
                                                            <tbody className="g-font-size-default g-color-black">
                                                                {skeletonRows}
                                                            </tbody>

                                                            :
                                                            <tbody>
                                                            {state.pageRows.map((item, index) => {
                                                                const { price_entity } = item._original;

                                                                let rowErrors = orderPriceErrors(item._original.id);

                                                                return (
                                                                    <tr key={index} className={index % 2 == 0 ? 'odd' : 'even'} role="row">
                                                                        <td className="product-name-field table-column-product-name">
                                                                            <div className="name">
                                                                                {item.name}
                                                                            </div>
                                                                            <div className="additional-info">
                                                                                Vendor Product #: {price_entity.vendor_number}
                                                                            </div>
                                                                            <div className="additional-info">
                                                                                Manufacturer #: {price_entity.product.manufacturer_number}
                                                                            </div>
                                                                        </td>
                                                                        {showVendorColumn ?
                                                                            <td className="table-column-vendor-name">
                                                                                {item.vendor}
                                                                            </td>

                                                                            : null}
                                                                        <td className="">
                                                                            {item.quantity}
                                                                        </td>
                                                                        <td className="">
                                                                            {this.getPriceTooltip(item._original)}
                                                                        </td>
                                                                        <td className="">
                                                                            {item.row_total}
                                                                        </td>
                                                                        <td className="">
                                                                            {this.getNoteTooltip(item._original)}
                                                                        </td>
                                                                    </tr>
                                                                );
                                                            })}
                                                            </tbody>
                                                        }
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row be-datatable-footer">
                                            <TablePaginationComponent tableState={state} tableInstance={instance} onChange={(currentPage) => { this.setState({ currentPage: currentPage }) }} />
                                        </div>
                                    </div>
                                )
                            }}
                        </ReactTable>
                    </div>
                </div>
            </div>
        );
    }
}

OrderPriceListComponent.propTypes = {
    vendors: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    currentPage: PropTypes.number,
    dispatch: PropTypes.func,
    history: PropTypes.object,
    loading: PropTypes.bool,
    location: PropTypes.object,
    match: PropTypes.object,
    pageSize: PropTypes.number,
    pages: PropTypes.number,
    sortby: PropTypes.array
}
