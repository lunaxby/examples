import React from 'react';
import { Route } from 'react-router-dom';
import { Switch } from 'react-router';
import OrderListContainer from "../../containers/order/OrderListContainer";
import PropTypes from "prop-types";

export class OrderComponent extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Switch>
                <Route exact path={`${this.props.match.url}`} component={OrderListContainer} />
            </Switch>
        );
    }
}

export default OrderComponent;

OrderComponent.propTypes = {
    dispatch: PropTypes.func,
    match: PropTypes.object,
}