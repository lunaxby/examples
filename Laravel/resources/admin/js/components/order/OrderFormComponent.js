import React from 'react';

import orderActions from '../../actions/order';
const { fetchOrder } = orderActions;

import { fetchAuthUser } from '../../actions/auth';

import toastr from "../../common/toastr";
import Modal from "../../common/modal";
import PropTypes from "prop-types";

import Select from 'react-select'
import Skeleton from 'react-loading-skeleton';
import OrderPriceListContainer from "../../containers/order/OrderPriceListContainer";

import { convertToPrice } from '../../../../common/price'
import { getPrice } from '../../../../common/order-price'

import {
    ORDER_STATUS_OPEN,
    ORDER_STATUS_SUBMITTED,
    ORDER_STATUS_PARTIALLY_RECEIVED,
    ORDER_STATUS_CLOSED,
    ORDER_STATUS_CANCELED,

    colorizeStatus,
    statusList
} from '../../../../common/order-status'

import { convertToDateTime, convertToDate } from '../../../../common/date'

import {
    sortSuborders
} from '../../../../common/suborder'

export default class OrderFormComponent extends React.Component {
    initialState() {
        return {
            formTitle: '',
            submitBtnText: '',

            id: null,
            number: '',

            sub_orders: [],
            active_sub_order_id: null,
            s2k_number: null,

            totalProducts: 0,
            subtotal: 0,
            discount: 0,
            shipping: 0,
            misc: 0,
            total: 0,

            bill_to: null,
            ship_to: null,
            notes: null,

            loading: true,
            submiting: false,

            modalBuyIsOpen: false,
            buyProductId: '',

            isOrderModified: false,

            orderCanBeCanceled: false,

            subOrderCanBeCanceled: false,
            subOrderCanBeClosed: false,
            subOrderCanBeEmailed: false,
            subOrderCanBePrinted: false,
        };
    }

    constructor(props) {
        super(props);
        this.state = this.initialState();

        this.statusList = statusList;
    }

    componentWillMount() {
        Modal.setAppElement('body');
    }

    orderPriceList(filterBySuborderId = false) {
        const { sub_orders } = this.state;
        const allOrderPrices = [];

        const filteredSuborders = sub_orders.filter((sub_order) => { return filterBySuborderId ? (sub_order.id === filterBySuborderId) : true })

        filteredSuborders.map((sub_order) => {
            const { order_prices } = sub_order
            if (Array.isArray(order_prices)) {
                order_prices.map((order_price) => {
                    allOrderPrices.push(order_price);
                })
            }
        })

        return allOrderPrices;
    }

    async afterOpenModal() {
        await this.setState({loading: true});

        const { author } = this.props;

        if (author) {
            await this.props.dispatch(fetchAuthUser({
                userId: author.id,
            }));
        }

        if (this.props.orderId) {
            this.setState({
                formTitle: '',
                submitBtnText: 'Save Order',
            });
            try {
                await this.props.dispatch(fetchOrder({
                    id: this.props.orderId,
                }));

                const { order } = this.props;
                await this.setState(order);
                await this.setState({
                    sub_orders: await sortSuborders(this.state.sub_orders)
                })
                await this.calculate();

                if (order.number) {
                    await this.setState({
                        formTitle: `Order: #${order.number}`,
                    });
                }

                try {

                    const billTo = order.bill_to;
                    const shipTo = order.ship_to;

                    await this.setState({
                        bill_to: billTo ? { value: billTo.id, label: billTo.name } : null,
                        ship_to: shipTo ? { value: shipTo.id, label: shipTo.name } : null,

                        billToLocationObject: billTo,
                        shipToLocationObject: shipTo,
                    });

                    await this.setState({
                        isOrderModified: false,
                        orderCanBeCanceled: await this.orderCanBeCanceled()
                    })
                } catch (e) {
                    this.errorMessage(e.message ? e.message : 'Can\'t load Location List');
                }
            } catch (e) {
                this.errorMessage(e.message ? e.message : `Can't load Order`);
            }

        } else {
            await this.setState({
                formTitle: 'Create New Order',
                submitBtnText: 'Save Order',
            });
        }

        await this.setState({ loading: false });
    }

    successMessage(message) {
        toastr.success(message, 'Success');
    }

    errorMessage(message) {
        toastr.error(message, 'Error');
    }

    close() {
        this.setState(this.initialState());
        this.props.closeModal();
    }

    async syncOrderAvailableActions() {
        await this.setState({
            subOrderCanBeCanceled: await this.subOrderCanBeCanceled(),
            subOrderCanBeClosed: await this.subOrderCanBeClosed(),
            subOrderCanBeEmailed: await this.subOrderCanBeEmailed(),
            subOrderCanBePrinted: await this.subOrderCanBePrinted(),
        })

        await this.setState({
            orderCanBeCanceled: await this.orderCanBeCanceled(),
        })
    }

    async handleSetActiveSuborder(sub_order_id) {
        const { sub_orders } = this.state;

        await this.setState({
            active_sub_order_id: sub_order_id,
            activeSuborder: sub_orders.find((sub_order) => { return sub_order.id === sub_order_id })
        })

        await this.syncOrderAvailableActions();
    }

    async calculate() {
        const { sub_orders } = this.state;

        let totalProducts = 0;
        let subtotal = 0;

        // TODO after DHPI sycn
        let discount = 0;

        let shipping = 0;
        let misc = 0;
        let total = 0;

        sub_orders.map((sub_order, i) => {
            const { order_prices } = sub_order;
            let sub_order_total = 0;

            if (Array.isArray(order_prices)) {
                order_prices.map((order_price) => {
                    const order_price_value = getPrice(order_price);
                    let order_price_total = Number(order_price.quantity) * Number(order_price_value);

                    sub_order_total += order_price_total;
                    totalProducts += Number(order_price.quantity);
                })
            }

            sub_orders[i].total = sub_order_total;

            subtotal += sub_order_total;
            if (order_prices.length > 0) {
                shipping += Number(sub_order.shipping);
                misc += Number(sub_order.misc);
            }
        })

        total = subtotal - discount + misc + shipping;

        await this.setState({
            sub_orders: sub_orders,

            totalProducts,
            subtotal,
            discount,
            shipping,
            misc,
            total,
        })
    }

    async findSuborder(subOrderId) {
        const  { sub_orders } = this.state;
        return sub_orders.find((sub_order) => { return sub_order.id === subOrderId})
    }

    async orderCanBeCanceled() {
        const { sub_orders } = this.state;
        let orderCanBeCanceled = false;

        orderCanBeCanceled = await Promise.all(sub_orders.map(async (subOrder) => {
            const subOrderCanBeCanceled = await this.subOrderCanBeCanceled(subOrder.id);
            if (subOrder.order_prices.length > 0 && subOrderCanBeCanceled) {
                return true;
            }

            return false;
        }))

        return Math.max(...orderCanBeCanceled);
    }

    async subOrderCanBeCanceled(subOrderId) {
        const { activeSuborder, id } = this.state;
        let status = null;
        if (activeSuborder) {
            status = activeSuborder.status;
        }
        if (subOrderId) {
            const subOrder = await this.findSuborder(subOrderId);
            if (subOrder) {
                status = subOrder.status;
            }
        }

        return id && (status === ORDER_STATUS_OPEN || status === ORDER_STATUS_SUBMITTED);
    }

    async subOrderCanBeClosed() {
        const { activeSuborder, id } = this.state;
        return id && activeSuborder && activeSuborder.status === ORDER_STATUS_SUBMITTED;
    }

    async subOrderCanBeEmailed() {
        const { activeSuborder, id } = this.state;
        return id && activeSuborder && activeSuborder.status === ORDER_STATUS_SUBMITTED;
    }

    async subOrderCanBePrinted() {
        const { activeSuborder, id } = this.state;
        return id && activeSuborder && activeSuborder.status === ORDER_STATUS_SUBMITTED;
    }

    render() {
        const {
            id,
            author,
            created_at,
            notes,
            bill_to,
            billToLocationObject,

            ship_to,
            shipToLocationObject,

            status,
            sub_orders,

            totalProducts,
            subtotal,
            discount,
            shipping,
            misc,
            total,

            active_sub_order_id,
            activeSuborder,
            orderCanBeCanceled,

            formTitle, submitBtnText, loading, submiting,
            clinic
        } = this.state;
        const { errors, locationList } = this.props;

        const orderPrices = this.orderPriceList(active_sub_order_id);
        const createMode = !id;

        const renderOrderTotal = (cssClass) => {
            return (
                <div className={`card${cssClass ? ' ' + cssClass : ''}`}>
                    <div className="card-header">
                        <span className="text-uppercase">Order</span>
                    </div>
                    <div className="card-body">
                        <div className="row mb-1">
                            <div className="col-sm-6">Total Products</div>
                            <div className="col-sm-6 text-right">{totalProducts}</div>
                        </div>
                        <div className="row mb-1">
                            <div className="col-sm-6">Subtotal</div>
                            <div className="col-sm-6 text-right">{convertToPrice(subtotal)}</div>
                        </div>
                        {discount ?
                            <div className="row mb-1">
                                <div className="col-sm-6">Discount</div>
                                <div className="col-sm-6 text-right">{convertToPrice(discount)}</div>
                            </div> :
                            <div></div>
                        }
                        {shipping ?
                            <div className="row mb-1">
                                <div className="col-sm-6">Shipping/Handling</div>
                                <div className="col-sm-6 text-right">{convertToPrice(shipping)}</div>
                            </div> :
                            <div></div>
                        }
                        {misc ?
                            <div className="row mb-1">
                                <div className="col-sm-6">Misc</div>
                                <div className="col-sm-6 text-right">{convertToPrice(misc)}</div>
                            </div> :
                            <div></div>
                        }
                        <div className="row">
                            <div className="col-sm-6"><h4>Order Total</h4></div>
                            <div className="col-sm-6 text-right"><h4>{convertToPrice(total)}</h4></div>
                        </div>
                    </div>
                </div>
            )
        }

        const renderOrderInformation = () => {
            const locationPlaceholder = '';
            const locationOptions = locationList.map(location => ({ value: location.id, label: location.name }));

            const getLocationAddress = (location) => {
                if (!location) {
                    return null;
                }

                const addresses = [];
                for (let i = 1; i <= 3; i++) {
                    if (location["address_" + i]) {
                        addresses.push(location["address_" + i]);
                    }
                }

                return {
                    'address': addresses.join(', '),
                    'city': location.city,
                    'state': location.state,
                    'zip': location.zip,
                };
            }

            const shipToAddress = getLocationAddress(shipToLocationObject);
            const billToAddress = getLocationAddress(billToLocationObject);

            const renderLocationSelector = (locationVariableName) => {
                const address = locationVariableName == 'bill_to' ? billToAddress : shipToAddress;

                return (
                    <div className="form-group row pt-0 required">
                        <div className="col-sm-12">
                            <Select
                                className={`react-select${errors[locationVariableName] ? ' is-invalid' : ''}`}
                                options={locationOptions}
                                onChange={(selectedItem) => { this.handleInputChange({ target: { name: locationVariableName, type: 'select', value: selectedItem } }) }}
                                placeholder={locationPlaceholder}
                                required
                                isDisabled={true}
                                isSearchable={true}
                                value={this.state[locationVariableName]}
                            />
                            <div className="invalid-feedback">{errors[locationVariableName] && errors[locationVariableName][0]}</div>
                            {address ?
                                <div className="location-address">
                                    {address.address ? <span>{address.address} <br /></span> : null}
                                    {address.city || address.state ? <span>{address.city} {address.state}<br /></span> : null}
                                    {address.zip ? <span>{address.zip} <br /></span> : null}
                                </div>
                                : ''}
                        </div>
                    </div>
                )
            }

            return (
                <div className="card mb-0">
                    <div className="card-header">
                        <span className="text-uppercase">Information</span>
                    </div>
                    <div className="card-body">
                        <div className="row mb-1">
                            <div className="col-sm-3">Clinic</div>
                            <div className="col-sm-9">{ clinic ? clinic.name : '' }</div>
                        </div>
                        <div className="row mb-1">
                            <div className="col-sm-3">Created by</div>
                            <div className="col-sm-9">{ author ? author.name : '' }</div>
                        </div>
                        <div className="row mb-1">
                            <div className="col-sm-3">Created On</div>
                            <div className="col-sm-9">{ created_at ? convertToDateTime(created_at) : '' }</div>
                        </div>
                        <div className="row mb-1">
                            <div className="col-sm-3">Bill to</div>
                            <div className="col-sm-9">

                                { renderLocationSelector('bill_to') }

                            </div>
                        </div>
                        <div className="row mb-1">
                            <div className="col-sm-3">Ship to</div>
                            <div className="col-sm-9">

                                {renderLocationSelector('ship_to')}

                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-3">Internal Notes</div>
                            <div className="col-sm-9">

                                <div className="form-group row">
                                    <div className="col-sm-12">
                                        <textarea rows={8}
                                            id="textarea-order-note"
                                            className={`form-control form-control-sm rounded${errors.notes ? ' is-invalid' : ''}`}
                                            type="text"
                                            name="notes"
                                            disabled={true}

                                            onChange={(e) => { this.handleInputChange(e) }}
                                            value={notes || ''}
                                        />
                                        <div className="invalid-feedback">{errors.notes && errors.notes[0]}</div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            );
        }

        const renderSuborderFields = () => {
            const { activeSuborder, s2k_number } = this.state;
            if (!activeSuborder) {
                return null;
            }

            let { misc, shipping, total, received_at, receive_all, notes, purchase_number, vendor } = activeSuborder
            const isDhpiVendor = vendor.dhpi;

            total = Number(total) ? Number(total) : 0;
            misc = Number(misc) ? Number(misc) : 0;
            shipping = Number(shipping) ? Number(shipping) : 0;

            const receiptTotal = convertToPrice(total + misc + shipping);

            return (
                <div className="row text-left mt-2">
                    <div className="col-lg-4 col-md-12">
                        <div className="row mb-3">
                            <div className="col-sm-12">
                                <h4>Receipt Total {receiptTotal}</h4>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">

                                <div className="form-group row mb-2">
                                    <label className="col-sm-4 col-form-label">Shipping/Handling</label>
                                    <div className="col-sm-8">
                                        <input
                                            className={`form-control form-control-sm rounded${errors.shipping ? ' is-invalid' : ''}`}
                                            type="text"
                                            name="shipping"
                                            disabled={true}
                                            onChange={(e) => {}}
                                            value={convertToPrice(shipping ? shipping : 0, '')}
                                        />
                                        <div className="invalid-feedback">{errors.shipping && errors.shipping[0]}</div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">

                                <div className="form-group row mb-2">
                                    <label className="col-sm-4 col-form-label">Misc</label>
                                    <div className="col-sm-8">
                                        <input
                                            disabled={true}
                                            className={`form-control form-control-sm rounded${errors.misc ? ' is-invalid' : ''}`}
                                            type="text"
                                            name="misc"
                                            onChange={(e) => {}}
                                            value={convertToPrice(misc ? misc : 0, '')}
                                        />
                                        <div className="invalid-feedback">{errors.misc && errors.misc[0]}</div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-12">
                        <div className="row">
                            <div className="col-sm-12">

                                <div className="form-group row mb-2">
                                    <label className="col-lg-3 col-md-4 col-form-label">Receive Date</label>
                                    <div className="col-lg-9 col-md-8">

                                            <input
                                                disabled={true}
                                                className={`form-control form-control-sm rounded${errors.received_at ? ' is-invalid' : ''}`}
                                                onChange={(e) => { }}
                                                value={received_at ? convertToDate(received_at) : ''} />

                                        <div className="invalid-feedback">{errors.received_at && errors.received_at[0]}</div>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-12">

                        <div className="row">
                            <div className="col-sm-12">

                                <div className="form-group row mb-2">
                                    <label className="col-sm-4 col-form-label">Notes to Vendor</label>
                                    <div className="col-sm-8">

                                        <textarea rows={2}
                                            disabled={true}
                                            className={`form-control form-control-sm rounded${errors.notes ? ' is-invalid' : ''}`}
                                            type="text"
                                            name="notes"
                                            onChange={(e) => {  }}
                                            value={notes || ''}
                                        />

                                        <div className="invalid-feedback">{errors.notes && errors.notes[0]}</div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">

                                <div className="form-group row mb-2">
                                    <label className="col-sm-4 col-form-label">PO #</label>
                                    <div className="col-sm-8">

                                        <input
                                            disabled={true}
                                            className={`form-control form-control-sm rounded${errors.purchase_number ? ' is-invalid' : ''}`}
                                            type="text"
                                            name="purchase_number"
                                            onChange={(e) => { }}
                                            value={purchase_number || ''}
                                        />

                                        <div className="invalid-feedback">{errors.purchase_number && errors.purchase_number[0]}</div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        {s2k_number && isDhpiVendor ?
                        <div className="row">
                            <div className="col-sm-12">

                                <div className="form-group row mb-2">
                                    <div className="col-sm-4">S2K Reference #</div>
                                    <div className="col-sm-8">
                                        {s2k_number ? s2k_number : '--/--'}
                                    </div>
                                </div>
                            </div>
                        </div> : null
                        }

                    </div>
                </div>
            )
        }

        return (
            <div>
                <Modal
                    isOpen={this.props.modalIsOpen}
                    onAfterOpen={(e) => { this.afterOpenModal() }}
                    onRequestClose={() => { this.close() }}
                    style={Modal.customStyles}
                >
                    <div className="order-form">
                        <div className="card-header card-header-divider">
                            {loading && !formTitle && <Skeleton height={25} width={250} />}
                            {formTitle}
                            <div className="tools">
                                <button type="button" className="close" id="cross-close-btn" data-dismiss="modal" aria-label="Close"
                                    onClick={() => { this.close() }}>
                                    <span className="mdi mdi-close" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>

                        <div className="modal-body">
                            {/* begin content */}

                            <div className="row">
                                <div className="col-lg-6 col-md-12">
                                     {loading ? <Skeleton height={250} width={"100%"} /> : renderOrderTotal()}
                                </div>
                                <div className="col-lg-6 col-md-12">
                                    {loading ? <Skeleton height={250} width={"100%"} /> : renderOrderInformation()}
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-sm-12">
                                    {loading ? <Skeleton height={33} width={"100%"} /> : <div className="card-header card-header-divider ml-0 mr-0">Order Items</div>}
                                </div>
                            </div>

                            {loading ? <Skeleton height={150} width={"100%"} /> :
                                <div className="row sub-order-panel">
                                    <div className="col-sm-12">
                                        {/* begin price list */}

                                        <ul className="nav nav-tabs nav-tabs-classic">
                                            <li className="nav-item text-center">
                                                <a className={`nav-link${!active_sub_order_id ? ' active' : ''}`} onClick={() => { this.handleSetActiveSuborder(null) }} href="#">
                                                    <div className="all-vendors-tab">
                                                        <span>All Vendors</span>
                                                    </div>
                                                </a>
                                            </li>
                                            {
                                                sub_orders.map((sub_order, i) => {
                                                    const { status, order_prices } = sub_order;
                                                    const statusLabel = this.statusList[status] ? this.statusList[status] : status;

                                                    return (
                                                        Array.isArray(order_prices) && order_prices.length > 0 ?
                                                        <li className="nav-item text-center" key={i}>
                                                            <a className={`nav-link${sub_order.id === active_sub_order_id ? ' active' : ''}`} onClick={() => { this.handleSetActiveSuborder(sub_order.id) }} href="#">
                                                                <div className="vendor-tab">
                                                                    <span>{sub_order.vendor.name.length < 30 ? sub_order.vendor.name : sub_order.vendor.name.slice(0,30)+"..."}</span>
                                                                    <br/>
                                                                    <span className={`order-status ${colorizeStatus(status)}`}>{statusLabel}</span>
                                                                </div>
                                                            </a>
                                                        </li>
                                                        : null
                                                    )
                                                })
                                            }
                                        </ul>

                                        <div className="card tab-content mb-0">
                                            <div className="card-body">
                                                <div className="row mb-2">
                                                    <div className="col-sm-12 text-right pr-0">
                                                        {/* { renderSuborderActions() } */}

                                                        { renderSuborderFields() }
                                                    </div>
                                                </div>
                                                <div className="row mb-1">
                                                    <div className="col-sm-12">
                                                        {/* begin order price table */}
                                                        <OrderPriceListContainer
                                                            showVendorColumn={ active_sub_order_id === null }
                                                            orderPrices={orderPrices}
                                                            onChangeQty={(orderPriceId, qty) => {  }}
                                                            onDelete={(orderPriceId) => {  }}
                                                            onChangeNote={(orderPriceId, note) => { }}
                                                        />
                                                        {/* end order price table */}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        {/* end price list */}
                                    </div>
                                </div>
                            }

                            {/* end content */}
                        </div>
                        <div className="modal-footer">
                            {loading ? <Skeleton height={50} width={70} /> :
                                <button onClick={() => { this.close() }}
                                    id="close-btn"
                                    className="btn btn-light" data-dismiss="modal">Close</button>
                            }
                        </div>
                    </div>
                </Modal>

            </div>
        );
    }
}

OrderFormComponent.propTypes = {
    afterSuccess: PropTypes.func,
    productId: PropTypes.number,
    closeModal: PropTypes.func,
    dispatch: PropTypes.func,
    errors: PropTypes.object,
    modalIsOpen: PropTypes.bool
}
