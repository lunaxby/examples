import React from 'react';
import { NavLink } from 'react-router-dom';
import { fetchAuthUser } from '../actions/auth' 

export default class SidebarNavComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { user } = this.props;

        if (user) {
            this.props.dispatch(fetchAuthUser({
                userId: user.id,
            }));
        }
    }

    render() {
        return (
            <div className="be-left-sidebar">
                <div className="left-sidebar-wrapper"><a className="left-sidebar-toggle" href="#">DHPI</a>
                    <div className="left-sidebar-spacer">
                        <div className="left-sidebar-scroll">
                            <div className="left-sidebar-content">

                                <ul className="sidebar-elements">
                                    <li className="parent open"><a href="#"><i className="icon mdi mdi-home"></i><span>Admin Panel</span></a>
                                        <ul className="sub-menu">
                                            <li>
                                                <NavLink className="nav-link"
                                                    activeClassName="active"
                                                    to={`/admin/orders`}
                                                    exact>Orders
                                    </NavLink>
                                            </li>

                                            <li>
                                                <NavLink className="nav-link"
                                                    activeClassName="active"
                                                    to={`/admin/vendors`}
                                                    exact>Vendors
                                    </NavLink>
                                            </li>

                                            <li>
                                                <NavLink className="nav-link"
                                                    activeClassName="active"
                                                    to={`/admin/users`}
                                                    exact>Manage Users
                                    </NavLink>
                                            </li>

                                            <li>
                                                <NavLink className="nav-link"
                                                    activeClassName="active"
                                                    to={`/admin/clinics`}
                                                    exact>Clinics
                                    </NavLink>
                                            </li>

                                            <li>
                                                <NavLink className="nav-link"
                                                    activeClassName="active"
                                                    to={`/admin/settings`}
                                                    exact>Settings
                                    </NavLink>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}