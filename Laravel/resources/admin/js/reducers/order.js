import {combineReducers} from 'redux';
import orderActions from '../actions/order';

const {
    REQUEST_ORDERS,
    RECEIVE_ORDERS,
    RECEIVE_ORDER,
} = orderActions;

const initState = {
    orders: [],
    order: {}
};

const store = (state = initState, action) => {
    switch (action.type) {
        case REQUEST_ORDERS:
            return {
                ...state,
                loading: action.loading,
            };
        case RECEIVE_ORDERS:
            return {
                ...state,
                loading: action.loading,
                orders: action.data.data,

                total: action.data.meta.total,
                pages: action.data.meta.last_page
            };
        case RECEIVE_ORDER:
            return {
                ...state,
                order: action.data,
                response: {},
            };
        default:
            return state;
    }
};

const orderReducer = combineReducers({
    store,
});

export default orderReducer;