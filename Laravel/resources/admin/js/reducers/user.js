import {combineReducers} from 'redux';
import userActions from '../actions/user';
import {RESET_ERROR} from "../../../common/actions/user";

const {
    REQUEST_USERS,
    RECEIVE_USERS,
    RECEIVE_POWER_USERS,
    RECEIVE_USER,
    SAVE_USER_RESPONSE,
    CREATE_USER,
} = userActions;

const initState = {
    users: [],
    user: {}
};

const store = (state = initState, action) => {
    switch (action.type) {
        case CREATE_USER:
            return {
                ...state,
                user: action.data,
                response: {}
            };
        case REQUEST_USERS:
            return {
                ...state,
                loading: action.loading,
            };
        case RECEIVE_USERS:
            return {
                ...state,
                loading: action.loading,
                users: action.data.data,

                total: action.data.meta.total,
                currentPage: action.data.meta.current_page,
                pageSize: action.data.meta.per_page,
                sortby: action.data.meta.sortby,
                pages: action.data.meta.last_page
            };
        case RECEIVE_POWER_USERS:
            return {
                ...state,
                loading: action.loading,
                powerUsers: action.data.data,

                // total: action.data.meta.total,
                // currentPage: action.data.meta.current_page,
                // pageSize: action.data.meta.per_page,
                // sortby: action.data.meta.sortby,
                // pages: action.data.meta.last_page
            };
        case RECEIVE_USER:
            return {
                ...state,
                user: action.data,
                response: {},
            };
        case SAVE_USER_RESPONSE:
            return {
                ...state,
                response: action.data,
            };
        case RESET_ERROR:
            return {
                ...state,
                errors:action.data
            }
        default:
            return state;
    }
};

const userReducer = combineReducers({
    store,
});

export default userReducer;