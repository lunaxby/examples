import { connect } from 'react-redux';
import LoginComponent from '../components/LoginComponent';

const mapStateToProps = (state) => {
    return {
        state,
        login: state.authReducer.auth.login,
        errors: state.authReducer.auth.errors || {},
        message: state.authReducer.auth.message,

        authenticated: state.authReducer.auth.authenticated,
        user: state.authReducer.auth.user,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    };
};

const LoginContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginComponent);

export default LoginContainer;