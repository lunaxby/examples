import { connect } from 'react-redux';
import SidebarNavComponent from '../components/SidebarNavComponent';

const mapStateToProps = (state) => {
    return {
        state,
        user: state.authReducer.auth.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    };
};

const SidebarNavContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SidebarNavComponent);

export default SidebarNavContainer;