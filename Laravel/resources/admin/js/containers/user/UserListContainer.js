import {connect} from 'react-redux';
import UserListComponent from '../../components/user/UserListComponent';
import { DEFAULT_PAGE_SIZE } from '../../../../common/grid'

const mapStateToProps = (state) => {
    return {
        state,
        authUser: state.authReducer.auth.user,
        
        users: state.userReducer.store.users || [],

        total: state.userReducer.store.total || 0,
        pages: state.userReducer.store.pages || 1,
        currentPage: state.userReducer.store.currentPage || 1,
        pageSize: state.userReducer.store.pageSize || DEFAULT_PAGE_SIZE,
        sortby: state.userReducer.store.sortby || [],
        loading: state.userReducer.store.loading || false,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    };
};

const UserListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(UserListComponent);

export default UserListContainer;