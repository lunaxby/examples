import { connect } from 'react-redux';
import UserComponent from '../../components/user/UserComponent';

const mapStateToProps = (state) => {
    return {
        state,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    };
};

const UserContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(UserComponent);

export default UserContainer;