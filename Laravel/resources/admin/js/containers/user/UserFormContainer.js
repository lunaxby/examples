import {connect} from 'react-redux';
import UserFormComponent from "../../components/user/UserFormComponent";

const mapStateToProps = (state) => {
    const { user, response } = state.userReducer.store;
    const { clinic } = state.clinicReducer.store;

    return {
        state,
        user: user ? user : {},
        errors: response && response.errors ? response.errors : {},

        clinics: state.clinicReducer.store.clinics || [],

        selectedClinic: clinic || {},
        clinicLocations: clinic && clinic.locations ? clinic.locations.map(location => ({ value: location.id, label: location.name })) : []
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    };
};

const UserFormContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(UserFormComponent);

export default UserFormContainer;