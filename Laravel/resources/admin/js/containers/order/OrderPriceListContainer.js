import {connect} from 'react-redux';
import OrderPriceListComponent from '../../components/order/OrderPriceListComponent';

const mapStateToProps = (state) => { 
    const { response } = state.orderReducer.store;
       
    return {
        state,
        errors: response && response.errors ? response.errors : {},
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    };
};

const OrderPriceListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(OrderPriceListComponent);

export default OrderPriceListContainer;