import {connect} from 'react-redux';
import OrderFormComponent from "../../components/order/OrderFormComponent";

const mapStateToProps = (state) => {
    const { order, response } = state.orderReducer.store;
    const author = state.authReducer.auth.user;
    
    return {
        state,
        order: order ? order : {},
        errors: response && response.errors ? response.errors : {},

        products: [],
        selectedPrice: {},
        
        locationList: [],
        
        author: author,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    };
};

const OrderFormContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(OrderFormComponent);

export default OrderFormContainer;