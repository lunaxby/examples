import {connect} from 'react-redux';
import OrderListComponent from '../../components/order/OrderListComponent';

const mapStateToProps = (state) => {
    const author = state.authReducer.auth.user;
    const { clinics, clinic } = state.clinicReducer.store;
    const { vendors, vendor } = state.vendorReducer.store;

    const dhpiVendor = vendors.find((vendor) => { return vendor.dhpi == 1 });
    let vendorList = [];
    if (clinic && clinic.vendors) {
        vendorList = vendorList.concat((dhpiVendor ? [dhpiVendor] : []), clinic.vendors);
    } else {
        vendorList = vendors ? vendors : []
    }

    return {
        state,
        orders: state.orderReducer.store.orders || [],

        vendorList: vendorList,
        vendor: vendor ? vendor : {},

        clinics: clinics ? clinics : [],
        clinic: clinic.name ? clinic : {},

        locationList: clinic && clinic.locations ? clinic.locations : [],
        billToLocations: clinic && clinic.locations ? clinic.locations.filter(location => { return location.bill_to == 1 }) : [],

        showBillTo: clinic.name ? true : false,
        showShipTo: clinic.name ? true : false,
        showVendor: clinic.name ? true : false,

        total: state.orderReducer.store.total || 0,
        pages: state.orderReducer.store.pages || 1,

        loading: state.orderReducer.store.loading || false,

        author: author,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    };
};

const OrderListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(OrderListComponent);

export default OrderListContainer;
