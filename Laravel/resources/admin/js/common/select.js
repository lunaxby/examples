import Select from 'react-select';

const SELECT_PLACEHOLDER_TEXT_COLOR = '#B6B9BB';
Select.styles = {
    placeholder: (defaultStyles) => {
        return {
            ...defaultStyles,
            color: SELECT_PLACEHOLDER_TEXT_COLOR,
        }
    }
}

export default Select;
