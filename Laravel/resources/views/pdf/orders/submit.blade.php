<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Order {{ $order->number }}</title>

    <style>
        body {
            font-size: 12px;
        }

        .order_prices {
            border-spacing: 0;
            width: 100%;
            margin-top: 40px;
        }

        .order_prices thead tr th {
            border-bottom: 1px solid #333;
            border-top: 1px solid #333;
        }

        .order_prices tbody tr td {
            border-bottom: 1px solid #333;
        }

        .order_prices tfoot tr td {
            border-bottom: 1px solid #333;
            text-align: right;
        }

        .text-align-center {
            text-align: center;
        }

        .address-cut-wrapper {
            max-width: 300px;
            overflow-wrap: break-word;
        }
        .product-name-cut-wrapper {
            max-width: 100px;
            overflow-wrap: break-word;
        }
    </style>
</head>
<body>
    <table style="width: 100%;">
        <tr>
            <td style="border-bottom: 1px solid #333; border-right: 1px solid #333; width: 33%;">
                <h1>Purchase Order</h1>
            </td>
            <td style="width: 33%;">
                <div class="text-align-center">Order Date: {{ ($order['created_at'])->format('m-d-Y') }}</div>
                <div class="text-align-center">Customer Number: {{ $clinic['customer_number'] }}</div>
            </td>
            <td style="width: 33%;">
                <div>Order Number: {{ $order['number'] }}</div>
                @if ($subOrder && $subOrder['purchase_number'])
                    <div>P.O. Number: {{ $subOrder['purchase_number'] }}</div>
                @endif
            </td>
        </tr>
    </table>

    <table style="width: 70%; border-bottom: 1px solid #333; border-right: 1px solid #333;">
        <tr>
            <td class="address-cut-wrapper">
                <h2>{{ $order['billTo']['name'] }}</h2>
            </td>
        </tr>
    </table>

    <table style="width: 100%;">
        <tr>
            <td style="vertical-align: top;" class="address-cut-wrapper">
                @if ($order['billTo']['address_1'])
                    <div>{{ $order['billTo']['address_1'] }}</div>
                @endif

                @if ($order['billTo']['address_2'])
                    <div>{{ $order['billTo']['address_2'] }}</div>
                @endif

                @if ($order['billTo']['address_3'])
                    <div>{{ $order['billTo']['address_3'] }}</div>
                @endif

                <div>{{ $order['billTo']['city'] }}, {{ $order['billTo']['state'] }} {{ $order['billTo']['zip'] }}</div>

                @if ($order['billTo']['phone'])
                    <div>Phone Number: {{ $order['billTo']['phone'] }}</div>
                @endif

                @if ($order['billTo']['fax'])
                    <div>Fax Number: {{ $order['billTo']['fax'] }}</div>
                @endif

                <div style="margin-top: 15px"><b><u>Vendor</u></b></div>

                @if ($subOrder['vendor']['name'])
                    <div>{{ $subOrder['vendor']['name'] }}</div>
                @endif

                @if ($subOrder['vendor']['shipping_address_1'])
                    <div>{{ $subOrder['vendor']['shipping_address_1'] }}</div>
                @endif

                @if ($subOrder['vendor']['shipping_address_2'])
                    <div>{{ $subOrder['vendor']['shipping_address_2'] }}</div>
                @endif

                @if ($subOrder['vendor']['shipping_address_3'])
                    <div>{{ $subOrder['vendor']['shipping_address_3'] }}</div>
                @endif

                <div>{{ $subOrder['vendor']['shipping_city'] }}, {{ $subOrder['vendor']['shipping_state'] }}
                    {{ $subOrder['vendor']['shipping_zip'] }}</div>

                @if ($subOrder['vendor']['phone'])
                    <div>Phone Number: {{ $subOrder['vendor']['phone'] }}</div>
                @endif

                @if ($subOrder['vendor']['fax'])
                    <div>Fax Number: {{ $subOrder['vendor']['fax'] }}</div>
                @endif

                @if ($subOrder['vendor']['contact_name'])
                    <div>Contact Name: {{ $subOrder['vendor']['contact_name'] }}</div>
                @endif

                <div><b>Order Number: {{ $order['number'] }}</b></div>
                <div><b>{{ $order['billTo']['name'] }}</b></div>
            </td>
            <td style="vertical-align: top;" class="address-cut-wrapper">
                <div><b>ShipTo</b></div>

                @if ($order['shipTo']['name'])
                    <div>{{ $order['shipTo']['name'] }}</div>
                @endif

                @if ($order['shipTo']['address_1'])
                    <div>{{ $order['shipTo']['address_1'] }}</div>
                @endif

                @if ($order['shipTo']['address_2'])
                    <div>{{ $order['shipTo']['address_2'] }}</div>
                @endif

                @if ($order['shipTo']['address_3'])
                    <div>{{ $order['shipTo']['address_3'] }}</div>
                @endif

                <div style="margin-top: 15px">{{ $order['shipTo']['city'] }}, {{ $order['shipTo']['state'] }} {{ $order['shipTo']['zip'] }}</div>

                @if ($subOrder['notes'])
                    <b>Message:</b>

                    <div>{{ $subOrder['notes'] }}</div>
                @endif
            </td>
        </tr>
    </table>

    <table class="order_prices">
        <thead>
        <tr>
            <th>Item</th>
            <th>Description <br> Notes</th>
            <th>Unit</th>
            <th style="text-align:center;">Quantity</th>
            <th style="text-align:right;">Unit Price</th>
            <th style="text-align:right;">Extended Price</th>
        </tr>
        </thead>
        <tbody>
        @foreach($subOrder['orderPrices'] as $orderPrice)
        <tr>
            <td>{{ $orderPrice['priceEntity']['vendor_number'] }}</td>
            <td class="product-name-cut-wrapper">{{ $orderPrice['priceEntity']['product']['name'] }}</td>
            <td>{{ $orderPrice['priceEntity']['product']['package_unit'] }}</td>
            <td style="text-align: center;">{{ $orderPrice['quantity'] }}</td>
            <td style="text-align: right;">${{ number_format($orderPrice['price'], 2) }}</td>
            <td style="text-align: right;">${{ number_format($orderPrice['total'], 2) }}</td>
        </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="5"><b>Grand Total:</b></td>
            <td><b>${{ number_format($subOrder['total'], 2) }}</b></td>
        </tr>
        </tfoot>
    </table>
</body>
</html>
