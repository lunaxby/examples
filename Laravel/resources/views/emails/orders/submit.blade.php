<p>Hello <i>{{ $subOrder->vendor->name }}</i>,</p>
<br/>
<p>A new order from <i>{{ $subOrder->vendor->clinic ? $subOrder->vendor->clinic->name : $subOrder->vendor->name }}</i> submitted to your through DHPI Options Cloud Service.
Please see the attached PDF-file.</p>
<br/>
<br/>
<br/>
<p>---</p>
<br/>
<p>Dental Health Products, Inc (DHPI)</p>
