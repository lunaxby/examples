<h3>Hello {{ $user->name }},</h3>

<p>Hereby we inform you that {{ $author->name }} from {{ $clinic_name }} has updated an account for you on Options
Cloud DHPI website.</p>

<p>You can log in {{ env('APP_URL') }} with the following data:</p>
<ol>
    <li>Your email {{ $user->email }}</li>
    <li>Your password {{ $password }}</li>
</ol>

<p>We strongly recommend you to change this password after the first login for security reasons.</p>

<p>--</p>

<p>Regards,<br>
DHPI team</p>