<h3>Hello {{ $user->name }},</h3>
<br/>
<p>To reset your password please click on the link below:</p>
<a href={{$url}}>link</a>
<br/>
<p>In case of any troubles with login or password, please contact us at support@dhpi.com, or simply reply to this email.</p>
<br/>
<p>--</p>
<br/>
<p>Regards,<br>
DHPI team</p>
