<!DOCTYPE html>
<html class="h-100" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('/js/manifest.js') }}"></script> --}}
    {{-- <script src="{{ asset('/js/vendor.js') }}"></script> --}}

    {{-- app --}}
    <script src="{{ asset('js/app.js') }}" defer></script>

    {{-- Icons --}}
    <link href="{{ asset('css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Theme -->
    <link href="{{ asset('css/theme.css') }}" rel="stylesheet">

    {{-- Custom styles --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app"></div>

     {{-- theme's JSs --}}
    <script src="{{ asset('lib/jquery/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('lib/perfect-scrollbar/js/perfect-scrollbar.min.js')}}" type="text/javascript"></script>
    {{-- <script src="{{ asset('lib/bootstrap/dist/js/bootstrap.bundle.min.js')}}" type="text/javascript" defer></script> --}}
    <script src="{{ asset('js/theme.app.js')}}" type="text/javascript" defer></script>
    <script type="text/javascript">
      $(document).ready(function(){
          App.init();
      });   
    </script>
</body>

</html>
