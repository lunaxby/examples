ssh devuser@46.101.253.165 << EOF
  cd /var/www/html/dhpi;
  git checkout develop;
  git stash;
  git pull;
  npm install;
  npm run prod;
  php artisan migrate;
EOF