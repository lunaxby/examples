<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use App\Entities\Product;

class ProductDeleted
{
    use SerializesModels;

    public $product;

    /**
     * Create a new event instance.
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }
}
