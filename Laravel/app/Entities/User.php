<?php

namespace App\Entities;

use App\Mail\ResetPassword;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\HasApiTokens;
use Watson\Rememberable\Rememberable;

/**
 * Class User
 * @package App
 *
 * @property string $id
 * @property string $name
 * @property string $email
 * @property integer $clinic_id
 * @property string $email_verified_at
 * @property string $password
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property string $role
 * @property string $status
 *
 * @property bool $admin
 * @property bool $adminZoneAvailable
 * @property bool $clinicContact
 *
 * @property Clinic $clinic
 *
 * @method Builder sort($sorts)
 * @method Builder search($search)
 * @method Builder filter($filter)
 */
class User extends Authenticatable
{
    use Notifiable, HasApiTokens, Rememberable;

    const ROLE_ADMIN = 'admin';
    const ROLE_USER_POWER = 'user-power';
    const ROLE_USER = 'user';
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public $rememberCacheTag = 'user_queries';
    public $rememberFor = 60;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @var array
     */
    protected $appends = ['admin', 'adminZoneAvailable', 'clinicContact', 'locationIds'];

    /**
     * @return mixed
     */
    public function getAdminAttribute(): bool
    {
        return $this->isAdmin();
    }

    /**
     * @return mixed
     */
    public function getAdminZoneAvailableAttribute(): bool
    {
        return $this->isAdmin();
    }

    /**
     * Check if the user is an admin
     * @return bool
     */
    private function isAdmin(): bool
    {
        return $this->role === self::ROLE_ADMIN;
    }

    /**
     * Check the user's role and status
     * @param $role
     * @return bool
     */
    public function hasRole($role): bool
    {
        return $this->role == $role && $this->status == self::STATUS_ACTIVE;
    }

    /**
     * Return the user's clinic entity
     * @return BelongsTo
     */
    public function clinic(): BelongsTo
    {
        return $this->belongsTo('App\Entities\Clinic');
    }

    /**
     * Return Clinic entity, if the user is the clinic's contact
     * @return HasOne
     */
    public function clinicContact(): HasOne
    {
        return $this->hasOne('App\Entities\Clinic');
    }

    /**
     * Return true, if the user is the clinic's contact
     * @return bool
     */
    public function getClinicContactAttribute(): bool
    {
        return $this->clinicContact()->exists();
    }

    /**
     * @return BelongsToMany
     */
    public function locations(): BelongsToMany
    {
        return $this->belongsToMany('App\Entities\Location', 'users_locations');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getLocationIdsAttribute()
    {
        return $this->locations()->allRelatedIds();
    }

    /**
     * @param Builder $query
     * @param $sorts
     * @return Builder
     */
    public function scopeSort(Builder $query, $sorts): Builder
    {
        if ($sorts) {
            foreach ($sorts as $sort) {
                $sortByEncoded = json_decode($sort);
                if ($sortByEncoded->id == 'clinic') {
                    $sortByEncoded->id = 'clinics.name';
                }
                $query->orderBy($sortByEncoded->id, isset($sortByEncoded->desc) ? 'desc' : 'asc');
            }
        } else {
            $query->orderBy('clinics.name', 'asc');
        }

        return $query;
    }

    /**
     * @param Builder $query
     * @param $search
     * @return Builder
     */
    public function scopeSearch(Builder $query, $search): Builder
    {
        if ($search) {
            /** @var User $user */
            $user = Auth::user();
            $query = $query->where(function ($query) use ($search, $user) {
                $query->where('users.name', 'like', "%{$search}%")
                    ->orWhere('users.email', 'like', "%{$search}%")
                    ->orWhere('locations.name', 'like', "%{$search}%");

                if ($user->isAdmin()) {
                    $query->orWhere('clinics.name', 'like', "%{$search}%");
                }
            });
        }

        return $query;
    }

    /**
     * @param Builder $query
     * @param $filter
     * @return Builder
     */
    public function scopeFilter(Builder $query, $filter): Builder
    {
        if ($filter) {
            foreach ($filter as $filterElement) {
                $filterDecodeElement = json_decode($filterElement);
                if (!isset($filterDecodeElement->field) || !isset($filterDecodeElement->value)) {
                    continue;
                }

                $query = $query->where($filterDecodeElement->field, $filterDecodeElement->value);
            }
        }

        return $query;
    }

    public function sendPasswordResetNotification($token)
    {
        if ($this->isAdmin()) {
            $route = route('admin.password.reset', ['token' => $token], false);
        } else {
            $route = route('password.reset', ['token' => $token], false);
        }

        $url = url(config('app.url') . $route);

        Mail::to($this->email)->send(new ResetPassword($this, $url));
    }
}
