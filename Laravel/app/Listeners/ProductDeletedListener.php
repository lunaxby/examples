<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use \App\Events\ProductDeleted;
use \App\Entities\Order;
use \App\Entities\Product;
use Illuminate\Support\Facades\DB;
use \App\Events\SubOrderUpdated;
use \App\Events\OrderUpdated;

class ProductDeletedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductDeleted  $event
     * @return void
     */
    public function handle(ProductDeleted $event)
    {
        /** @var Product $product */
        $product = $event->product;
        
        /** @var Order[] $orders */
        $orders = $product->orders()->get();
        $subOrderIds = $product->subOrders()->pluck('sub_orders.id')->toArray();

        /** @var OrderPrices[] $orderPrices */
        $orderPrices = $product->orderPrices()->get();

        DB::beginTransaction();

        foreach ($orderPrices as $orderPrice) {
            if (!$orderPrice->delete()) {
                DB::rollBack();
                throw new \DomainException('Can\'t delete Order Price # '+ $orderPrice->id, 500);
            }
        }

        foreach ($orders as $order) {
            $subOrderWasRecalculated = false;

            foreach ($order->subOrders as $subOrder) {
                if (in_array($subOrder->id, $subOrderIds)) {
                    $subOrderWasRecalculated = true;
                    event(new SubOrderUpdated($subOrder));
                }
            }

            if ($subOrderWasRecalculated) {
                event(new OrderUpdated($order));
            }
        }
        
        DB::commit();
    }
}
