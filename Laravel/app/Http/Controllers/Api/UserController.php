<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\IndexUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserCollection;
use App\Services\UserService;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends Controller
{
    private $userService;

    /**
     * UserController constructor.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;

        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexUserRequest $request
     *
     * @return UserCollection
     *
     * @OA\Get(
     *     path="/api/users",
     *     tags={"User:Users"},
     *     summary="Get users",
     *     description="Return users collection",
     *     @OA\Parameter(
     *          name="per_page",
     *          in="path",
     *          description="Items per page",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="search",
     *          in="path",
     *          required=false,
     *          description="Keyword for search by E-mail and Name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="sortby",
     *          in="path",
     *          description="Ordering items",
     *          example="sortby[]={id:name,desc:true}",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="object"
     *              )
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="page",
     *          in="path",
     *          description="Page No",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="OK"
     *     ),
     * )
     */
    public function index(IndexUserRequest $request)
    {
        try {
            $collection = $this->userService->index($request);
        } catch (\DomainException $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }

        return $collection;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUserRequest $request
     * @return \Illuminate\Http\Response
     * @throws
     *
     * @OA\Post(
     *      path="/api/users",
     *      tags={"User:Users"},
     *      summary="Create user",
     *     @OA\Parameter(
     *          name="name",
     *          in="query",
     *          required=true,
     *          description="Username",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="email",
     *          in="query",
     *          required=true,
     *          description="E-mail",
     *          @OA\Schema(
     *              type="email"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="role",
     *          in="query",
     *          required=true,
     *          description="Role (admin, user-power or user)",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="password",
     *          in="query",
     *          description="Password",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="autoGeneratePassword",
     *          in="query",
     *          description="If true, password will be auto-generated",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="status",
     *          in="query",
     *          description="Status (1 => active, 0 => inactive)",
     *          @OA\Schema(
     *              type="integer",
     *              default="active"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="OK"
     *     ),
     * )
     */
    public function store(CreateUserRequest $request)
    {
        try {
            $user = $this->userService->make($request);
        } catch (\DomainException $e) {
            return response()->json(
                ['message' => 'The given data was invalid', 'errors' => ['password' => [$e->getMessage()]]],
                $e->getCode()
            );
        }

        return response()->json(new UserResource($user));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
     * @OA\Get(
     *     path="/api/users/{id}",
     *     tags={"User:Users"},
     *     summary="Get user",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="User ID",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="OK"
     *     )
     * )
     */
    public function show($id)
    {
        try {
            $user = $this->userService->show($id);
        } catch (NotFoundHttpException $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        } catch (MethodNotAllowedHttpException $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }

        return response()->json($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws
     *
     * @OA\Put(
     *     path="/api/users/{id}",
     *     tags={"User:Users"},
     *     summary="Update user",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="User ID",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="name",
     *          in="query",
     *          description="Username",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="email",
     *          in="query",
     *          description="E-mail",
     *          @OA\Schema(
     *              type="email"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="role",
     *          in="query",
     *          required=true,
     *          description="Role (admin, user-power or user)",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="password",
     *          in="query",
     *          description="Password",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="autoGeneratePassword",
     *          in="query",
     *          description="If true, password will be auto-generated",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="status",
     *          in="query",
     *          required=true,
     *          description="Status (1 => active, 0 => inactive)",
     *          @OA\Schema(
     *              type="integer",
     *              default="active"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="OK"
     *     )
     * )
     */
    public function update(UpdateUserRequest $request, $id)
    {
        try {
            $user = $this->userService->update($request, $id);
        } catch (NotFoundHttpException $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        } catch (\DomainException $e) {
            return response()->json(
                ['message' => 'The given data was invalid', 'errors' => ['password' => [$e->getMessage()]]],
                $e->getCode()
            );
        } catch (ValidationException $e) {
            return response()->json(['message' => $e->getMessage(), 'errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }

        return response()->json($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
     * @OA\Delete(
     *     path="/api/users/{id}",
     *     tags={"User:Users"},
     *     summary="Delete user",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="User ID",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="OK"
     *     )
     * )
     */
    public function destroy($id)
    {
        try {
            $this->userService->destroy($id);
        } catch (\DomainException $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }

        return response()->json();
    }
}
