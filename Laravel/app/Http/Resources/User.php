<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->role,
            'clinic' => new Clinic($this->clinic),
            'locations' => $this->locationIds,
            'status' => $this->status,
            'admin' => $this->admin,
            'adminZoneAvailable' => $this->adminZoneAvailable,
            'clinicContact' => $this->clinicContact,
        ];
    }
}
