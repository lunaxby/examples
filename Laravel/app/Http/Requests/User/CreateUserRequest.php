<?php

namespace App\Http\Requests\User;

use App\Entities\User;
use App\Utils\RegExp;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

/**
 * Class CreateUserRequest
 * @package App\Http\Requests\User
 *
 * @property $name
 * @property $email
 * @property $clinic_id
 * @property $role
 * @property $password
 * @property $status
 * @property $autoGeneratePassword
 * @property array $locations
 */
class CreateUserRequest extends FormRequest
{
    public function __construct()
    {
        parent::__construct();

        Validator::extendImplicit('password_rule', function ($attribute, $value, $parameters, $validator) {
            return !preg_match('/^\s+$/', $value);
        });
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email:strict|unique:users',
            'clinic' => 'nullable|integer',
            'locations' => 'nullable|array',
            'role' => 'required|in:' . User::ROLE_ADMIN . ',' . User::ROLE_USER . ',' . User::ROLE_USER_POWER,
            'password' => [
                'password_rule',
                'required_if:autoGeneratePassword,false',
                'nullable',
                // 'string',
                'regex:' . RegExp::PASS,
                'min:6',
                'max:20',
            ],
            'autoGeneratePassword' => 'boolean',
            'status' => 'nullable|in:' . User::STATUS_ACTIVE . ',' . User::STATUS_INACTIVE,
        ];
    }

    public function messages()
    {
        return [
            'password_rule' => 'Password can not contain only spaces',
            'regex' => 'You can use only uppercase and lowercase letters, digits and special symbols (!@#$%^&*_=+-) for the password.',
            'required_if' => 'Password can\'t be blank. You can use only uppercase and lowercase letters, digits and special symbols (!@#$%^&*_=+-) for the password.'
        ];
    }
}
