<?php

namespace App\Http\Requests\User;

use App\Entities\User;
use App\Utils\RegExp;
use Illuminate\Validation\Factory;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateUserRequest
 * @package App\Http\Requests\User
 *
 * @property $name
 * @property $email
 * @property $clinic_id
 * @property $role
 * @property $password
 * @property $status
 * @property $autoGeneratePassword
 * @property array $locations
 */
class UpdateUserRequest extends FormRequest
{
    public function __construct(Factory $factory)
    {
        parent::__construct();

        $factory->extendImplicit('password_rule', function ($attribute, $value, $parameters, $validator) {
            return !preg_match('/^\s+$/', $value);
        });
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email:strict',
            'clinic_id' => 'nullable|integer|exists:clinics,id',
            'locations' => 'nullable|array',
            'locations.*' => 'integer|exists:locations,id',
            'role' => 'required|in:' . User::ROLE_ADMIN . ',' . User::ROLE_USER . ',' . User::ROLE_USER_POWER,
            'password' => [
                'nullable',
                'string',
                'regex:' . RegExp::PASS,
                'min:6',
                'max:20',
                'password_rule'
            ],
            'autoGeneratePassword' => 'boolean',
            'status' => 'required|in:' . User::STATUS_ACTIVE . ',' . User::STATUS_INACTIVE
        ];
    }

    public function messages()
    {
        return [
            'password_rule' => 'Password can not contain only spaces',
            'regex' => 'You can use only uppercase and lowercase letters, digits and special symbols (!@#$%^&*_=+-) for the password.',
            'required_if' => 'Password can\'t be blank. You can use only uppercase and lowercase letters, digits and special symbols (!@#$%^&*_=+-) for the password.'
        ];
    }
}
