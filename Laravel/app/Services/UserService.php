<?php

namespace App\Services;

use App\Entities\UserLocation;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\IndexUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\UserCollection;
use App\Mail\CreateUserMail;
use App\Mail\UpdateUserMail;
use App\Entities\User;
use App\Http\Resources\User as UserResource;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserService
 * @package App\Services
 */
class UserService
{
    /**
     * Users count per page
     */
    const API_TOKEN_PER_PAGE = 50;

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * UserService constructor.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param IndexUserRequest $request
     * @return UserCollection
     */
    public function index(IndexUserRequest $request): UserCollection
    {
        $perPage = (int) $request->get('per_page', self::API_TOKEN_PER_PAGE);

        /** @var Builder|User $query */
        $query = User::query()->select(['users.*', 'clinics.name as clinic_name'])->distinct();
        $query = $query->join('clinics', 'users.clinic_id', '=', 'clinics.id', 'left');
        $query = $query->join('users_locations', 'users_locations.user_id', '=', 'users.id', 'left');
        $query = $query->join('locations', 'users_locations.location_id', '=', 'locations.id', 'left');
        $sorts = $request->get('sortby', []);

        $query = $query->sort($sorts);
        $query = $query->search($request->search);
        $query = $query->filter($request->filter);

        /** @var User $authUser */
        $authUser = Auth::user();
        if (!$authUser->admin) {
            if ($authUser->clinic_id != null) {
                $query = $query->where('users.clinic_id', $authUser->clinic_id);
            } else {
                throw new \DomainException('You must be related clinic', 500);
            }
        }

        $query = $query->groupBy('users.id')->paginate($perPage);

        $collection = (new UserCollection($query))->additional([
            'meta' => [
                'sortby' => $sorts
            ]
        ]);

        return $collection;
    }

    /**
     * @param CreateUserRequest $request
     * @param bool $fromClinic
     * @return User
     * @throws \DomainException|\Exception
     */
    public function make(CreateUserRequest $request, bool $fromClinic = false): User
    {
        if ($request->autoGeneratePassword) {
            $password = Str::random(8);
        } else {
            $password = $request->password;
        }

        $user = new User();

        /** @var User $authUser */
        $authUser = Auth::user();
        if ($authUser->admin) {
            $user->clinic_id = $request->clinic_id;
        } else {
            $user->clinic_id = $authUser->clinic_id;
        }

        if ($request->role == User::ROLE_ADMIN) {
            $user->clinic_id = null;
        }

        if (($request->role == User::ROLE_USER_POWER || $request->role == User::ROLE_USER)
            && $request->clinic_id == null
        ) {
            if ($authUser->admin && !$fromClinic) {
                throw new \Exception('Clinic can\'t be empty', 500);
            } else {
                $user->clinic_id = $authUser->clinic_id;
            }
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->password = bcrypt($password);
        $user->email_verified_at = now();
        $user->created_at = now();
        $user->status = is_null($request->status) || $request->status!= 0 ? User::STATUS_ACTIVE : User::STATUS_INACTIVE;
        $user->save();

        if (isset($request->locations) && count($request->locations) > 0) {
            foreach ($request->locations as $location) {
                $userLocation = new UserLocation();
                $userLocation->user_id = $user->id;
                $userLocation->location_id = $location;
                $userLocation->save();
            }
        }

        /** @var User $author */
        $author = Auth::user();

        try {
            Mail::to($user->email)->send(new CreateUserMail($user, $password, $author));
        } catch (\Exception $e) {
            throw new \ErrorException('The email wasn\'t sent ', 500);
        }

        User::flushCache('user_queries');

        return $user;
    }

    /**
     * @param $id
     * @return UserResource
     */
    public function show($id): UserResource
    {
        /** @var User $user */
        $user = User::query()->find($id);

        /** @var User $authUser */
        $authUser = Auth::user();

        if ($authUser->role == User::ROLE_USER && $authUser->id != $user->id) {
            throw new MethodNotAllowedHttpException([], 'Method not allowed', null, 405);
        }

        if ($user === null) {
            throw new NotFoundHttpException('User not found', null, 404);
        }

        if (!$authUser->admin && $user->clinic_id !== $authUser->clinic_id) {
            throw new NotFoundHttpException('User not found', null, 404);
        }

        return new UserResource($user);
    }

    /**
     * @param UpdateUserRequest $request
     * @param $id
     * @return UserResource
     * @throws \Exception|\DomainException|NotFoundHttpException
     */
    public function update(UpdateUserRequest $request, $id): UserResource
    {
        $password = null;
        if ($request->get('autoGeneratePassword', false)) {
            $password = Str::random(8);
        }

        /** @var User $user */
        $user = User::query()->find($id);

        if (!$user) {
            throw new NotFoundHttpException('User not found', null, 404);
        }

        if ($request->password) {
            /*if (preg_match('/^\s+$/',$request->password)) {
                throw new \DomainException('You can use only uppercase and lowercase letters, digits and special symbols (!@#$%^&*_=+-) for the password', 500);
            }*/
            $password = $request->password;
        }

        /** @var User $authUser */
        $authUser = Auth::user();

        if ($user->id == $authUser->id) {
            if ($request->status != $user->status) {
                $validator = Validator::make([], []);
                $validator->errors()->add('status', 'You can\'t change your status');
                throw new ValidationException($validator);
            }

            if ($request->role != $user->role) {
                $validator = Validator::make([], []);
                $validator->errors()->add('role', 'You can\'t change your role');
                throw new ValidationException($validator);
            }
        }

        if ($authUser->admin) {
            $user->clinic_id = $request->clinic_id;
        } else {
            $user->clinic_id = $authUser->clinic_id;
        }

        if ($request->role == User::ROLE_ADMIN) {
            $user->clinic_id = null;
        }

        if (($request->role == User::ROLE_USER_POWER || $request->role == User::ROLE_USER)
            && $request->clinic_id == null
        ) {
            if ($authUser->admin) {
                throw new \Exception('Clinic can\'t be empty', 500);
            } else {
                $user->clinic_id = $authUser->clinic_id;
            }
        }

        $user->name = $request->name ?: $user->name;
        $user->email = $request->email ?: $user->email;
        $user->role = $request->role ?: $user->role;

        if ($password) {
            $user->password = bcrypt($password);
        }

        $user->updated_at = now();

        if ($request->status !== null) {
            $user->status = (int) $request->status == 0 ? User::STATUS_INACTIVE : User::STATUS_ACTIVE;
        }

        $user->save();

        UserLocation::query()->where(['user_id' => $user->id])->delete();
        if (isset($request->locations) && count($request->locations) > 0) {
            foreach ($request->locations as $location) {
                $userLocation = new UserLocation();
                $userLocation->user_id = $user->id;
                $userLocation->location_id = $location;
                $userLocation->save();
            }
        }

        /** @var User $author */
        $author = Auth::user();

        if ($password) {
            Mail::to($user->email)->send(new UpdateUserMail($user, $password, $author));
        }

        User::flushCache('user_queries');

        return new UserResource($user);
    }

    /**
     * @param $id
     * @return bool
     */
    public function destroy($id): bool
    {
        /** @var User $authUser */
        $authUser = Auth::user();

        if ($authUser->id == $id) {
            throw new \DomainException('You can\'t remove yourself', 500);
        }

        $result = User::destroy($id);

        if (!$result) {
            throw new \DomainException('Can\'t remove user', 500);
        }

        User::flushCache('user_queries');

        return $result > 0;
    }
}
