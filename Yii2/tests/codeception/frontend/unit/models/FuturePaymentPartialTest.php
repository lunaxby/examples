<?php

namespace tests\codeception\frontend\unit\models;

use common\models\Category;
use common\models\ShortNotes;
use Yii;
use tests\codeception\frontend\unit\TestCase;
use common\models\FuturePayment;
use common\models\Wallet;
use common\models\Project;
use common\models\FuturePaymentHistory;
use common\models\Balance;

class FuturePaymentPartialTest extends TestCase
{
    const FUTURE_ONE_TIME_PAYMENT_ID = 101;
    const FUTURE_DAILY_PAYMENT_ID = 102;
    const FUTURE_WEEKLY_PAYMENT_ID = 103;
    const FUTURE_MONTHLY_PAYMENT_ID = 104;

    const NOTE_FOR_ONETIME_PAYMENT = 'onetime';
    const NOTE_FOR_DAILY_PAYMENT = 'daily';
    const NOTE_FOR_WEEKLY_PAYMENT = 'weekly';
    const NOTE_FOR_MONTHLY_PAYMENT = 'monthly';

    const DATE = '01.09.2016';
    const DATE_PER_DAY = '02.09.2016';
    const DATE_PER_WEEK = '08.09.2016';
    const DATE_PER_MONTH = '01.10.2016';

    const PROJECT_ID = 107;
    const WALLET_ID = 101;
    const CATEGORY_ID = 47;
    const SHORTNOTE_ID = 47;

    protected function setUp()
    {
        parent::setUp();


        $project = new Project();
        $project->id = self::PROJECT_ID;
        $project->title = 'Test Project for Future Payment Partial';
        $project->wallet = 'Test Wallet';
        $project->save();


        $wallet = new Wallet();
        $wallet->id = self::WALLET_ID;
        $wallet->project_id = self::PROJECT_ID;
        $wallet->title = 'Касса';
        $wallet->sum_capital = 100;
        $wallet->save();

        $category = Category::findOne(self::CATEGORY_ID);
        if (empty($category)) {
            $category = new Category();
            $category->id = self::CATEGORY_ID;
            $category->title = 'Test Category';
            $category->project_id = self::PROJECT_ID;
            $category->level = 1;
            $category->type = 0;
            $category->save();
        }
        $shortnote = ShortNotes::findOne(self::CATEGORY_ID);
        if (empty($shortnote)) {
            $shortnote = new ShortNotes();
            $shortnote->id = self::SHORTNOTE_ID;
            $shortnote->title = 'Test Category';
            $shortnote->project_id = self::PROJECT_ID;
            $shortnote->category_id = self::CATEGORY_ID;
            $shortnote->save();
        }

        $futurePayment = new FuturePayment();
        $futurePayment->id = self::FUTURE_ONE_TIME_PAYMENT_ID;
        $futurePayment->outflow = 100;
        $futurePayment->category_id = self::CATEGORY_ID;
        $futurePayment->shortnotes_id = self::SHORTNOTE_ID;
        $futurePayment->project_id = self::PROJECT_ID;
        $futurePayment->wallet_id = self::WALLET_ID;
        $futurePayment->added_at = self::DATE;
        $futurePayment->notes = self::NOTE_FOR_ONETIME_PAYMENT;
        $futurePayment->periodicity = FuturePayment::PERIODICITY_ONETIME_ID;
        $futurePayment->active = 1;
        $futurePayment->save();

        $futurePayment = new FuturePayment();
        $futurePayment->id = self::FUTURE_DAILY_PAYMENT_ID;
        $futurePayment->outflow = 100;
        $futurePayment->category_id = self::CATEGORY_ID;
        $futurePayment->shortnotes_id = self::SHORTNOTE_ID;
        $futurePayment->project_id = self::PROJECT_ID;
        $futurePayment->wallet_id = self::WALLET_ID;
        $futurePayment->added_at = self::DATE;
        $futurePayment->notes = self::NOTE_FOR_DAILY_PAYMENT;
        $futurePayment->periodicity = FuturePayment::PERIODICITY_DAILY_ID;
        $futurePayment->active = 1;
        $futurePayment->save();

        $futurePayment = new FuturePayment();
        $futurePayment->id = self::FUTURE_WEEKLY_PAYMENT_ID;
        $futurePayment->outflow = 100;
        $futurePayment->category_id = self::CATEGORY_ID;
        $futurePayment->shortnotes_id = self::SHORTNOTE_ID;
        $futurePayment->project_id = self::PROJECT_ID;
        $futurePayment->wallet_id = self::WALLET_ID;
        $futurePayment->added_at = self::DATE;
        $futurePayment->notes = self::NOTE_FOR_WEEKLY_PAYMENT;
        $futurePayment->periodicity = FuturePayment::PERIODICITY_WEEKLY_ID;
        $futurePayment->active = 1;
        $futurePayment->save();

        $futurePayment = new FuturePayment();
        $futurePayment->id = self::FUTURE_MONTHLY_PAYMENT_ID;
        $futurePayment->outflow = 100;
        $futurePayment->category_id = self::CATEGORY_ID;
        $futurePayment->shortnotes_id = self::SHORTNOTE_ID;
        $futurePayment->project_id = self::PROJECT_ID;
        $futurePayment->wallet_id = self::WALLET_ID;
        $futurePayment->added_at = self::DATE;
        $futurePayment->notes = self::NOTE_FOR_MONTHLY_PAYMENT;
        $futurePayment->periodicity = FuturePayment::PERIODICITY_MONTHLY_ID;
        $futurePayment->active = 1;
        $futurePayment->save();
    }

    protected function tearDown()
    {
        FuturePayment::deleteAll(['project_id' => self::PROJECT_ID]);
        Wallet::deleteAll(['id' => self::WALLET_ID]);
        FuturePaymentHistory::deleteAll(['project_id' => self::PROJECT_ID]);
        Balance::deleteAll(['project_id' => self::PROJECT_ID]);
        Project::deleteAll(['id' => self::PROJECT_ID]);
        Category::deleteAll(['project_id' => self::PROJECT_ID]);
    }

    public function testPerformPartialOneTimePayment()
    {
        $futurePayment = FuturePayment::findOne(self::FUTURE_ONE_TIME_PAYMENT_ID);
        /** @var $futurePayment FuturePayment */
        $this->assertEquals($futurePayment->outflow, 100);

        $futurePayment = FuturePayment::findOne(self::FUTURE_ONE_TIME_PAYMENT_ID);
        $futurePayment->performPartialPayment(50);

        $futurePayment = FuturePayment::findOne(self::FUTURE_ONE_TIME_PAYMENT_ID);
        $this->assertEquals($futurePayment->outflow, 100);
        $balance = Balance::find()->where(['project_id' => self::PROJECT_ID, 'notes' => 'onetime'])->one();
        $this->assertEquals($balance->outflow, 50);

        $futurePayment = FuturePayment::findOne(self::FUTURE_ONE_TIME_PAYMENT_ID);
        $futurePayment->performPartialPayment(150);
        $futurePayment = FuturePayment::findOne(self::FUTURE_ONE_TIME_PAYMENT_ID);
        $this->assertEquals($futurePayment->outflow, 100);
        $balance = Balance::find()->where(['project_id' => self::PROJECT_ID])->all();
        $this->assertEquals(count($balance), 2);

        $futurePayment = FuturePayment::findOne(self::FUTURE_ONE_TIME_PAYMENT_ID);
        $futurePayment->performPartialPayment(50);
        $futurePayment = FuturePayment::findOne(self::FUTURE_ONE_TIME_PAYMENT_ID);
        $this->assertEquals($futurePayment->active, 0);
        $this->assertEquals($futurePayment->outflow, 100);

        $condition = [
            'project_id' => self::PROJECT_ID,
            'notes' => self::NOTE_FOR_ONETIME_PAYMENT
        ];
        $balance = Balance::find()->where($condition)->all();
        $this->assertEquals(count($balance), 2);

        $futurePaymentHistories = FuturePaymentHistory::find()->where(['project_id' => self::PROJECT_ID])->all();
        $this->assertEquals(count($futurePaymentHistories), 2);
    }

    public function testPerformPartialDailyPayment()
    {
        $futurePayment = FuturePayment::findOne(self::FUTURE_DAILY_PAYMENT_ID);
        /* @var $futurePayment FuturePayment */
        $this->assertEquals($futurePayment->outflow, 100);

        $futurePayment = FuturePayment::findOne(self::FUTURE_DAILY_PAYMENT_ID);
        $futurePayment->performPartialPayment(50);
        $futurePayment = FuturePayment::findOne(self::FUTURE_DAILY_PAYMENT_ID);
        $this->assertEquals($futurePayment->outflow, 100);

        $futurePayment = FuturePayment::findOne(self::FUTURE_DAILY_PAYMENT_ID);
        $futurePayment->performPartialPayment(150);
        $futurePayment = FuturePayment::findOne(self::FUTURE_DAILY_PAYMENT_ID);
        $this->assertEquals($futurePayment->outflow, 100);

        $futurePayment = FuturePayment::findOne(self::FUTURE_DAILY_PAYMENT_ID);
        $futurePayment->performPartialPayment(50);
        $futurePayment = FuturePayment::findOne(self::FUTURE_DAILY_PAYMENT_ID);
        $this->assertEmpty($futurePayment);

        $condition = [
            'project_id' => self::PROJECT_ID,
            'periodicity' => FuturePayment::PERIODICITY_DAILY_ID
        ];
        $futurePayment = FuturePayment::find()->where($condition)->one();
        $this->assertEquals($futurePayment->active, 1);
        $this->assertEquals($futurePayment->outflow, 100);
        $this->assertEquals($futurePayment->added_at, self::DATE_PER_DAY);

        $condition = [
            'project_id' => self::PROJECT_ID,
            'notes' => self::NOTE_FOR_DAILY_PAYMENT
        ];
        $balance = Balance::find()->where($condition)->all();
        $this->assertEquals(count($balance), 2);

        $futurePaymentHistories = FuturePaymentHistory::find()->where(['project_id' => self::PROJECT_ID])->all();
        $this->assertEquals(count($futurePaymentHistories), 2);
    }

    public function testPerformPartialWeeklyPayment()
    {
        $futurePayment = FuturePayment::findOne(self::FUTURE_WEEKLY_PAYMENT_ID);
        /* @var $futurePayment FuturePayment */
        $this->assertEquals($futurePayment->outflow, 100);

        $futurePayment = FuturePayment::findOne(self::FUTURE_WEEKLY_PAYMENT_ID);
        $futurePayment->performPartialPayment(50);
        $futurePayment = FuturePayment::findOne(self::FUTURE_WEEKLY_PAYMENT_ID);
        $this->assertEquals($futurePayment->outflow, 100);

        $futurePayment = FuturePayment::findOne(self::FUTURE_WEEKLY_PAYMENT_ID);
        $futurePayment->performPartialPayment(150);
        $futurePayment = FuturePayment::findOne(self::FUTURE_WEEKLY_PAYMENT_ID);
        $this->assertEquals($futurePayment->outflow, 100);

        $futurePayment = FuturePayment::findOne(self::FUTURE_WEEKLY_PAYMENT_ID);
        $futurePayment->performPartialPayment(50);
        $futurePayment = FuturePayment::findOne(self::FUTURE_WEEKLY_PAYMENT_ID);
        $this->assertEmpty($futurePayment);

        $condition = [
            'project_id' => self::PROJECT_ID,
            'periodicity' => FuturePayment::PERIODICITY_WEEKLY_ID
        ];
        $futurePayment = FuturePayment::find()->where($condition)->one();
        $this->assertEquals($futurePayment->active, 1);
        $this->assertEquals($futurePayment->outflow, 100);
        $this->assertEquals($futurePayment->added_at, self::DATE_PER_WEEK);

        $condition = [
            'project_id' => self::PROJECT_ID,
            'notes' => self::NOTE_FOR_WEEKLY_PAYMENT
        ];
        $balance = Balance::find()->where($condition)->all();
        $this->assertEquals(count($balance), 2);

        $futurePaymentHistories = FuturePaymentHistory::find()->where(['project_id' => self::PROJECT_ID])->all();
        $this->assertEquals(count($futurePaymentHistories), 2);
    }

    public function testPerformPartialMonthlyPayment()
    {
        $futurePayment = FuturePayment::findOne(self::FUTURE_MONTHLY_PAYMENT_ID);
        /* @var $futurePayment FuturePayment */
        $this->assertEquals($futurePayment->outflow, 100);

        $futurePayment = FuturePayment::findOne(self::FUTURE_MONTHLY_PAYMENT_ID);
        $futurePayment->performPartialPayment(50);
        $futurePayment = FuturePayment::findOne(self::FUTURE_MONTHLY_PAYMENT_ID);
        $this->assertEquals($futurePayment->outflow, 100);

        $futurePayment = FuturePayment::findOne(self::FUTURE_MONTHLY_PAYMENT_ID);
        $futurePayment->performPartialPayment(150);
        $futurePayment = FuturePayment::findOne(self::FUTURE_MONTHLY_PAYMENT_ID);
        $this->assertEquals($futurePayment->outflow, 100);

        $futurePayment = FuturePayment::findOne(self::FUTURE_MONTHLY_PAYMENT_ID);
        $futurePayment->performPartialPayment(50);
        $futurePayment = FuturePayment::findOne(self::FUTURE_MONTHLY_PAYMENT_ID);
        $this->assertEmpty($futurePayment);

        $condition = [
            'project_id' => self::PROJECT_ID,
            'periodicity' => FuturePayment::PERIODICITY_MONTHLY_ID
        ];
        $futurePayment = FuturePayment::find()->where($condition)->one();
        $this->assertEquals($futurePayment->active, 1);
        $this->assertEquals($futurePayment->outflow, 100);
        $this->assertEquals($futurePayment->added_at, self::DATE_PER_MONTH);

        $condition = [
            'project_id' => self::PROJECT_ID,
            'notes' => self::NOTE_FOR_MONTHLY_PAYMENT
        ];
        $balance = Balance::find()->where($condition)->all();
        $this->assertEquals(count($balance), 2);

        $futurePaymentHistories = FuturePaymentHistory::find()->where(['project_id' => self::PROJECT_ID])->all();
        $this->assertEquals(count($futurePaymentHistories), 2);
    }
}
