<?php

namespace common\models;

use frontend\components\CategoryHelper;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $title
 * @property integer $project_id
 * @property integer $user_id
 * @property integer $type
 * @property integer $level
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Balance[] $balances
 * @property Project $project
 */
class Category extends \yii\db\ActiveRecord
{
    public $isExampleCopy;

    public $categoryTitle = "";

    const LEVEL_SYSTEM = 0;
    const LEVEL_USER = 1;

    public static $_level = [
        'system'=>0,
        'user'=>1
    ];

    const TYPE_ZERO = 0;

    const TRANSFER = 1;
    const INVEST = 2;
    const DIVIDENT = 3;
    const AD = 4;


    const TRANSFER_ID = 17;
    const INVEST_ID = 26;
    const DIVIDENT_ID = 27;



    /**
     * @inheritdoc
     */
    public function behaviors()
    {

        return [
            'timestampwithcondition' => [
                'class' => 'common\behaviors\TimeStampWithCondition',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'in_attribute' => 'isExampleCopy'
            ],
            'name' => [
                'class' => 'common\behaviors\NameNormalization',
                'in_attribute' => 'title',

            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
    */
    public function rules()
    {
        return [
            [['type', 'level', 'project_id', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 1024],
            ['title', 'required'],
            ['title', 'validateUniqueTitle'],
//            [['title', 'project_id'], 'unique', 'targetAttribute' => ['title', 'project_id']],
//            ['title', 'unique', 'targetAttribute' => 'project_id'],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $reportTemplates = ReportTemplate::find()->where(['project_id' => $this->project_id])->all();
            if (!empty($reportTemplates)) {
                foreach ($reportTemplates as $reportTemplate) {
                    /** @var  $reportTemplate ReportTemplate */
                    $reportTemplateNewCategory = new ReportTemplateNewCategory();
                    $reportTemplateNewCategory->report_template_id = $reportTemplate->id;
                    $reportTemplateNewCategory->category_id = $this->id;
                    $reportTemplateNewCategory->save();
                }
            }

            $dashboardSetting = DashboardSetting::findOne(['project_id' => $this->project_id]);
            /** @var $dashboardSetting DashboardSetting */
            if (!empty($dashboardSetting)) {
                $categoryGaugeList = $dashboardSetting->category_gauge_list;
                $categoryGaugeList[] = (string)$this->id;

                $dashboardSetting->category_gauge_list = Json::encode($categoryGaugeList);
                $dashboardSetting->category_gauge_list_value = Json::encode($dashboardSetting->category_gauge_list_value);
                $dashboardSetting->category_sale_list = Json::encode($dashboardSetting->category_sale_list);
                $dashboardSetting->save();
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function getData($project_id)
    {
        $list = $this::find()
            ->where('project_id=:project_id or level!=:level', [
                ':project_id' => $project_id,
                ':level' => self::$_level['user'],
            ])
            ->orderBy('type, title')->all();

        $data = [];
        $options = [];

        foreach ($list as $item) {
            $data[$item->id] = $item->title;
            $options[$item->id] = [
                'type' => $item->type
            ];
        }

        return [
            'data' => $data,
            'options' => $options
        ];
    }

    public function getCategoryByTitle($title, $project_id)
    {
        $title = trim($title);
        $title = strtolower($title);

        $model = $this::find()->where('title=:title and project_id = :project_id', [
            ':title' => $title,
            ':project_id' => $project_id,
        ])->one();

        if (!isset($model->id)) {
            $model = new Category();
            $model->attributes = [
                'title' => $title,
                'project_id' => $project_id,
                'level' => 1,
                'type' => 0
            ];
            $model->save();
            return $model;
        } else {
            return $model;
        }
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => 'Название',
            'project_id' => Yii::t('app', 'Project ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBalances()
    {
        return $this->hasMany(Balance::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    public function getShortNotes()
    {
        return $this->hasMany(ShortNotes::className(), ['category_id' => 'id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function createSystemCategories($projectId)
    {
        if (isset(Yii::$app->user)) {
            $userId = Yii::$app->user->id;
        } else {
            $userId = null;
        }

        $categories = CategoryHelper::getSystemCategoriesData();
        $newCategories = [];
        foreach ($categories as $category) {
            $newCategory = new Category();
            $newCategory->title = $category['title'];
            $newCategory->project_id = $projectId;
            $newCategory->user_id = $userId;
            $newCategory->level = Category::LEVEL_SYSTEM;
            $newCategory->type = $category['type'];
            if ($newCategory->save()) {
                $newCategories[] = [
                    'oldCategoryId' => ArrayHelper::getValue($category, 'oldCategoryId'),
                    'newCategoryId' => $newCategory->id
                ];
            }
        }

        return $newCategories;
    }

    public static function createSystemCategory($projectId, $category, $validate = true)
    {
        if (isset(Yii::$app->user)) {
            $userId = Yii::$app->user->id;
        } else {
            $userId = null;
        }

        $newCategory = new Category();
        $newCategory->title = $category['title'];
        $newCategory->project_id = $projectId;
        $newCategory->user_id = $userId;
        $newCategory->level = Category::LEVEL_SYSTEM;
        $newCategory->type = $category['type'];
        $return = null;
        if ($newCategory->save($validate)) {
            $return = [
                'oldCategoryId' => $category['oldCategoryId'],
                'newCategoryId' => $newCategory->id,
                'title' => $newCategory->title
            ];
        }
        return $return;
    }

    public function validateUniqueTitle()
    {
        $condition = [
            'project_id' => $this->project_id,
            'LOWER(title)' => strtolower($this->title)
        ];
        $model = Category::find()->where($condition)->one();
        if (!empty($model)) {
            try {
                // for frontend
                $link = Html::a('Управление записями', ['/balance/move', 'projectId' => $this->project_id]);
                $msg = '<div class="help-block">Категория <b>' . $this->title . '</b> уже существует.</div> Переместите записи в категорию через ' . $link;
            } catch (\Exception $exc) {
                // for tests, console
                $msg = 'Категория ' . $this->title . ' уже существует. Переместите записи в категорию через ';
            }
            $this->addError('title', $msg);
        }
    }

    public static function getDividentCategoryId($projectId)
    {
        $cacheKey = 'getDividentCategoryId_' . $projectId;
        $result = \Yii::$app->cacheHelper->getCacheValue($cacheKey);
        if ($result === \Yii::$app->cacheHelper->notCached) {
            /** @var Category $category */
            $category = Category::find()->where(['type' => 3, 'level' => 0, 'project_id' => $projectId])->one();
            if (isset($category->id)) {
                $result = $category->id;
            } else {
                Yii::$app->session->setFlash('warning', 'Не существует системная категория Дивиденды для проекта');
                $result = null;
            }

            \Yii::$app->cacheHelper->setCacheValue($cacheKey, $result);
        }
        return $result;
    }

    public static function getInvestCategoryId($projectId)
    {
        $cacheKey = 'getInvestCategoryId_' . $projectId;
        $result = \Yii::$app->cacheHelper->getCacheValue($cacheKey);
        if ($result === \Yii::$app->cacheHelper->notCached) {

            /** @var Category $category */
            $category = Category::find()->where(['type' => 2, 'level' => 0, 'project_id' => $projectId])->one();
            if (empty($category)) {
                Yii::$app->session->setFlash('warning', 'Не существует системная категория Инвест для проекта');
                $result = null;
            } else {
                $result = $category->id;
            }

            \Yii::$app->cacheHelper->setCacheValue($cacheKey, $result);
        }
        return $result;
    }

    public static function getTransferCategoryId($projectId)
    {
        $cacheKey = 'getTransferCategoryId_' . $projectId;
        $result = \Yii::$app->cacheHelper->getCacheValue($cacheKey);
        if ($result === \Yii::$app->cacheHelper->notCached) {

            /** @var Category $category */
            $category = Category::find()->where(['type' => 1, 'level' => 0, 'project_id' => $projectId])->one();
            if (empty($category)) {
                Yii::$app->session->setFlash('warning', 'Не существует системная категория Перевод для проекта');
                $result = null;
            } else {
                $result = $category->id;
            }

            \Yii::$app->cacheHelper->setCacheValue($cacheKey, $result);
        }
        return $result;
    }

    public static function getCategoriesWithoutDividendAndInvest($projectId)
    {
        /** @var Category $category */
        $category = Category::find()->where([
            'level' => 1,
            'project_id' => $projectId
        ])->andWhere(['!=', 'id', self::getDividentCategoryId($projectId)])
            ->andWhere(['!=', 'id', self::getInvestCategoryId($projectId)])
            ->column();
        return $category;
    }

    public static function getCategoryAd($projectId)
    {
        $cacheKey = 'getCategoryAd_' . $projectId;
        $result = \Yii::$app->cacheHelper->getCacheValue($cacheKey);
        if ($result === \Yii::$app->cacheHelper->notCached) {

            $condition = [
                'LOWER(title)' => 'реклама',
                'project_id' => $projectId
            ];
            /** @var Category $category */
            $category = Category::find()->where($condition)->one();
            if (empty($category)) {
                Yii::$app->session->setFlash('warning', 'Не существует системная категория Реклама для проекта');
                $result = null;
            } else {
                $result = $category->id;
            }
            \Yii::$app->cacheHelper->setCacheValue($cacheKey, $result);
        }
        return $result;
    }

    public static function getAllCategoryByProject($projectId)
    {
        /** @var Category $category */
        $category = Category::find()->where([
            'project_id' => $projectId
        ])->andWhere(['!=', 'id', self::getTransferCategoryId($projectId)])
            ->column();
        return $category;
    }
}
