<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "future_payment_setting".
 *
 * @property integer $id
 * @property integer $calculate_forecast_type
 * @property integer $project_id
 */
class FuturePaymentSetting extends ActiveRecord
{
    const FORECAST_BY_FUTURE_PAYMENT_ID = 1;
    const FORECAST_BY_FUTURE_PAYMENT_TEXT = 'Cчитать исключительно исходя из информации которая добавлена в разделе Будущие платежи';
    const FORECAST_BY_DASHBOARD_ID = 2;
    const FORECAST_BY_DASHBOARD_TEXT = 'Cчитать исходя из данных по затратам категорий раздела «dashboard» за минусом уже совершенных затрат';
    const FORECAST_BY_MIXED_ID = 3;
    const FORECAST_BY_MIXED_TEXT = 'Смешанный метод';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'future_payment_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['calculate_forecast_type', 'project_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'calculate_forecast_type' => 'Способ расчета прогноза расходов',
            'project_id' => 'Project ID',
        ];
    }

    public static function getForecastTypes()
    {
        return [
            self::FORECAST_BY_FUTURE_PAYMENT_ID => self::FORECAST_BY_FUTURE_PAYMENT_TEXT,
            self::FORECAST_BY_DASHBOARD_ID => self::FORECAST_BY_DASHBOARD_TEXT,
            self::FORECAST_BY_MIXED_ID => self::FORECAST_BY_MIXED_TEXT,
        ];
    }

    public static function createSettingForNewProject($projectId)
    {
        $model = new FuturePaymentSetting();
        $model->calculate_forecast_type = self::FORECAST_BY_DASHBOARD_ID;
        $model->project_id = $projectId;
        $model->save();
    }
}
