<?php

namespace common\models;

use DateTime;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "future_payment".
 *
 * @property integer $id
 * @property string $outflow
 * @property string $inflow
 * @property integer $category_id
 * @property integer $shortnotes_id
 * @property integer $project_id
 * @property string $notes
 * @property integer $wallet_id
 * @property integer $added_at
 * @property integer $type
 * @property integer $parent_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $tag
 * @property integer $user_id
 * @property integer $periodicity
 * @property integer $active
 * @property integer $outflow_partial
 * @property integer $inflow_partial
 * @property integer $new_category
 * @property integer $new_shortnote
 * @property integer $amocrm_lead_id
 * @property integer $amocrm_lead_status
 * @property integer $manager_id
 * @property Manager $manager
 * @property integer $contractor_id
 * @property Contractor $contractor
 *
 * @property Category $category
 * @property ShortNotes $shortnotes
 * @property Project $project
 * @property User $user
 * @property Wallet $wallet
 */
class FuturePayment extends ActiveRecord
{
    public $isExampleCopy;
    public $partial;
    public $new_category;
    public $new_shortnote;

    const PERIODICITY_UNDEFINED_TEXT = 'Не указано';
    const PERIODICITY_ONETIME_ID = 1;
    const PERIODICITY_ONETIME_TEXT = 'Разово';
    const PERIODICITY_DAILY_ID = 2;
    const PERIODICITY_DAILY_TEXT = 'Ежедневно';
    const PERIODICITY_DAILY_INTERVAL = '+1 day';
    const PERIODICITY_WEEKLY_ID = 3;
    const PERIODICITY_WEEKLY_TEXT = 'Неделя';
    const PERIODICITY_WEEKLY_INTERVAL = '+1 week';
    const PERIODICITY_MONTHLY_ID = 4;
    const PERIODICITY_MONTHLY_TEXT = 'Месяц';
    const PERIODICITY_MONTHLY_INTERVAL = '+1 month';
    const ACTIVE_TEXT = 'Активный';
    const ACTIVE = 1;
    const NO_ACTIVE_TEXT = 'Неактивный';
    const NO_ACTIVE = 0;

    public static $balance_type = [
        'basic' => 0,
        'transfer' => 1,
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'future_payment';
    }

    public function behaviors()
    {

        return [
            'timestampwithcondition' => [
                'class' => 'common\behaviors\TimeStampWithCondition',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'in_attribute' => 'isExampleCopy'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'wallet_id', 'type', 'parent_id', 'created_at', 'updated_at', 'user_id', 'periodicity', 'active', 'new_category', 'new_shortnote', 'manager_id', 'contractor_id'], 'integer'],
            [['notes', 'tag'], 'string'],
            [['added_at'], 'safe'],
            [['category_id', 'shortnotes_id', 'wallet_id', 'project_id', 'periodicity'], 'required'],
            [['outflow', 'inflow', ], 'number'],
            [['amocrm_lead_id', 'amocrm_lead_status'], 'safe'],
            [['amocrm_lead_id', 'amocrm_lead_status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'outflow' => 'Расход',
            'inflow' => 'Приход',
            'category_id' => 'Категория',
            'shortnotes_id' => 'Подкатегория',
            'project_id' => 'Проект',
            'notes' => 'Примечание',
            'wallet_id' => 'Кошелек',
            'added_at' => 'Дата',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'tag' => 'Тег',
            'type' => 'Тип записи',
            'user_id' => 'Пользователь',
            'periodicity' => 'Периодичность',
            'active' => 'Статус',
            'manager_id' => 'Менеджер',
            'contractor_id' => 'Контрагент',
        ];
    }

    public function beforeValidate()
    {
        if ($this->added_at != '') {
            $datetime = DateTime::createFromFormat('d.m.Y', $this->added_at);
            if ($datetime) {
                $this->added_at = $datetime->getTimestamp();
            }
        } else {
            $this->added_at = time();
        }

        if ($this->outflow == '' && $this->inflow == '') {
            $this->addError('outflow', 'Должно быть заполнено одно из полей Расход или Доход');
        }

        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($insert) {
            $this->outflow_partial = $this->outflow;
            $this->inflow_partial = $this->inflow;
        }
        return true;
    }

    public function afterFind()
    {
        $this->added_at = date('d.m.Y', $this->added_at);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShortnotes()
    {
        return $this->hasOne(ShortNotes::className(), ['id' => 'shortnotes_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWallet()
    {
        return $this->hasOne(Wallet::className(), ['id' => 'wallet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function initCategory()
    {
        $this->type = self::$balance_type['basic'];
        if ($this->category_id != "") {
            $category = new Category();
            if ($this->new_category) {
                $categoryModel = $category->getCategoryByTitle($this->category_id, $this->project_id);
            } else {
                $categoryModel = $category::findOne($this->category_id);
                $this->type = $categoryModel->type;
            }
            $this->category_id = $categoryModel->id;
        }
    }

    public function initShortnotes()
    {
        if ($this->shortnotes_id != "") {
            $shortnotes = new ShortNotes();
            if ($this->new_shortnote) {
                $shortnotesModel = $shortnotes->getShortnotesByTitle(
                    $this->shortnotes_id,
                    $this->project_id,
                    $this->category_id
                );
            } else {
                $shortnotesModel = $shortnotes::findOne($this->shortnotes_id);
            }
            $this->shortnotes_id = $shortnotesModel->id;
        }
    }

    public function createBalanceUsingThisFuturePayment($outflow = null, $inflow = null)
    {
        $balance = new Balance();
        $balance->outflow = $outflow;
        $balance->inflow = $inflow;
        $balance->category_id = $this->category_id;
        $balance->shortnotes_id = $this->shortnotes_id;
        $balance->project_id = $this->project_id;
        $balance->notes = $this->notes;
        $balance->wallet_id = $this->wallet_id;
        $balance->type = $this->type;
        $balance->parent_id = $this->parent_id;
        $balance->tag = $this->tag;
        $balance->manager_id = $this->manager_id;
        $balance->contractor_id = $this->contractor_id;
        $balance->save();
        $balance->wallet->updateSum();
        return $balance;
    }

    public function addFuturePaymentHistory($balance)
    {
        /* @var $balance \common\models\Balance */
        $future_payment_history = new FuturePaymentHistory();
        $future_payment_history->added_at = time();
        $periodicity = [
            FuturePayment::PERIODICITY_ONETIME_ID => FuturePayment::PERIODICITY_ONETIME_TEXT,
            FuturePayment::PERIODICITY_DAILY_ID => FuturePayment::PERIODICITY_DAILY_TEXT,
            FuturePayment::PERIODICITY_WEEKLY_ID => FuturePayment::PERIODICITY_WEEKLY_TEXT,
            FuturePayment::PERIODICITY_MONTHLY_ID => FuturePayment::PERIODICITY_MONTHLY_TEXT,
        ];
        $active = [
            FuturePayment::ACTIVE => FuturePayment::ACTIVE_TEXT,
            FuturePayment::NO_ACTIVE => FuturePayment::NO_ACTIVE_TEXT,
        ];
        $futurePayment = [
            'ID' => $this->id,
            'Расход' => $this->outflow,
            'Доход' => $this->inflow,
            'Категория' => $this->category->title,
            'Подкатегория' => $this->shortnotes->title,
            'Проект' => $this->project->title,
            'Менеджер' => !empty($this->manager) ? $this->manager->name : '',
            'Контрагент' => !empty($this->contractor) ? $this->contractor->name : '',
            'Примечание' => $this->notes,
            'Кошелек' => $this->wallet->title,
            'Дата "совершить"' => $this->added_at,
            'Тип записи' => $this->type,
            'Создано' => Yii::$app->formatter->asDatetime($this->created_at),
            'Обновлено' => Yii::$app->formatter->asDatetime($this->updated_at),
            'Тег' => $this->tag,
            'Пользователь' => isset($this->user) ? $this->user->username : '',
            'Периодичность' => $periodicity[$this->periodicity],
            'Статус' => $active[$this->active],
        ];
        $future_payment_history->future_payment = Json::encode($futurePayment);

        $budgetPayment = [
            'ID' => $balance->id,
            'Расход' => $balance->outflow,
            'Приход' => $balance->inflow,
            'Категория' => $balance->category->title,
            'Подкатегория' => $balance->shortnotes->title,
            'Проект' => $balance->project->title,
            'Менеджер' => !empty($balance->manager) ? $balance->manager->name : '',
            'Контрагент' => !empty($balance->contractor) ? $balance->contractor->name : '',
            'Примечание' => $balance->notes,
            'Кошелек' => $balance->wallet->title,
            'Дата баланса' => Yii::$app->formatter->asDatetime($balance->added_at),
            'Тип записи' => $balance->type,
            'Создано' => Yii::$app->formatter->asDatetime($balance->created_at),
            'Обновлено' => Yii::$app->formatter->asDatetime($balance->updated_at),
            'Тег' => $balance->tag,
            'Пользователь' => isset($balance->user) ? $balance->user->username : '',
        ];
        $future_payment_history->balance = Json::encode($budgetPayment);

        $future_payment_history->project_id = $this->project_id;

        return $future_payment_history->save();
    }

    public function copy()
    {
        $interval = [
            FuturePayment::PERIODICITY_DAILY_ID => FuturePayment::PERIODICITY_DAILY_INTERVAL,
            FuturePayment::PERIODICITY_WEEKLY_ID => FuturePayment::PERIODICITY_WEEKLY_INTERVAL,
            FuturePayment::PERIODICITY_MONTHLY_ID => FuturePayment::PERIODICITY_MONTHLY_INTERVAL,
        ];
        $futurePayment = clone $this;
        $futurePayment->isNewRecord = true;
        $futurePayment->id = null;
        $futurePayment->added_at = strtotime($this->added_at . $interval[$this->periodicity]);
        $futurePayment->created_at = strtotime(date('d.m.Y', $this->created_at) . $interval[$this->periodicity]);
        $futurePayment->updated_at = null;
        if (!$futurePayment->save()) {
            $msg = Json::encode($futurePayment->errors);
            Yii::warning($msg, __METHOD__);
        }
    }

    public function performPartialPayment($partialSum)
    {
        /* @var $this \common\models\FuturePayment */
        $this->outflow_partial -= $partialSum;

        if ($this->outflow_partial < 0) {
            return false;
        }

        if ($this->outflow_partial > 0) {
            $this->save();
            $this->outflow = $partialSum;
            $balance = $this->createBalanceUsingThisFuturePayment($this->outflow);
            $this->addFuturePaymentHistory($balance);
        } else {
            $outflow = $this->outflow;
            $this->outflow = $partialSum;
            $balance = $this->createBalanceUsingThisFuturePayment($this->outflow);
            $this->addFuturePaymentHistory($balance);
            $this->outflow = $outflow;

            if ($this->periodicity == FuturePayment::PERIODICITY_ONETIME_ID) {
                $this->outflow_partial = $outflow;
                $this->active = FuturePayment::NO_ACTIVE;
                $this->save();
            } else {
                $this->copy();
                $this->delete();
            }
        }
    }

    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['id' => 'manager_id']);
    }

    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }
}
