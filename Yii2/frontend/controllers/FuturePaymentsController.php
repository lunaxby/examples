<?php

namespace frontend\controllers;

use common\models\Balance;
use common\models\Category;
use common\models\FuturePayment;
use common\models\FuturePaymentSetting;
use common\models\Project;
use common\models\ShortNotes;
use common\models\Wallet;
use frontend\components\AccessHelper;
use frontend\components\Controller;
use frontend\components\FuturePaymentHelper;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class FuturePaymentsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                        ],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            /** @var AccessHelper $access */
                            $access = \Yii::$app->access;
                            $userId = \Yii::$app->user->id;

                            $projectId = \Yii::$app->request->get('project_id');
                            if (empty($projectId)) {
                                $projectId = \Yii::$app->request->get('projectId');
                            }

                            if ($access->isUserProjectOwner($userId, $projectId) ||
                                $access->userCanWriteProject($userId, $projectId) ||
                                $access->userCanReadProject($userId, $projectId) ||
                                $access->userCanManageProject($userId, $projectId)) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    ],
                    [
                        'actions' => [
                            'create',
                            'create-ajax',
                            'update',
                            'delete',
                            'perform',
                            'setting',
                            'setting-save',
                            'partial',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ]
        ];
    }

    public function actionIndex($project_id)
    {
        $currentProject = Project::findOne($project_id);
        if (!$currentProject) {
            $msg = "Проект не найден";
            throw new NotFoundHttpException($msg);
        }

        $query = FuturePayment::find()->where([
            'future_payment.project_id' => $project_id,
            'future_payment.active' => FuturePayment::ACTIVE]
        );
        $query->joinWith('shortnotes');
        $query->joinWith('category');

        $data = new ActiveDataProvider([
            'query' => $query
        ]);

        $data->sort->attributes['shortnotes_id'] = [
            'asc' => ['shortnotes.title' => SORT_ASC],
            'desc' => ['shortnotes.title' => SORT_DESC],
        ];

        $data->sort->attributes['category_id'] = [
            'asc' => ['category.title' => SORT_ASC],
            'desc' => ['category.title' => SORT_DESC],
        ];

        $dateFromInt = strtotime(date('Y-m-d 00:00:00'));
        $dateToIntWeekly = strtotime(date('Y-m-d 00:00:00') . FuturePayment::PERIODICITY_WEEKLY_INTERVAL);
        $dateToIntMonthly = strtotime(date('Y-m-d 00:00:00') . FuturePayment::PERIODICITY_MONTHLY_INTERVAL);

        $forecast = [
            'future-payment' => [
                'week' => FuturePaymentHelper::getForecastSumForProjectFromDateToDate(
                    $project_id,
                    FuturePayment::PERIODICITY_WEEKLY_INTERVAL,
                    $dateFromInt,
                    $dateToIntWeekly
                ),
                'month' => FuturePaymentHelper::getForecastSumForProjectFromDateToDate(
                    $project_id,
                    FuturePayment::PERIODICITY_MONTHLY_INTERVAL,
                    $dateFromInt,
                    $dateToIntMonthly
                ),
            ],
            'dashboard' => [
                'week' => FuturePaymentHelper::getForecastSumForProjectByDashboard(
                    $project_id,
                    FuturePayment::PERIODICITY_WEEKLY_INTERVAL
                ),
                'month' => FuturePaymentHelper::getForecastSumForProjectByDashboard(
                    $project_id,
                    FuturePayment::PERIODICITY_MONTHLY_INTERVAL
                ),
            ],
            'mixed' => [
                'week' => FuturePaymentHelper::getForecastSumForProjectMixed(
                    $project_id,
                    FuturePayment::PERIODICITY_WEEKLY_INTERVAL,
                    $dateFromInt,
                    $dateToIntWeekly
                ),
                'month' => FuturePaymentHelper::getForecastSumForProjectMixed(
                    $project_id,
                    FuturePayment::PERIODICITY_MONTHLY_INTERVAL,
                    $dateFromInt,
                    $dateToIntMonthly
                ),
            ]
        ];

        return $this->render('index', [
            'currentProject' => $currentProject,
            'project' => $currentProject,
            'data' => $data,
            'forecast' => $forecast,
        ]);
    }

    public function actionCreateAjax()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new Balance();

        $balance = Yii::$app->request->post('Balance');
        $project_id = $balance['project_id'];

        $currentProject = Project::findOne($project_id);
        $walletModel = Wallet::find()->where(['user_id' => Yii::$app->user->id])->all();

        if (!empty($model->added_at)) {
            $model->added_at = date('d.m.Y', $model->added_at);
        }

        $model->load(Yii::$app->request->post());
        $inflow = $model->inflow;
        $model->initCategory();
        $model->initShortnotes();

        if ($model->type == Balance::BALANCE_TYPE_TRANSFER) {
            if ($model->outflow == "") {
                $model->outflow = $model->inflow;
            }
            $model->inflow = "";
        } else {
            $model->parent_id = "";
            if ($model->outflow != "") {
                $model->inflow = "";
            }
        }

        $model->user_id = \Yii::$app->user->id;

        if ($model->save()) {
            $model->wallet->updateSum();
            if ($model->type == Balance::BALANCE_TYPE_TRANSFER) {
                $newModel = new Balance();
                $newModel->load(Yii::$app->request->post());

                $newModel->wallet_id = $model->wallet_id_transfer;
                $model->notes = $model->wallet->title . "->" . $newModel->wallet->title;
                $model->parent_id = $model->id;
                $model->save(false);

                $newModel->initCategory();
                $newModel->initShortnotes();
                $newModel->notes = $model->wallet->title . "->" . $newModel->wallet->title;

                if (empty($inflow)) {
                    $newModel->inflow = $model->outflow;
                } else {
                    $newModel->inflow = $inflow;
                }

                $newModel->outflow = "";
                $newModel->parent_id = $model->id;
                $newModel->user_id = \Yii::$app->user->id;
                $newModel->save();
                $newModel->wallet->updateSum();
            }

            return [
                'status' => 'success',
                'reset-form' => '1'
            ];
        }

        $model->load(Yii::$app->request->post());

        return [
            'status'=>'error',
            'html'=>$this->renderPartial('create', [
                'model' => $model,
                'categoryModel'=>new Category(),
                'shortnotesModel'=>new ShortNotes(),
                'walletModel'=>$walletModel,
                'currentProject'=>$currentProject,
                'tpl'=>'create'
            ])
        ];
    }

    public function actionCreate($project_id = null)
    {
        $model = new FuturePayment();
        if (!$project_id) {
            $project_id = Yii::$app->request->get('project_id');
        }
        $model->project_id = $project_id;

        if ($model->load(Yii::$app->request->post())) {
            if (!$project_id) {
                $project_id = Yii::$app->request->get('project_id');
            }
            if ($model->project_id) {
                $project_id = $model->project_id;
            }

            $model->initCategory();
            $model->initShortnotes();
            $model->save();

            $model = new FuturePayment();

            $model->project_id = $project_id;
            return $this->renderPartial('create', [
                'model' => $model,
                'action' => 'create',
                'projectId' => $project_id,
            ]);
        } else {
            return $this->renderPartial('create', [
                'model' => $model,
                'action' => 'create',
                'projectId' => $project_id,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = FuturePayment::findOne($id);
        /* @var $model FuturePayment */

        if ($model->load(Yii::$app->request->post())) {
            $model->outflow_partial = $model->outflow;
            $model->save();
            if ($model->project_id) {
                $project_id = $model->project_id;
            } else {
                $project_id = Yii::$app->request->get('project_id');
            }

            $model = new FuturePayment();

            $model->project_id = $project_id;

            return $this->renderPartial('create', [
                'model' => $model,
                'action' => 'create',
                'projectId' => $model->project_id,
            ]);
        } else {
            return $this->renderPartial('update', [
                'model' => $model,
                'action' => 'update',
                'projectId' => $model->project_id,
            ]);
        }
    }

    public function actionDelete($id)
    {
        FuturePayment::findOne($id)->delete();
    }

    public function actionPerform($id)
    {
        $futurePayment = FuturePayment::findOne($id);
        /* @var $futurePayment FuturePayment */
        $balance = $futurePayment->createBalanceUsingThisFuturePayment(
            $futurePayment->outflow_partial,
            $futurePayment->inflow_partial
        );
        $futurePayment->addFuturePaymentHistory($balance);
        if (!$futurePayment->periodicity) {
            throw new NotFoundHttpException('Не указана периодичность платежа');
        }

        if ($futurePayment->periodicity == FuturePayment::PERIODICITY_ONETIME_ID) {
            $futurePayment->active = FuturePayment::NO_ACTIVE;
            $futurePayment->save();
        } else {
            $futurePayment->copy();
            $futurePayment->delete();
        }
        if (Yii::$app->request->isAjax) {
            return "OK";
        } else {
            return $this->redirect(['/future-payments/index', 'project_id' => $futurePayment->project_id]);
        }
    }

    public function actionSetting($projectId)
    {
        $project = Project::findOne($projectId);
        $model = FuturePaymentSetting::findOne(['project_id' => $projectId]);
        if (empty($model)) {
            $model = new FuturePaymentSetting();
            $model->project_id = $projectId;
            $model->save();
        }

        return $this->render('/future-payments/setting', [
            'projectId' => $projectId,
            'project' => $project,
            'model' => $model
        ]);
    }

    public function actionSettingSave($id)
    {
        $model = FuturePaymentSetting::findOne($id);
        /* @var $model FuturePaymentSetting */
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/future-payments/setting', 'projectId' => $model->project_id]);
        }
    }

    public function actionPartial($id, $projectId)
    {
        $model = FuturePayment::findOne($id);
        /* @var $model FuturePayment */
        $post = Yii::$app->request->post('FuturePayment');
        if ($post && isset($post['partial']) && $post['partial']) {
            if ($post['partial'] <= $model->outflow_partial) {
                $partialSum = $post['partial'];
                $model->performPartialPayment($partialSum);
            } else {
                $msg = 'Сумма частичной оплаты не может быть больше суммы всего платежа';
                Yii::$app->session->setFlash('error', $msg);
            }
        } else {
            $msg = 'Не указана сумма частичного платежа или ошибка при отправке формы';
            Yii::$app->session->setFlash('error', $msg);
        }
        return $this->redirect(['future-payments/index', 'project_id' => $projectId]);
    }
}
