<?php

namespace frontend\components;

use common\models\Category;
use common\models\FuturePayment;
use common\models\FuturePaymentSetting;

class FuturePaymentHelper
{
    public static function getForecastSumForProject($projectId, $period)
    {
        $dateFromInt = strtotime(date('Y-m-d 00:00:00'));
        $dateToInt = strtotime(date('Y-m-d 00:00:00') . $period);
        $setting = FuturePaymentSetting::findOne(['project_id' => $projectId]);
        $sum = 0;
        /* @var $setting FuturePaymentSetting */
        if (!empty($setting)) {
            switch ($setting->calculate_forecast_type) {
                case FuturePaymentSetting::FORECAST_BY_FUTURE_PAYMENT_ID:
                default:
                    $sum = self::getForecastSumForProjectFromDateToDate($projectId, $period, $dateFromInt, $dateToInt);
                    break;
                case FuturePaymentSetting::FORECAST_BY_DASHBOARD_ID:
                    $sum = self::getForecastSumForProjectByDashboard($projectId, $period);
                    break;
                case FuturePaymentSetting::FORECAST_BY_MIXED_ID:
                    $sum = self::getForecastSumForProjectMixed($projectId, $period, $dateFromInt, $dateToInt);
                    break;
            }
        }

        $sum = intval($sum);
        return $sum;
    }

    public static function getForecastSumForProjectFromDateToDate($projectId, $period, $dateFromInt, $dateToInt)
    {
        $condition = [
            'project_id' => $projectId,
            'active' => FuturePayment::ACTIVE,
        ];
        $futurePayments = FuturePayment::find()->where($condition);
        $futurePayments->andWhere(['<=', 'added_at', $dateToInt]);
        $futurePayments->andWhere([
            '!=',
            'category_id',
            [
                Category::getDividentCategoryId($projectId),
                Category::getTransferCategoryId($projectId)
            ]
        ]);
        $futurePayments = $futurePayments->all();

        $sum = 0;
        foreach ($futurePayments as $futurePayment) {
            /* @var $futurePayment \common\models\FuturePayment */
            if ($futurePayment->periodicity && strtotime($futurePayment->added_at) >= $dateFromInt) {
                $days = round(($dateToInt - strtotime($futurePayment->added_at)) / 60 / 60 / 24);
                $weeks = round($days / 7);
                switch ($futurePayment->periodicity) {
                    case FuturePayment::PERIODICITY_ONETIME_ID:
                        $sum += $futurePayment->outflow_partial ? (int) $futurePayment->outflow_partial : (int) $futurePayment->outflow;
                        break;
                    case FuturePayment::PERIODICITY_DAILY_ID:
                        $sum += $futurePayment->outflow_partial ? (int) $futurePayment->outflow_partial + (int) $futurePayment->outflow * ($days - 1) : (int) $futurePayment->outflow * $days;
                        break;
                    case FuturePayment::PERIODICITY_WEEKLY_ID:
                        $sum += $futurePayment->outflow_partial ? (int) $futurePayment->outflow_partial + (int) $futurePayment->outflow * ($weeks - 1) : (int) $futurePayment->outflow * $weeks;
                        break;
                    case FuturePayment::PERIODICITY_MONTHLY_ID:
                        $sum += $futurePayment->outflow_partial ? $futurePayment->outflow_partial : (int) $futurePayment->outflow;
                        break;
                }
            }
            if (strtotime($futurePayment->added_at) < $dateFromInt) {
                $sum += (int) $futurePayment->outflow;
            }
        }

        return $sum;
    }

    public static function getForecastSumForProjectByDashboard($projectId, $period, $currentDate = null)
    {
        if (!$currentDate) {
            $currentDate = time();
        }
        $dashboardHelper = new DashboardHelper();
        $categories = DashboardHelper::getCategoryListForGauge($projectId);
        $gauges = [];
        $allDeltaMonthSum = 0;
        $allCurrentMonthSum = 0;
        if ($categories) {
            foreach ($categories as $category) {
                if ($category == Category::getTransferCategoryId($projectId) ||
                    $category == Category::getDividentCategoryId($projectId)) {
                    continue;
                }
                $gauge = $dashboardHelper->getDataGaugeForCategoriesOutflow(
                    $projectId,
                    date('m.Y', $currentDate),
                    date('m.Y', $currentDate),
                    $category,
                    null,
                    ' - 6 months',
                    DashboardHelper::GET_DATA_FOR_FORECAST
                );

                if ($gauge['currentMonthSum'] > $gauge['deltaMonthSum']) {
                    continue;
                }
                $allDeltaMonthSum += $gauge['deltaMonthSum'];
                $allCurrentMonthSum += $gauge['currentMonthSum'];
                $gauges[] = $gauge;
            }
        }
        $days = date('t', $currentDate) - date('j', $currentDate);
        $countDaysInNextMonth = date('t', strtotime(date('Y-m-d', $currentDate).' + 1 month'));
        $sumForDayInNextMonth = $allDeltaMonthSum / $countDaysInNextMonth;
        $residue = ($allDeltaMonthSum - $allCurrentMonthSum);
        $sum = 0;
        if ($period == FuturePayment::PERIODICITY_WEEKLY_INTERVAL) {
            if ($days >= 7) {
                $sum = $residue / $days * 7;
            } else {
                $sum = $residue + ((7 - $days) * $sumForDayInNextMonth);
            }
        }
        if ($period == FuturePayment::PERIODICITY_MONTHLY_INTERVAL) {
            $addDays = date('t', $currentDate) - $days;
            $sum = ($sumForDayInNextMonth * $addDays + $residue);
        }

        $sum = intval($sum);
        return $sum;
    }

    public static function getForecastSumForProjectMixed($projectId, $period, $dateFromInt, $dateToInt, $currentDate = null)
    {
        $sum = self::getForecastSumForProjectByDashboard($projectId, $period, $currentDate);

        $condition = [
            'project_id' => $projectId,
            'active' => FuturePayment::ACTIVE,
        ];
        $futurePayments = FuturePayment::find()->where($condition);
        $futurePayments->andWhere(['<=', 'added_at', $dateToInt]);
        $futurePayments->andWhere([
            '!=',
            'category_id',
            [
                Category::getDividentCategoryId($projectId),
                Category::getTransferCategoryId($projectId)
            ]
        ]);
        $futurePayments = $futurePayments->all();
        foreach ($futurePayments as $futurePayment) {
            /** @var $futurePayment FuturePayment */
            $sum += $futurePayment->outflow_partial ? $futurePayment->outflow_partial : $futurePayment->outflow;
        }

        $sum = intval($sum);
        return $sum;
    }
}
